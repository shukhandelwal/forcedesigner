<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="img/favicon.ico" type="image/gif" sizes="16x16">

	<link type="text/css" href="css/bootstrap-datepicker.css" rel="stylesheet">
	<link type="text/css" href="css/font-awesome.min.css" rel="stylesheet">
	<link type="text/css" href="css/jquery-ui.css" rel="stylesheet">
	<link type="text/css" href="css/dataTables.bootstrap.css" rel="stylesheet">
	<link type="text/css" href="css/multi-select.css" rel="stylesheet">
	<link type="text/css" href="css/bootstrap.min.css" rel="stylesheet">

	<title>Deloitte Force Designer - Select Components</title>

	<style type="text/css">
	 		@font-face {
              font-family: "Frutiger Next Pro Light";
              src: url('fonts/FrutigerNextPro-Light.otf');
            }
            @font-face {
              font-family: "Frutiger Next Pro Medium";
              src: url('fonts/FrutigerNextPro-Medium.otf');
            }
            @font-face {
              font-family: "Frutiger Next Pro Bold";
              src: url('fonts/FrutigerNextPro-Bold.otf');
            }
            /*
             * General styles
             */
            /* body, html {
                height: 100%;
                background-repeat: no-repeat;
                font-family: 'Frutiger Next Pro Light', sans-serif;
            } */
			html{height: 100%;}
			body{background:#fff; min-height: 100%; height: 100%;}

			.box {
				position: relative;
				-webkit-box-shadow: 0px 0px 0px rgba(0,0,0,.5);
				-moz-box-shadow: 0px 0px 0px rgba(0,0,0,.5);
				box-shadow: 0px 0px 0px rgba(0,0,0,.5); 
				/* Kokakify */
				padding: 1px;
				background: #F7F7F7;
				border-color:#337ab7;
				border: 1px solid transparent #306EFF;
				//border-radius:4px;
			}
			
			/*.btncustom{height:40px; border:none; background:#0C6; width:100%; outline:none; font-family: 'Source Sans Pro', sans-serif; font-size:20px; font-weight:bold; color:#eee; border-bottom:solid 3px #093; border-radius:0px; cursor:pointer;}*/
			.form-container{margin-top: 20px;}
			.multiselect-table{margin-top: 10px;}
			#user-select-container select, .component-select-main-container select{min-height: 150px !important; max-height: 200px !important; width:250px;}

			.add-arrow, .remove-arrow, .add-all-arrow, .remove-all-arrow{padding: 3px 20px;}
			.button-container{margin-top: 10px; margin-left: 0px;}
			.component-select-main-container{margin-top: 20px;}
			.spinner{
	            border: none;
	            height: 40px;
	            width: 40px;
	            position: fixed;
	            top: 50%;
	            left: 50%;
	            margin-left: -20px;
	            margin-top: -20px;
	            z-index: 2;
	        }
	        .cover 
	        {
	            background-color:black;
	            width:100%;
	            height:100%;
	            position:fixed;
	            top: 0;
	            left: 0;
	            opacity: 0.2;
	            z-index:5;
	        }

	        .full-wrapper {
			    display:flex;
			    /* Direction of the items, can be row or column */
			    flex-direction: column;
			    min-height: 100%;
			}
			header{
				height: 100px;
			}
			footer{
				height: 50px;
			}
			main {
			    flex:1;
			}
			.navbar-default {
				background-color: #000;
				border-color: #e7e7e7;
				color: #fff;
			}
			.table>thead>tr>th {
				vertical-align: top;
				font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
				font-size: 12px;
			}
			
			.footer{text-align: center; background-color: #F7F7F7; padding: 10px;}
			.header-dflogo{height: 80px; padding: 10px 0; -webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;}
			.header-brand-text{display:inline; font-family: 'Frutiger Next Pro Bold', sans-serif;}
			.header-brand-text h2{display:inline; vertical-align:middle;}
			.header-ddlogo{height: 80px; float: left; padding: 10px 0; -webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;}
			.clear{clear: both;}
			.panel-heading a:after {font-family:'Glyphicons Halflings';content:"\e114";float: right;color: grey;}
			.panel-heading a.collapsed:after {content:"\e080";}
			.panel-heading a:hover{text-decoration: none;}

			#componentResultsPanel{margin: 30px 0 40px 0;}
			/* .required:before{content:"|";color:#c00;background-color: #c00;width: 3px;float:left;padding:1px 0;margin:0 2px;} */
			.wrapper {
			    position:relative;
			    margin:0 auto;
			    overflow:hidden;
				padding:5px;
			  	height:43px;
			}

			.tab-content {
				padding:10px;
			}

			.list {
			    position:absolute;
			    left:0px;
			    top:0px;
			  	min-width:3000px;
			  	margin-left:12px;
			    margin-top:0px;
			}

			.list li{
				display:table-cell;
			    position:relative;
			    text-align:center;
			    cursor:grab;
			    cursor:-webkit-grab;
			    color:#efefef;
			    vertical-align:middle;
			    border-bottom: 1px solid
			}

			.scroller {
			  text-align:center;
			  cursor:pointer;
			  display:none;
			  padding:7px;
			  padding-top:11px;
			  white-space:no-wrap;
			  vertical-align:middle;
			  background-color:#fff;
			}

			.scroller-right{
			  float:right;
			}

			.scroller-left {
			  float:left;
			}
			.required-asterisk{color: #CA1E34; margin-left: 2px; font-size: 20px;}
			#errorMsg{margin-top: 20px;}
			@media (max-width: 768px){
				header{height: 150px;}
			}
			.width-30-percent {
				width:30%;
			}
			.width-40-percent {
				width:40%;
			}
			.multiselect-table, .glyphicon{
				cursor:pointer;
			}
			.inline-element{
				display:inline;
			}
			#userNamePanel, .glyphicon-container{
				text-align: right;
				margin-top: 30px;
			}
	</style>
</head>

<body>
	<div class="full-wrapper">
		<header class="header">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="col-xs-2 col-sm-2 col-md-2">
						<img id="header-ddlogo" alt="Force Designer" class="header-ddlogo " src="img/DFDesignerLogo_transparent.png"></img>
					</div>
					<div class="col-xs-7 col-sm-7 col-md-7 header-brand-text" style="text-align:center;">
						<img class="header-dflogo" alt="Deloitte Force Designer"
							src="img/DFDesignerLogo_transparent.png" style="visibility:hidden;"></img> 
						<h2>Deloitte Force Designer</h2>

					</div>
					<div class="col-xs-2 col-sm-2 col-md-2" id="userNamePanel"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 glyphicon-container"  >
						<span class="glyphicon glyphicon-off" id="signoutIcon" aria-hidden="true" title="Signout"></span>

					</div>
				</div>
				
				
			</nav>
			<div class="clear"></div>
		</header>

		<main>
		<div class="container">
			<form action="FirstPageClass" method="post" class="form-container" id="dfd-form" role="form">
				
				<div id="errorMsg" class="alert alert-danger" role="alert" style="display:none;"></div>

				<div class="form-group row">

					<label for="clientName" for="clientName" data-toggle="tooltip" data-placement="right"
						title="Name of the client" class="col-xs-12 col-sm-2 col-md-2">
						Client Name
					</label>
					<div class="col-xs-12 col-sm-10 col-md-10 required inline-element">
						<input id="clientName" name="clientName"
									class="form-control width-30-percent inline-element" placeholder="Client Name" required
									autofocus />
						<strong class="required-asterisk">*</strong>
					</div>
				</div>


				
				<div class="form-group row">
	
					<label for="projectName" data-toggle="tooltip" data-placement="right"
						title="Name of the project" class="col-xs-12 col-sm-2 col-md-2">
						Project Name
					</label>
					<div class="col-xs-12 col-sm-10 col-md-10 required inline-element">
						<input id="projectName" name="projectName"
							class="form-control width-30-percent inline-element" placeholder="Project Name" required />
						<strong class="required-asterisk">*</strong>
					</div>
						
				</div>
				
				
				<div class="form-group row">
					
					<label for="startDate" data-toggle="tooltip" data-placement="right"
						title="Project Commencement Date or Sprint Start Date" class="col-xs-12 col-sm-2 col-md-2">
						Project/Sprint Start Date
					</label>
					<div class="col-xs-12 col-sm-4 col-md-4 required inline-element ">
						<input id="startDate" name="startDate" class="datepicker inline-element" placeholder="MM/DD/YYYY" required />
						<strong class="required-asterisk">*</strong>
					</div>
			
					<label for="endDate" data-toggle="tooltip" data-placement="right"
						title="Sprint End Date, for selecting components created between the Start and End dates"
						class="col-xs-12 col-sm-2 col-md-2">
						Project/Sprint End Date
					</label> 
					<div class="col-xs-12 col-sm-4 col-md-4 required inline-element">
						<input id="endDate" name="endDate" class="datepicker" placeholder="MM/DD/YYYY"/>
					</div>
					
				</div>


				<div class="advanced-options-container">
					<div class="panel-group" id="accordion">
						<div class="panel panel-default" id="advancedOptionsPanel">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-target="#collapseOne"
										href="#collapseOne" class="collapsed"> Advanced Options </a>
								</h4>

							</div>
							<div id="collapseOne" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="form-group row">
										<label for="componentPrefix" class="col-xs-12 col-sm-4"
											data-toggle="tooltip" data-placement="right"
											title="If there are any prefix for the items to be considered, include them here. For example, if all the items for the project start with PRJ_ then include PRJ_ here.">Customization Identifier Prefix
										</label> 
										<div class="col-xs-12 col-sm-8">
											<input id="componentPrefix" class="form-control width-40-percent" placeholder="e.g., abc*" />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-xs-12 col-sm-4" data-toggle="tooltip"
											data-placement="right"
											title="Users who created or updated components">
											Select Team
										</label>
										<div class="col-xs-12 col-sm-8">
											<div class="input-group width-40-percent">
										      	<input type="text" class="form-control" placeholder="Search for a User..." id="userNameFilter">

										      	<span class="input-group-btn">
										        	<button type="button" class="btn btn-primary" aria-label="Search User" onClick="getUsersList();">
										        		<span class="glyphicon glyphicon-search"></span>
										        	</button>

										      	</span>
										    </div>
								      	</div>

								      	<div class="col-xs-12 col-sm-4"></div>
										<div id="user-loading-message" class="col-xs-12 col-sm-8">Loading...</div>
										<div id="user-select-container" class="col-xs-12 col-sm-8">
											<table class="multiselect-table ">
												<tr>
													<td><select id="users-to-select" multiple="multiple"
														name="usernames">
													</select></td>
													<td>
														<div class="add-all-arrow">
															<a id="addAllTeam"><span
																class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
														</div>
														<div class="add-arrow">
															<a id="addTeam"><span
																class="glyphicon glyphicon-triangle-right"
																aria-hidden="true"></span></a>
														</div>
														<div class="remove-arrow">
															<a id="removeTeam"><span
																class="glyphicon glyphicon-triangle-left"
																aria-hidden="true"></span></a>
														</div>
														<div class="remove-all-arrow">
															<a id="removeAllTeam"><span
																class="glyphicon glyphicon-backward" aria-hidden="true"></span></a>
														</div>
													</td>
													<td><select multiple="multiple" id="users-selected"
														name="selectedusernames"></select></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="component-select-main-container">
					<div class="box">
						<label> Select the Components to be included in the
							Technical Design Document:</label>
					</div>
					<div id="components">
						<table class="multiselect-table">
							<tr>
								<td><select id="components-to-select" multiple="multiple"
									name="components-to-select">
										<option value="ApexClass">Apex Classes</option>
										<option value="ApexComponent">Apex Components</option>
										<option value="ApexTrigger">Apex Triggers</option>
										<option value="ApprovalProcess">Approval Processes</option>
										<option value="BusinessProcess">Business Process</option>
										<option value="CustomField">Custom Fields</option>
										<option value="CustomLabel">Custom Labels</option>
										<option value="CustomObject">Custom Objects</option>
										<option value="CustomSetting">Custom Settings</option>
										<option value="CustomTab">Custom Tabs</option>
										<option value="EmailTemplate">Email Templates</option>
										<!--<option value="PageLayout">Page Layouts</option>-->
										<option value="PermissionSet">Permission Sets</option>
										<option value="Profile">Profiles</option>
										<!--<option value="RecordType">Record Types</option>-->
										<option value="Role">Roles</option>
										<option value="SharingRules">Sharing Rules</option>
										<option value="StaticResource">Static Resources</option>
										<option value="ValidationRule">Validation Rules</option>
										<option value="ApexPage">Visualforce Pages</option>
										<option value="WorkflowAlert">Workflow Email Alerts</option>
										<option value="WorkflowFieldUpdate">Workflow Field Updates</option>
										<option value="WorkflowOutboundMessage">Workflow Outbound Messages</option>
										<option value="WorkflowRule">Workflow Rules</option>
										<option value="WorkflowTask">Workflow Tasks</option>

								</select></td>
								<td>
									<div class="add-all-arrow">
										<a id="addAllComp"><span
											class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
									</div>
									<div class="add-arrow">
										<a id="addComp"><span
											class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></a>
									</div>
									<div class="remove-arrow">
										<a id="removeComp"><span
											class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span></a>
									</div>
									<div class="remove-all-arrow">
										<a id="removeAllComp"><span
											class="glyphicon glyphicon-backward" aria-hidden="true"></span></a>
									</div>
								</td>
								<td><select multiple="multiple" id="selected-components"
									name="selected-components"></select></td>
							</tr>
						</table>
					</div>
				</div>
				
				<div class="row button-container">
					<button type="button" class="btn btn-primary"
						id="fetchComponentsBtn">Fetch the Component list</button>
				</div>
				<div class="row button-container">
					<button type="submit" class="btn btn-success generateBtn"
						id="generateTDDBtn" name="GenerateTDD" onClick="$('#generateTDDChk').prop('checked', true);$('#generateDDChk').prop('checked', false)" style="display: none">Generate
						Technical Design Document</button>
					<input type="checkbox" id="generateTDDChk" style="visibility: hidden;" name="generateTDDChk" checked/>
					<button type="submit" class="btn btn-success generateBtn" id="generateDDBtn" name="GenerateDD" onClick="$('#generateTDDChk').prop('checked',false);$('#generateDDChk').prop('checked', true)" style="display: none">Generate
						Data Dictionary</button>
					<input type="checkbox" id="generateDDChk" style="visibility: hidden;" name="generateDDChk" checked/>
				</div>

				<div id="componentResultsPanel" style="display: none;">
					<div class="scroller scroller-left">
						<i class="glyphicon glyphicon-chevron-left"></i>
					</div>
					<div class="scroller scroller-right">
						<i class="glyphicon glyphicon-chevron-right"></i>
					</div>
					<!-- Nav tabs -->
					<div class="wrapper">
						<ul id="compTab" class="nav nav-tabs list" role="tablist">
							<li class="active"><a href="#apexclass" data-toggle="tab">Apex Classes</a></li>
							<li><a href="#apexcomponents" data-toggle="tab">Apex Components</a></li>
							<li><a href="#trigger" data-toggle="tab">Apex Triggers</a></li>
							<li><a href="#approvalprocesses" data-toggle="tab">Approval Processes</a></li>
							<li><a href="#businessprocess" data-toggle="tab">Business Processes</a></li>
							<li><a href="#customfields" data-toggle="tab">Custom Fields</a></li>
							<li><a href="#customlabels" data-toggle="tab">Custom Labels</a></li>
							<li><a href="#customobjects" data-toggle="tab">Custom Objects</a></li>
							<li><a href="#customsettings" data-toggle="tab">Custom Settings</a></li>
							<li><a href="#customtabs" data-toggle="tab">Custom Tabs</a></li>
							<li><a href="#emailtemplates" data-toggle="tab">Email Templates</a></li>
							<!--<li><a href="#pagelayouts" data-toggle="tab">Page Layouts</a></li>-->
							<li><a href="#permissionsets" data-toggle="tab">Permission Sets</a></li>
							<li><a href="#profiles" data-toggle="tab">Profiles</a></li>
							<li><a href="#recordtypes" data-toggle="tab">Record Types</a></li>
							<li><a href="#roles" data-toggle="tab">Roles</a></li>
							<li><a href="#sharingrules" data-toggle="tab">Sharing Rules</a></li>
							<li><a href="#staticresources" data-toggle="tab">Static Resources</a></li>
							<li><a href="#validationrule" data-toggle="tab">Validation Rules</a></li>
							<li><a href="#vfpage" data-toggle="tab">Visualforce Pages</a></li>
							<li><a href="#workflowalerts" data-toggle="tab">Workflow Email Alerts</a></li>
							<li><a href="#workflowfieldupdates" data-toggle="tab">Workflow Field Updates</a></li>
							<li><a href="#workflowoutboundmessages" data-toggle="tab">Workflow Outbound Messages</a></li>		
							<li><a href="#workflowrules" data-toggle="tab">Workflow Rules</a></li>
							<li><a href="#workflowtasks" data-toggle="tab">Workflow Tasks</a></li>
						</ul>
					</div>

					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane fade in active" id="apexclass">
							<table id="apexclasstable" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="apexcheck1"
											class="apexcheckclass" name="apexcheck1" checked/></th>
										<th>Class Name</th>
										<th>Class Status</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="trigger">
							<table id="apextriggertable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="triggercheckall"
											name="triggercheckall" checked/></th>
										<th>Trigger Name</th>
										<th>Trigger Status</th>
										<th>Object Name</th>
										<th>Trigger Conditions</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="vfpage">
							<table id="vftable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="vfapexcheck"
											name="vfapexcheck" checked/></th>
										<th>Page Name</th>
										<th>Description</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="apexcomponents">
							<table id="apexComponentTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="apexComponentCheckAll"
											name="apexComponentCheckAll" checked/></th>
										<th>Component Label</th>
										<th>Component Name</th>
										<th>Description</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="businessprocess">
							<table id="businessProcessTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="businessprocessCheckAll"
											name="businessprocessCheckAll" checked/></th>
										<th>Object Name</th>
										<th>Full Name</th>
										<th>Description</th>
										<th>Active</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="validationrule">
							<table id="validationruletable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="validationcheckall"
											name="validationcheckall" checked/></th>
										<th>Validation Rule Name</th>
										<th>Active</th>
										<th>Description</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="customfields">
							<table id="customFieldTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="customFieldCheckAll"
											name="customFieldCheckAll" checked/></th>
										<th>Field Name</th>
										<th>Object Name</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>						
						<div class="tab-pane fade" id="assignmentrules">
							<table id="assignmentRuleTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="assignmentRuleCheckAll"
											name="assignmentRuleCheckAll" checked/></th>
										<th>Assignment Rule Name</th>
										<th>Active</th>
										<th>Object</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="staticresources">
							<table id="staticresourcestable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="staticresourcesCheckAll"
											name="staticresourcesCheckAll" checked/></th>
										<th>Name</th>
										<th>Description</th>
										<th>Content Type</th>
										<th>Created By </th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="customlabels">
							<table id="customLabelTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="customLabelCheckAll"
											name="customLabelCheckAll" checked/></th>
										<th>Name</th>
										<th>Category</th>
										<th>Short Description</th>
										<th>Value</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
						<div class="tab-pane fade" id="sharingrules">
							<table id="sharingRulesTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="sharingRulesCheckAll"
											name="sharingRulesCheckAll" checked/></th>
										<th>Name</th>
										<th>Object</th>
										<th>Type</th>
										<th>Created By </th>
										<th>Created Date</th>
										<th>LastModified By</th>
										<th>LastModified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane fade" id="workflowtasks">
							<table id="workflowTaskTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="workflowTaskCheckAll"
											name="workflowTaskCheckAll" checked/></th>
										<th>Name</th>
										<th>Object</th>
										<th>Subject</th>
										<th>Priority</th>	
										<th>Type</th>												
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane fade" id="customobjects">
							<table id="customobjectstable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="customobjectsCheckAll"
										name="customobjectsCheckAll" checked/></th>
										<th>Label</th>
										<th>Name</th>
										<th>Description</th>
										<th>Created By </th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="customsettings">
							<table id="customsettingstable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="customsettingsCheckAll"
										name="customsettingsCheckAll" checked/></th>
										<th>Label</th>
										<th>Name</th>
										<th>Description</th>
										<th>Custom Setting Type</th>
										<th>Visibility</th>
										<th>Created By </th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="customtabs">
							<table id="customtabtable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="customTabCheckAll"
										name="customTabCheckAll" checked/></th>
										<th>Name</th>
										<th>Description</th>
										<th>Custom Object</th>
										<th>Type</th>
										<th>Created By </th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="emailtemplates">
							<table id="emailTemplateTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="emailTemplateCheckAll"
											name="emailTemplateCheckAll" checked/></th>
										<th>Name</th>
										<th>Description</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane fade" id="pagelayouts">
							<table id="pageLayoutTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="pageLayoutCheckAll"
											name="pageLayoutCheckAll" checked/></th>
										<th>Name</th>
										<th>Object</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane fade" id="roles">
							<table id="roleTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="roleCheckAll"
											name="roleCheckAll" checked/></th>
										<th>Name</th>
										<th>Label</th>
										<th>Parent Role</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane fade" id="flows">
							<table id="triggertable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="apexcheck2"
											name="apexcheck2" checked/></th>
										<th>Name</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="queues">
							<table id="triggertable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="apexcheck2"
											name="apexcheck2" checked/></th>
										<th>Name</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane fade" id="reporttypes">
							<table id="triggertable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="apexcheck2"
											name="apexcheck2" checked/></th>
										<th>Name</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="approvalprocesses">
							<table id="approvalProcessTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="approvalProcessCheckAll"
											name="approvalProcessCheckAll" checked/></th>
										<th>Full Name</th>
										<th>Description</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="permissionsets">
							<table id="permissionSetTable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="permissionSetCheckAll"
											name="permissionSetCheckAll" checked/></th>
										<th>Full Name</th>
										<th>Description</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="profiles">
							<table id="profilestable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="profileCheckAll"
											name="profileCheckAll" checked/></th>
										<th>Full Name</th>
										<th>Description</th>
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
						<div class="tab-pane fade" id="recordtypes">
							<table id="recordtypetable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="recordtypecheckall"
											name="recordtypecheckall" checked/></th>
										<th>Record Type Name</th>
										<th>Description</th>											
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
						<div class="tab-pane fade" id="workflowrules">
							<table id="workflowruletable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="workflowrulecheckall"
											name="workflowrulecheckall" checked/></th>
										<th>Workflow Rule Name</th>
										<th>Object Name</th>													
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
						<div class="tab-pane fade" id="workflowfieldupdates">
							<table id="workflowfieldupdatetable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="workflowfieldupdatecheckall"
											name="workflowfieldupdatecheckall" checked/></th>
										<th>Workflow Field Update Name</th>
										<th>Object Name</th>													
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="workflowoutboundmessages">
							<table id="workflowoutboundmessagetable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="workflowoutboundmessagecheckall"
											name="workflowoutboundmessagecheckall" checked/></th>
										<th>Workflow Outbound Message Name</th>
										<th>Object Name</th>													
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					 <div class="tab-pane fade" id="workflowalerts">
							<table id="workflowalerttable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="workflowalertcheckall"
											name="workflowalertcheckall" checked/></th>
										<th>Workflow Email Alert Name</th>
										<th>Object Name</th>													
										<th>Created By</th>
										<th>Created Date</th>
										<th>Last Modified By</th>
										<th>Last Modified Date</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>	
						<div class="tab-pane fade" id="customsettings">
							<table id="triggertable"
								class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><input type="checkbox" id="apexcheck2"
											name="apexcheck2" checked/></th>
										<th>Name</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
				</div>
				<!-- End of component results panel -->
			</form>
			<div id="spinner" class="spinner" style="display: none;">
				<i class="fa fa-spinner fa-pulse fa-4x"></i>
			</div>
			<div class="cover" style="display: none;"></div>
		</div>
		<!-- End of Middle section --> </main>

		<footer class="footer">
			<div class="container">
				<p>Copyright &copy; 2015 Deloitte Development LLC. All rights
					reserved.</p>
			</div>
		</footer>
	</div>
	<!-- Full Wrapper -->



	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery-check-all.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.multi-select.js"></script>
	<script type="text/javascript" src="js/response-monitor.js"></script>
	<script type="text/javascript" src="js/response-monitor.jquery.js"></script>
	<script>
		// var dfd_form = document.getElementById('dfd-form');
	 //    //the submit event of the form will be monitored
	 //    ResponseMonitor.register(dfd_form);
	 	var customFieldTable, validationruletable, vftable, apexclasstable, apexcomponenttable, staticresourcetable, customobjecttable, assignmentRuleTable, profileTable,customsettingstable;
	 	var customtabtable,apexComponentTable,approvalProcessTable,customLabelTable, sharingRulesTable;
	 	
	 	function getUsersList() {
	 		
	 		
			$.ajax({
				type: "GET", 
				url : 'FirstPageClass',
				data : {
					action : "fetchuserlist",
					userNameFilter : $("#userNameFilter").val()
				},
				beforeSend: function(jqXHR,settings ){
					$("#user-select-container").hide();
					$("#user-loading-message").show();
				},	
				success : function(responseText) {
					if(responseText.success == true && responseText.result){
						var $select = $('#users-to-select');
						$select.html("");
						var resultJSON = JSON.parse(responseText.result);
						$.each(resultJSON, function(index, value) {               
							$('<option>').val(value.Id).text(value.Name).appendTo($select);      
						});
						$("#users-to-select").html($('#users-to-select option').sort(function(x, y) {
							return $(x).text() < $(y).text() ? -1 : 1;
						}));
						$("#user-select-container").show();
						$("#user-loading-message").hide();
						
					}else{
						$('#users-to-select').html("");
						$("#user-select-container").show();
						$("#user-loading-message").hide();
						// If success but no data, then redirect to login page
						//Something went wrong message.
						//alert('Sorry, something went wrong. Please login again and proceed');
						//window.location.replace('login.jsp');
					}
				}
			});
		}

		$(document).ready(function() {				
			$("#user-select-container").hide();
			$("#componentResultsPanel").hide();
			$('.datepicker').datepicker();
			$('[data-toggle="tooltip"]').tooltip();

			/*** BEGIN TAB SCROLL ***/
			var hidWidth;
			var scrollBarWidths = 40;


			$(window).on('resize',function(e){  
			  	reAdjust();
			});

			$('.scroller-right').click(function() {
			  $('.scroller-left').fadeIn('slow');
        // if (($('.wrapper').outerWidth()) >= widthOfList()) {
        //     $('.scroller-right').fadeOut('slow');
        // }
        // $('.list').animate({left:"+="+widthOfHidden()+"px"},'slow',function(){
        // });
        var wrapperWidth = $('.wrapper').outerWidth();
        var tabPanelLeft = Math.abs($('.list').position().left);
        var toShift = 300;
        if((tabPanelLeft + wrapperWidth + 300) >= widthOfList()){
			  	$('.scroller-right').fadeOut('slow');
        	toShift = (tabPanelLeft + wrapperWidth + 300) - widthOfList() + 10;
			  }
        $('.list').animate({
            left: "+=" + (-toShift) + "px"
        }, 'slow', function() {});
			  
        // If left position + wrapper's width is more than the total list's width, then stop the right arrow.
			});

			$('.scroller-left').click(function() {
    	var listLeftPosition = $('.list').position().left;
        if ( listLeftPosition <= 0){
					$('.scroller-left').fadeOut('slow');
				}
				$('.scroller-right').fadeIn('slow');
        var toShift = 300;
        if(listLeftPosition - toShift <= 0){
        	toShift = listLeftPosition;
        }
        $('.list').animate({
            left: "-=" + toShift + "px"
        }, 'slow', function() {});
			  	});
			/*** END OF TAB SCROLL ***/

			/**** BEGIN FORM SUBMIT DOWNLOAD MONITOR ****/
		    var options = {
		        onRequest: function(token){
		            // Start spinner
					$('#spinner').show();
					$(".cover").show();
		        },
		        onMonitor: function(countdown){
		            // $('#duration').html(countdown); 
		        },
		        onResponse: function(status){
		            // Hide spinner
					$('#spinner').hide();
					$(".cover").hide();
		        },
		        onTimeout: function(){
		            // Hide spinner
					$('#spinner').hide();
					$(".cover").hide();
		        }
		    };
		    $('#dfd-form').ResponseMonitor(options);
		    /**** END OF FORM SUBMIT DOWNLOAD MONITOR ****/

		    function validateUsersList() {
		    	if($("#users-selected option").length > 10) {
		    		return false;
		    	}
		    	return true;
		    }

			$('#addTeam').click(function() {  
				!$('#users-to-select option:selected').remove().appendTo('#users-selected');
				$("#users-selected").html($('#users-selected option').sort(function(x, y) {
					return $(x).text() < $(y).text() ? -1 : 1;
				}));
			 });  
			 $('#removeTeam').click(function() {  
				!$('#users-selected option:selected').remove().appendTo('#users-to-select'); 
				$("#users-to-select").html($('#users-to-select option').sort(function(x, y) {
					return $(x).text() < $(y).text() ? -1 : 1;
				}));
			 });
			 $('#addAllTeam').click(function() {  
				!$('#users-to-select option').remove().appendTo('#users-selected');
				$("#users-selected").html($('#users-selected option').sort(function(x, y) {
					return $(x).text() < $(y).text() ? -1 : 1;
				}));
			 });  
			 $('#removeAllTeam').click(function() {  
				!$('#users-selected option').remove().appendTo('#users-to-select'); 
				$("#users-to-select").html($('#users-to-select option').sort(function(x, y) {
					return $(x).text() < $(y).text() ? -1 : 1;
				}));
			 });

			 $('#addComp').click(function() {  
				!$('#components-to-select option:selected').remove().appendTo('#selected-components'); 
				$("#selected-components").html($('#selected-components option').sort(function(x, y) {
					return $(x).text() < $(y).text() ? -1 : 1;
				}));
			 });  
			 $('#removeComp').click(function() {  
				!$('#selected-components option:selected').remove().appendTo('#components-to-select'); 
				$("#components-to-select").html($('#components-to-select option').sort(function(x, y) {
					return $(x).text() < $(y).text() ? -1 : 1;
				}));
			 });
			 $('#addAllComp').click(function() {  
				!$('#components-to-select option').remove().appendTo('#selected-components'); 
				$("#selected-components").html($('#selected-components option').sort(function(x, y) {
					return $(x).text() < $(y).text() ? -1 : 1;
				}));
			 });  
			 $('#removeAllComp').click(function() {  
				!$('#selected-components option').remove().appendTo('#components-to-select'); 
				$("#components-to-select").html($('#components-to-select option').sort(function(x, y) {
					return $(x).text() < $(y).text() ? -1 : 1;
				}));
			 });
			

			 getUsersList();

			$.ajax({
				type: "GET", 
				url : "FirstPageClass",
				data : {
					action : "fetchCurrentUser"
				},
				success : function(responseText) {
					if(responseText.success == true && responseText.result){
						$("#userNamePanel").html(responseText.result);
					}
				}
			});

			$('#signoutIcon').click(function() {
				$.ajax({
					type: "GET", 
					url : "FirstPageClass",
					data : {
						action : "signoutUser"
					},
					beforeSend: function(jqXHR,settings ){
						// Start spinner
						$('#spinner').show();
						$(".cover").show();
					},
					success : function(responseText) {
						if(responseText.success == true){
							$('#spinner').hide();
							$(".cover").hide();
							window.location.replace('login.jsp');
						}else{
							// If success but no data, then redirect to login page
							//Something went wrong message.
							$('#spinner').hide();
							$(".cover").hide();
							alert('Sorry, something went wrong. Please login again and proceed');
							window.location.replace('login.jsp');
						}
					}
				})
			});
			

			$('#fetchComponentsBtn').click(function() {
				
				
				//shubham: insert login details in database
				insertLoginDetails();
				
				var isValid = validateForm();
				if(isValid === false)
					return false;
				var components = [];
				$.each($("#selected-components option"), function(){   	          
					components.push($(this).val());
				});
				components.sort();
				var compostring =  components.join(",");
				var userlist = [];
				$.each($("#users-selected option"), function(){            
					userlist.push($(this).val());
				});
				var userstring =  userlist.join(",");
				var clientName = $('#clientName').val();
				var startDate = $('#startDate').val();
				var endDate = $('#endDate').val();
				var componentPrefix = $('#componentPrefix').val();
				// Hide the generate button if it's already visible.
				$('.generateBtn').hide();
				// Doing POST as the selected data might be huge
				$.ajax({
					
					type: "POST", 
					timeout: 1000*600,
					url : "FirstPageClass",
					data : {
						action : "fetchcomplist",
						componentlist : compostring,
						userlist : userstring,
						clientName: clientName,
						startDate: startDate,
						endDate: endDate,
						componentPrefix: componentPrefix
					},
					dataType: 'json',
					beforeSend: function(jqXHR,settings ){
						// Start spinner
						console.log('in before send');
						$('#spinner').show();
						$(".cover").show();

						// Clear all tables
						$('#apexclasstable').dataTable().fnClearTable();
						$('#profilestable').dataTable().fnClearTable();
						$('#vftable').dataTable().fnClearTable();
						$('#apextriggertable').dataTable().fnClearTable();
						$('#customobjectstable').dataTable().fnClearTable();
						$('#customsettingstable').dataTable().fnClearTable();
						$('#customtabtable').dataTable().fnClearTable();
						$('#staticresourcestable').dataTable().fnClearTable();
						$('#customFieldTable').dataTable().fnClearTable();
						$('#workflowruletable').dataTable().fnClearTable();
						$('#workflowfieldupdatetable').dataTable().fnClearTable();
						$('#workflowoutboundmessagetable').dataTable().fnClearTable();
						$('#workflowalerttable').dataTable().fnClearTable();
						$('#apexComponentTable').dataTable().fnClearTable();
						$('#approvalProcessTable').dataTable().fnClearTable();
						$('#permissionSetTable').dataTable().fnClearTable();
						$('#emailTemplateTable').dataTable().fnClearTable();
						$('#assignmentRuleTable').dataTable().fnClearTable();
						$('#customLabelTable').dataTable().fnClearTable();
						$('#validationruletable').dataTable().fnClearTable();
						$('#recordtypetable').dataTable().fnClearTable();
						$('#sharingRulesTable').dataTable().fnClearTable();
						$('#workflowTaskTable').dataTable().fnClearTable();
						$('#pageLayoutTable').dataTable().fnClearTable();
						$('#businessProcessTable').dataTable().fnClearTable();
						$('#roleTable').dataTable().fnClearTable();
					},
					success : function(responseText) {
						console.log('in success');
						if(typeof responseText.success !== 'undefined' && responseText.success === false){
							//Hide spinner
							$('#spinner').hide();
							$(".cover").hide();
							console.log('in success undefined');
						}
						console.log('in success 2' );
						if(responseText.apexclasslist){
							apexclasstable.clear();
							$.each(responseText.apexclasslist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';
								if(this.createdDate != null) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var jRow = $('<tr>').append('<td> <input type="checkbox" id="apexcheck" name="apexcheck" class="apexcheck" value="' 
								+ this['id']+'" checked/> </td><td>'+ this.name
								+ '</td><td>' + this.status  + '</td><td>' 
								+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
								+ createDate + '</td><td>' 
								+  (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '') + '</td><td>' 
								+  modDate +'</td>');
								apexclasstable = apexclasstable.row.add(jRow);									
							});
							apexclasstable.draw();
						}
					
						if(responseText.apexpagelist){	
							vftable.clear();
							$.each(responseText.apexpagelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
									var jRow = $('<tr>').append('<td> <input type="checkbox" id="vfcheck" name="vfcheck" class="vfcheck" value="' 
									+ this['id']+'" checked/> </td><td>'+ this.name
									+ '</td><td>' + this.description  + '</td><td>' 
									+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
									+ createDate + '</td><td>' 
									+ (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '')  + '</td><td>' 
									+  modDate +'</td>');
									vftable = vftable.row.add(jRow);
							});
							vftable.draw();
						}

						if(responseText.apextriggerlist){
							apextriggertable.clear();
							$.each(responseText.apextriggerlist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var triggerConditions = '';
								if(this.usageBeforeInsert){
								triggerConditions+='BeforeInsert; ';
								}
								if(this.usageBeforeUpdate){
								triggerConditions+='BeforeUpdate; ';	
								}
								if(this.usageBeforeDelete){
									triggerConditions+='BeforeDelete; ';
								}
								if(this.usageAfterInsert){
									triggerConditions+='AfterInsert; ';
								}
								if(this.usageAfterUpdate){
									triggerConditions+='AfterUpdate; ';
								}
								if(this.usageAfterDelete){
									triggerConditions+='AfterDelete; ';
								}
								if(this.usageAfterUndelete){
									triggerConditions+='AfterUndelete; ';
								}
								var jRow = $('<tr>').append('<td> <input type="checkbox" id="triggercheck" name="triggercheck" class="triggercheck" value="' 
										+ this['id']+'" checked/> </td><td>'
										+ this['name'] +'</td><td>' 
							   			+ this['status'] + '</td><td>' 
							   			+ this['tableEnumOrId'] + '</td><td>' 
							   			+ triggerConditions + '</td><td>' 
							   			+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
							   			+ createDate + '</td><td>' 
							   			+ (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '') + '</td><td>' 
							   			+ modDate +'</td>');									
								apextriggertable = apextriggertable.row.add(jRow);
							});
							apextriggertable.draw();
						}
						if(responseText.validationrulelist){
							validationruletable.clear();
							$.each(responseText.validationrulelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var jRow = $('<tr>').append('<td> <input type="checkbox" id="validationcheck" name="validationcheck" class="validationcheck" value="' 
							   			+ this['id']+'" checked/> </td><td>'+ this['validationName'] +'</td><td>' 
							   			+ this['active'] + '</td><td>' + this.description  + '</td><td>' 
							   			 + (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
							   			+ createDate + '</td><td>' 
							   			+  (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '') + '</td><td>' 
							   			+  modDate +'</td>');								
								validationruletable = validationruletable.row.add(jRow);
							});
							validationruletable.draw();
						}

						if(responseText.customfieldlist){
							customFieldTable.clear();
							$.each(responseText.customfieldlist, function() {
								var entityDefinitionLabel = (this.entityDefinition != null && typeof this.entityDefinition != 'undefined') ? this.entityDefinition.label : ''; 
								
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdBy != null && typeof this.createdBy != 'undefined') ? this.createdBy.name : '';
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var jRow = $('<tr>').append('<td> <input type="checkbox" id="customFieldCheck" name="customFieldCheck" class="customFieldCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ this.developerName +'</td><td>'
							    		+ entityDefinitionLabel + '</td><td>'  
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								customFieldTable = customFieldTable.row.add(jRow);
							 });			
							 customFieldTable.draw();
						}
						
						if(responseText.apexcomponentlist){
							$.each(responseText.apexcomponentlist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdBy != null && typeof this.createdBy != 'undefined') ? this.createdBy.name : '';
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var acRow = $('<tr>').append('<td> <input type="checkbox" id="apexComponentCheck" name="apexComponentCheck" class="apexComponentCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ this.masterLabel +'</td><td>'
							    		+ this.name + '</td><td>'
							    		+ this.description + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								apexComponentTable = apexComponentTable.row.add(acRow);
							});
							apexComponentTable.draw();
							//alert(responseText.apexcomponentlist);
						}
						if(responseText.approvalprocesslist){
							$.each(responseText.approvalprocesslist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdByName != null && typeof this.createdByName != 'undefined') ? this.createdByName : '';
								var lastModifiedByName = (this.lastModifiedByName != null && typeof this.lastModifiedByName != 'undefined') ? this.lastModifiedByName : '';
								var fullName = this.fullName.split('|')[0];
								var description = this.fullName.split('|')[1];
								var apRow = $('<tr>').append('<td> <input type="checkbox" id="approvalProcessCheck" name="approvalProcessCheck" class="approvalProcessCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ fullName +'</td><td>'
							    		+ description + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								approvalProcessTable = approvalProcessTable.row.add(apRow);
							});
							approvalProcessTable.draw();
						}
						if(responseText.businessprocesslist){
							$.each(responseText.businessprocesslist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdByName != null && typeof this.createdByName != 'undefined') ? this.createdByName : '';
								var lastModifiedByName = (this.lastModifiedByName != null && typeof this.lastModifiedByName != 'undefined') ? this.lastModifiedByName : '';
								var fullName = this.fullName.split('|')[0];
								var description = this.fullName.split('|')[1];
								var objectName = this.fileName.split('/')[0].split('.')[0];
								var active = this.fileName.split('/')[1];
								
								var bpRow = $('<tr>').append('<td> <input type="checkbox" id="businessProcessCheck" name="businessProcessCheck" class="businessProcessCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ objectName +'</td><td>'
							    		+ fullName +'</td><td>'
							    		+ description + '</td><td>' 
							    		+ active + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								businessProcessTable = businessProcessTable.row.add(bpRow);
							});
							businessProcessTable.draw();
						}
						if(responseText.customsettingslist){
							customsettingstable.clear();
							$.each(responseText.customsettingslist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdByName != null && typeof this.createdByName != 'undefined') ? this.createdByName : '';
								var lastModifiedByName = (this.lastModifiedByName != null && typeof this.lastModifiedByName != 'undefined') ? this.lastModifiedByName : '';
								var fullName = this.fullName.split('|')[0];
								var description = this.fullName.split('|')[1];
								var customSettingType = this.fileName.split('|')[1];
								var label = this.fileName.split('|')[2];
								var CSVisibility = this.fileName.split('|')[3];
								var jRow = $('<tr>').append('<td> <input type="checkbox" id="customsettingcheck" name="customsettingcheck" class="customsettingcheck" value="' 
										+ this.id +'" checked/> </td><td>'
										+ label+ '</td><td>' 
										+ fullName+ '</td><td>' 
										+ description + '</td><td>' 
							    		+ customSettingType + '</td><td>' 
							    		+ CSVisibility + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');							
								customsettingstable = customsettingstable.row.add(jRow);
							});
							customsettingstable.draw();
						}
						if(responseText.customobjectlist){
							customobjecttable.clear();
							$.each(responseText.customobjectlist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdByName != null && typeof this.createdByName != 'undefined') ? this.createdByName : '';
								var lastModifiedByName = (this.lastModifiedByName != null && typeof this.lastModifiedByName != 'undefined') ? this.lastModifiedByName : '';
								var fullName = this.fullName.split('|')[0];
								var description = this.fullName.split('|')[1];
								var label = this.fileName.split('|')[1];
								var jRow = $('<tr>').append('<td> <input type="checkbox" id="customobjcheck" name="customobjcheck" class="customobjcheck" value="' 
										+ this.id+'" checked/> </td><td>'
										+ label+ '</td><td>' 
										+ fullName+ '</td><td>' 
										+ description + '</td><td>' 
										+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');								
								customobjecttable = customobjecttable.row.add(jRow);
							});
							customobjecttable.draw();
						}
						if(responseText.emailtemplatelist){
							$.each(responseText.emailtemplatelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdBy != null && typeof this.createdBy != 'undefined') ? this.createdBy.name : '';
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var etRow = $('<tr>').append('<td> <input type="checkbox" id="emailTemplateCheck" name="emailTemplateCheck" class="emailTemplateCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ this.name +'</td><td>'
							    		+ this.description + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								emailTemplateTable = emailTemplateTable.row.add(etRow);
							});
							emailTemplateTable.draw();
						}

						if(responseText.pagelayoutlist){
							$.each(responseText.pagelayoutlist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdBy != null && typeof this.createdBy != 'undefined') ? this.createdBy.name : '';
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var plRow = $('<tr>').append('<td> <input type="checkbox" id="pageLayoutCheck" name="pageLayoutCheck" class="pageLayoutCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ this.name +'</td><td>'
							    		+ '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								pageLayoutTable = pageLayoutTable.row.add(plRow);
							});
							pageLayoutTable.draw();
						}
						if(responseText.rolelist){
							$.each(responseText.rolelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								

								//var createdByName = (this.createdBy != null && typeof this.createdBy != 'undefined') ? this.createdBy.name : '';
								//var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var plRow = $('<tr>').append('<td> <input type="checkbox" id="roleCheck" name="roleCheck" class="roleCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ this.fullName.split('|')[0] +'</td><td>'
							    		+ this.fullName.split('|')[1] + '</td><td>'
							    		+ this.fullName.split('|')[2] + '</td><td>' 
							    		+ this.createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ this.lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								roleTable = roleTable.row.add(plRow);
							});
							roleTable.draw();
						}

						if(responseText.profilelist){
							$.each(responseText.profilelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdBy != null && typeof this.createdBy != 'undefined') ? this.createdBy.name : '';
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var etRow = $('<tr>').append('<td> <input type="checkbox" id="profileCheck" name="profileCheck" class="profileCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ this.name +'</td><td>'
							    		+ this.description + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								profileTable = profileTable.row.add(etRow);
							});
							profileTable.draw();
						}
						if(responseText.permissionsetlist){
							$.each(responseText.permissionsetlist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdByName != null && typeof this.createdByName != 'undefined') ? this.createdByName : '';
								var lastModifiedByName = (this.lastModifiedByName != null && typeof this.lastModifiedByName != 'undefined') ? this.lastModifiedByName : '';
								var fullName = this.fullName.split('|')[0];
								var description = this.fullName.split('|')[1];
								var apRow = $('<tr>').append('<td> <input type="checkbox" id="permissionSetCheck" name="permissionSetCheck" class="permissionSetCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ fullName +'</td><td>'
							    		+ description + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								permissionSetTable = permissionSetTable.row.add(apRow);
							});
							permissionSetTable.draw();
						}	

						
						
						if(responseText.customtablist){
							customtabtable.clear();
							$.each(responseText.customtablist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var jRow = $('<tr>').append('<td> <input type="checkbox" id="customtabcheck" name="customtabcheck" class="customtabcheck" value="' 
										+ this['id']+'" checked/> </td><td>'+ this.developerName + '</td><td>' 
										+ this.description  + '</td><td>' 
										+ '</td><td>'
										+ this.type  + '</td><td>'  
										+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
										+ createDate + '</td><td>' 
										+  (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '') + '</td><td>' 
										+  modDate +'</td>');							
								customtabtable = customtabtable.row.add(jRow);
							});
							customtabtable.draw();
						}
						
						if(responseText.staticresourcelist){
							staticresourcetable.clear();
							$.each(responseText.staticresourcelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var jRow = $('<tr>').append('<td> <input type="checkbox" id="staticresourcecheck" name="staticresourcecheck" class="staticresourcecheck" value="' 
										+ this['id']+'" checked/> </td><td>'+ this.name
										+ '</td><td>' + this.description  + '</td><td>' 
										+ this.contentType  + '</td><td>' 
										+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
										+ createDate + '</td><td>' 
										+ lastModifiedByName + '</td><td>' 
										+ modDate +'</td>');							
								staticresourcetable = staticresourcetable.row.add(jRow);
							});
							staticresourcetable.draw();
						}
						if(responseText.assignmentrulelist){
							$.each(responseText.assignmentrulelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdBy != null && typeof this.createdBy != 'undefined') ? this.createdBy.name : '';
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var assignmentRuleRow = $('<tr>').append('<td> <input type="checkbox" id="assignmentRuleCheck" name="assignmentRuleCheck" class="assignmentRuleCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ this.name +'</td><td>'
							    		+ this.active + '</td><td>'
							    		+ this.entityDefinition.fullName + '</td><td>'
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								assignmentRuleTable = assignmentRuleTable.row.add(assignmentRuleRow);
							});
							assignmentRuleTable.draw();
						}
						
						if(responseText.workflowrulelist){
							workflowruletable.clear();
							$.each(responseText.workflowrulelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
							   	var jRow = $('<tr>').append('<td> <input type="checkbox" id="workflowrulecheck" name="workflowrulecheck" class="workflowcheck" value="' 
							   			+ this['id']+'" checked/> </td><td>'
										+ this['name'] +'</td><td>' 
										+ this['tableEnumOrId']+'</td><td>' 
										+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
							   			+ createDate + '</td><td>' 
							   			+ (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '') + '</td><td>' 
							   			+ modDate +'</td>');				
								workflowruletable = workflowruletable.row.add(jRow);
							});
							workflowruletable.draw();
						}

						if(responseText.workflowfieldupdatelist){
							workflowfieldupdatetable.clear();
							$.each(responseText.workflowfieldupdatelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {	
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
							   	var jRow = $('<tr>').append('<td> <input type="checkbox" id="workflowfieldupdatecheck" name="workflowfieldupdatecheck" class="workflowfieldupdatecheck" value="' 
							   			+ this['id']+'" checked/> </td><td>'
										+ this['name'] +'</td><td>' 
										+ this['sourceTableEnumOrId']+'</td><td>' 
										+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
							   			+ createDate + '</td><td>' 
							   			+ (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '') + '</td><td>' 
							   			+ modDate +'</td>');				
								workflowfieldupdatetable = workflowfieldupdatetable.row.add(jRow);
							});
							workflowfieldupdatetable.draw();
						}
						
						if(responseText.workflowoutboundmessagelist){
							workflowoutboundmessagetable.clear();
							$.each(responseText.workflowoutboundmessagelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
							   	var jRow = $('<tr>').append('<td> <input type="checkbox" id="workflowoutboundmessagecheck" name="workflowoutboundmessagecheck" class="workflowoutboundmessagecheck" value="' 
							   			+ this['id']+'" checked/> </td><td>'
										+ this['name'] +'</td><td>' 
										+ this.entityDefinition.qualifiedApiName+'</td><td>' 
										+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
							   			+ createDate + '</td><td>' 
							   			+ (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '')+ '</td><td>' 
							   			+ modDate +'</td>');				
								workflowoutboundmessagetable = workflowoutboundmessagetable.row.add(jRow);
							});
							workflowoutboundmessagetable.draw();
						}

						if(responseText.workflowalertlist){
							$.each(responseText.workflowalertlist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
							   	var jRow = $('<tr>').append('<td> <input type="checkbox" id="workflowalertcheck" name="workflowalertcheck" class="workflowalertcheck" value="' 
							   			+ this['id']+'" checked/> </td><td>'
										+ this['developerName'] +'</td><td>' 
										+ this.entityDefinition.qualifiedApiName+'</td><td>' 
										+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
							   			+ createDate + '</td><td>' 
							   			+ (this.lastModifiedBy != null && this.lastModifiedBy != 'undefined' ? this.lastModifiedBy.name : '') + '</td><td>' 
							   			+ modDate +'</td>');				
								workflowalerttable = workflowalerttable.row.add(jRow);
							});
							workflowalerttable.draw();
						}
						
						if(responseText.recordtypelist){
							recordtypetable.clear();
							$.each(responseText.recordtypelist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
							   	var jRow = $('<tr>').append('<td> <input type="checkbox" id="recordtypecheck" name="recordtypecheck" class="recordtypecheckclass" value="' 
							   			+ this['id']+'" checked/> </td><td>'
										+ this.name +'</td><td>'
							    		+ this.description + '</td><td>'
										+ (this.createdBy != null && this.createdBy != 'undefined' ? this.createdBy.name : '') + '</td><td>' 
							   			+ createDate + '</td><td>' 
							   			+ 1 + '</td><td>' 
							   			+ modDate +'</td>');				
							   	recordtypetable = recordtypetable.row.add(jRow);
							});
							recordtypetable.draw();
						}
							
						if(responseText.customlabellist){
							$.each(responseText.customlabellist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdByName != null && typeof this.createdByName != 'undefined') ? this.createdByName : '';
								var lastModifiedByName = (this.lastModifiedByName != null && typeof this.lastModifiedByName != 'undefined') ? this.lastModifiedByName : '';
								var fullName = this.fullName.split('|')[0];
								var category = this.fullName.split('|')[1];
								var description = this.fullName.split('|')[2];
								var value = this.fullName.split('|')[3];
								var apRow = $('<tr>').append('<td> <input type="checkbox" id="customLabelCheck" name="customLabelCheck" class="customLabelCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ fullName +'</td><td>'
							    		+ category + '</td><td>' 
							    		+ description + '</td><td>' 
							    		+ value + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								customLabelTable = customLabelTable.row.add(apRow);
							});
							customLabelTable.draw();
						}
						if(responseText.sharingruleslist){
							$.each(responseText.sharingruleslist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdByName != null && typeof this.createdByName != 'undefined') ? this.createdByName : '';
								var lastModifiedByName = (this.lastModifiedByName != null && typeof this.lastModifiedByName != 'undefined') ? this.lastModifiedByName : '';
								var name = this.fullName.split('.')[1];
								var object = this.fullName.split('.')[0];
								var srRow = $('<tr>').append('<td> <input type="checkbox" id="sharingRulesCheck" name="sharingRulesCheck" class="sharingRulesCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ name +'</td><td>'
							    		+ object +'</td><td>'
							    		+ this.type +'</td><td>'
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								sharingRulesTable = sharingRulesTable.row.add(srRow);
							});
							sharingRulesTable.draw();
						}
						
						if(responseText.workflowtasklist){
							$.each(responseText.workflowtasklist, function() {
								var createdDateTemp;
								var modDateTemp;
								var createDate = '';
								var modDate = '';

								if(this.createdDate != null ) {
									createdDateTemp = new Date(this.createdDate.weekYear, this.createdDate.time.month, this.createdDate.time.date, this.createdDate.time.hours, this.createdDate.time.minutes);
									createDate = (createdDateTemp.getMonth()+1)  +  "/" + createdDateTemp.getDate() +  "/" +  createdDateTemp.getFullYear() + "   " + createdDateTemp.getHours() + "." + createdDateTemp.getMinutes();
								}
								if(this.lastModifiedDate != null ) {
									modDateTemp = new Date(this.lastModifiedDate.weekYear, this.lastModifiedDate.time.month, this.lastModifiedDate.time.date, this.lastModifiedDate.time.hours, this.lastModifiedDate.time.minutes);
									modDate = (modDateTemp.getMonth()+1) + "/" + modDateTemp.getDate() + "/" +  modDateTemp.getFullYear() + "   " + modDateTemp.getHours() + ":" + modDateTemp.getMinutes();
								}
								var createdByName = (this.createdBy != null && typeof this.createdBy != 'undefined') ? this.createdBy.name : '';
								var lastModifiedByName = (this.lastModifiedBy != null && typeof this.lastModifiedBy != 'undefined') ? this.lastModifiedBy.name : '';
								var wtRow = $('<tr>').append('<td> <input type="checkbox" id="workflowTaskCheck" name="workflowTaskCheck" class="workflowTaskCheck" value="' +  this.id + '" checked/> </td><td>'
							    		+ this.fullName.split('.')[1] +'</td><td>'
							    		+ this.fullName.split('.')[0] +'</td><td>'
							    		+ this.subject +'</td><td>'
							    		+ this.priority +'</td><td>'
							    		+ this.entityDefinition.label + '</td><td>' 
							    		+ createdByName + '</td><td>'
							    		+ createDate + '</td><td>' 
							   			+ lastModifiedByName + '</td><td>'
							   			+ modDate +'</td>');
								workflowTaskTable = workflowTaskTable.row.add(wtRow);
							});
							workflowTaskTable.draw();
						}

						$("#componentResultsPanel").show();
						$('.generateBtn').show();
						//Hide spinner
						$('#spinner').hide();
						$(".cover").hide();
						// Enable scroll on tabs
						reAdjust();
					},
					error : function(responseText) {
						//Hide spinner
						$('#spinner').hide();	
						$(".cover").hide();
						//TODO
					}
				});
			});
			validationruletable = $('#validationruletable').DataTable({
								"columnDefs": [
											  	 { "width": "10%", "targets": 0 },
											  	 { "width": "20%", "targets": 1 },
											  	 { "width": "50%", "targets": 2 },
											  	 { "width": "20%", "targets": 3 },
											  	 { "width": "40%", "targets": 4 },
											  	 { "width": "50%", "targets": 5 },
											  	 { "width": "30%", "targets": 6 },
											  	 { "width": "50%", "targets": 7 }
											   ],
								"ordering": true,
								"paging": true,
								"searching": true          	        
							});
			$('#validationcheckall').change(function() {
			    var checked = $(this).is(":checked");
				$("input#validationcheck", validationruletable.rows().nodes()).prop( 'checked', checked );
			});
			
			apexclasstable = $('#apexclasstable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#apexcheck1').change(function() {
			    var checked = $(this).is(":checked");
				$("input.apexcheck", apexclasstable.rows().nodes()).prop( 'checked', checked );
			});

			vftable = $('#vftable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});

			$('#vfapexcheck').change(function() {
			    var checked = $(this).is(":checked");
				$("input.vfcheck", vftable.rows().nodes()).prop( 'checked', checked ); 
			});
			
			apextriggertable = $('#apextriggertable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});
			$('#triggercheckall').change(function() {
				var checked = $(this).is(":checked");
				$("input.triggercheck", apextriggertable.rows().nodes()).prop( 'checked', checked );
			});
			
			customobjecttable = $('#customobjectstable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});			
			$('#customobjectsCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.customobjcheck", customobjecttable.rows().nodes()).prop( 'checked', checked );
			});

			customsettingstable = $('#customsettingstable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});			
			$('#customsettingsCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.customsettingcheck", customsettingstable.rows().nodes()).prop( 'checked', checked );
			});



			
			customtabtable = $('#customtabtable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});			
			$('#customTabCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.customtabcheck", customtabtable.rows().nodes()).prop( 'checked', checked );
			});
			
			staticresourcetable = $('#staticresourcestable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});			
			$('#staticresourcesCheckAll').change(function() {
			var checked = $(this).is(":checked");
				$("input.staticresourcecheck", staticresourcetable.rows().nodes()).prop( 'checked', checked );
			});
					
			customFieldTable = $('#customFieldTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			$('#customFieldCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.customFieldCheck", customFieldTable.rows().nodes()).prop( 'checked', checked );
			});
			
			roleTable = $('#roleTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#roleCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input#roleCheck", roleTable.rows().nodes()).prop( 'checked', checked );
			});	

			pageLayoutTable = $('#pageLayoutTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#pageLayoutCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input#pageLayoutCheck", pageLayoutTable.rows().nodes()).prop( 'checked', checked );
			});

			workflowruletable = $('#workflowruletable').DataTable({				   
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});
			$('#workflowrulecheckall').change(function() {
				var checked = $(this).is(":checked");
				$("input.workflowrulecheck", workflowruletable.rows().nodes()).prop( 'checked', checked );
			});
		
			workflowfieldupdatetable = $('#workflowfieldupdatetable').DataTable({				   
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});
			$('#workflowfieldupdatecheckall').change(function() {
				var checked = $(this).is(":checked");
				$("input.workflowfieldupdatecheck", workflowfieldupdatetable.rows().nodes()).prop( 'checked', checked );
			});
		
			workflowoutboundmessagetable = $('#workflowoutboundmessagetable').DataTable({				   
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});
			$('#workflowoutboundmessagecheckall').change(function() {
				var checked = $(this).is(":checked");
				$("input.workflowoutboundmessagecheck", workflowoutboundmessagetable.rows().nodes()).prop( 'checked', checked );
			});
			
			workflowalerttable = $('#workflowalerttable').DataTable({				   
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});
			$('#workflowalertcheckall').change(function() {
				var checked = $(this).is(":checked");
				$("input.workflowalertcheck", workflowalerttable.rows().nodes()).prop( 'checked', checked );
			});
			
			recordtypetable = $('#recordtypetable').DataTable({				   
				"ordering": true,
				"paging": true,
				"searching": true          	        
			});
			$('#recordtypecheckall').change(function() {
				var checked = $(this).is(":checked");
				$("input#recordtypecheck", recordtypetable.rows().nodes()).prop( 'checked', checked );
			});
			apexComponentTable = $('#apexComponentTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#apexComponentCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.apexComponentCheck", apexComponentTable.rows().nodes()).prop( 'checked', checked );
			});
			
			approvalProcessTable = $('#approvalProcessTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#approvalProcessCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.approvalProcessCheck", approvalProcessTable.rows().nodes()).prop( 'checked', checked );
			});
			
			permissionSetTable = $('#permissionSetTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#permissionSetCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.permissionSetCheck", permissionSetTable.rows().nodes()).prop( 'checked', checked );
			});			

			businessProcessTable = $('#businessProcessTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			$('#businessprocessCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.businessProcessCheck", businessProcessTable.rows().nodes()).prop( 'checked', checked );
			});
			
			

			emailTemplateTable = $('#emailTemplateTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			$('#emailTemplateCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.emailTemplateCheck", emailTemplateTable.rows().nodes()).prop( 'checked', checked );
			});
			

			profileTable = $('#profilestable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});

			$('#profileCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.profileCheck", profileTable.rows().nodes()).prop( 'checked', checked );
			});

			assignmentRuleTable = $('#assignmentRuleTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#assignmentRuleCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.assignmentRuleCheck", assignmentRuleTable.rows().nodes()).prop( 'checked', checked );
			});
			
			customLabelTable = $('#customLabelTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#customLabelCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input.customLabelCheck", customLabelTable.rows().nodes()).prop( 'checked', checked );
				// customFieldTable.draw();
			});

			sharingRulesTable = $('#sharingRulesTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#sharingRulesCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input#sharingRulesCheck", sharingRulesTable.rows().nodes()).prop( 'checked', checked );
			});
			
			workflowTaskTable = $('#workflowTaskTable').DataTable({
				"ordering": true,
				"paging": true,
				"searching": true
			});
			
			$('#workflowTaskCheckAll').change(function() {
			    var checked = $(this).is(":checked");
				$("input#workflowTaskCheck", workflowTaskTable.rows().nodes()).prop( 'checked', checked );
			});
		});
		
		// Add hidden rows of datatable while sending data across to the server
		$('form').submit(function(){
			var isValid = validateForm();
			if(isValid === false)
				return;
			removeHiddenElements();
			
			
			
			
			// Create a hidden element for each type and add values to it.
			var hiddenValidationRule = document.createElement( 'input' );
            hiddenValidationRule.type = 'hidden';
            hiddenValidationRule.name = "hidden_validationCheck";
            var allvals = [];
			$("input:checked.validationcheck", validationruletable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
	        hiddenValidationRule.value = allvals;
            $('form').append(hiddenValidationRule);
            
            //TODO: Check if the input element already exists. If so, add data to same element.
			var hiddenTriggerRule = document.createElement( 'input' );
			hiddenTriggerRule.type = 'hidden';
			hiddenTriggerRule.name = "hidden_triggerCheck";
            var allvals = [];
			$("input:checked.triggercheck", apextriggertable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenTriggerRule.value = allvals;
            $('form').append(hiddenTriggerRule);

            var hiddenCustomField = document.createElement( 'input' );
            hiddenCustomField.type = 'hidden';
            hiddenCustomField.name = "hidden_customFieldCheck";
            allvals = [];
			$("input:checked.customFieldCheck", customFieldTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
	        hiddenCustomField.value = allvals;
            $('form').append(hiddenCustomField);
			
			var hiddenApexClass = document.createElement( 'input' );
            hiddenApexClass.type = 'hidden';
            hiddenApexClass.name = "hidden_apexCheck";
            var allvals = [];
			$("input:checked.apexcheck", apexclasstable.rows().nodes()).each(function(index, nRow){
				//Add to form
	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	            
	        });
	        hiddenApexClass.value = allvals;
            $('form').append(hiddenApexClass);

            
			
			var hiddenApexPage = document.createElement( 'input' );
            hiddenApexPage.type = 'hidden';
            hiddenApexPage.name = "hidden_apexvfpageCheck";
            var allvals = [];
			$("input:checked.vfcheck", vftable.rows().nodes()).each(function(index, nRow){
				//Add to form
	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
	        hiddenApexPage.value = allvals;
            $('form').append(hiddenApexPage);
			
            // Custom Object
            var hiddenCustomObj = document.createElement( 'input' );
            hiddenCustomObj.type = 'hidden';
            hiddenCustomObj.name = "hidden_customObjCheck";
            var allvals = [];
			$("input:checked.customobjcheck", customobjecttable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
	        hiddenCustomObj.value = allvals;
            $('form').append(hiddenCustomObj);
            
            // Custom Tab
            var hiddenCustomTab = document.createElement( 'input' );
            hiddenCustomTab.type = 'hidden';
            hiddenCustomTab.name = "hidden_customTabCheck";
            var allvals = [];
			$("input:checked.customtabcheck", customtabtable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
	        hiddenCustomTab.value = allvals;
            $('form').append(hiddenCustomTab);
            
            // Static Resource
            var hiddenStaticResource = document.createElement( 'input' );
            hiddenStaticResource.type = 'hidden';
            hiddenStaticResource.name = "hidden_staticResourceCheck";
            var allvals = [];
			$("input:checked.staticresourcecheck", staticresourcetable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
	        hiddenStaticResource.value = allvals;
            $('form').append(hiddenStaticResource);
			
			var hiddenWorkflowRule = document.createElement( 'input' );
            hiddenWorkflowRule.type = 'hidden';
            hiddenWorkflowRule.name = "hidden_workflowRuleCheck";
            var allvals = [];
			$("input:checked.workflowcheck", workflowruletable.rows().nodes()).each(function(index, nRow){
	            allvals.push($(nRow).val());
	        });
			hiddenWorkflowRule.value = allvals;
            $('form').append(hiddenWorkflowRule);

            var hiddenWorkflowFieldUpdate = document.createElement( 'input' );
            hiddenWorkflowFieldUpdate.type = 'hidden';
            hiddenWorkflowFieldUpdate.name = "hidden_workflowFieldUpdateCheck";
            var allvals = [];
			$("input:checked.workflowfieldupdatecheck", workflowfieldupdatetable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenWorkflowFieldUpdate.value = allvals;
            $('form').append(hiddenWorkflowFieldUpdate);

            var hiddenWorkflowOutboundMessage = document.createElement( 'input' );
            hiddenWorkflowOutboundMessage.type = 'hidden';
            hiddenWorkflowOutboundMessage.name = "hidden_workflowOutboundMessageCheck";
            var allvals = [];
			$("input:checked.workflowoutboundmessagecheck", workflowoutboundmessagetable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenWorkflowOutboundMessage.value = allvals;
            $('form').append(hiddenWorkflowOutboundMessage);
            
            var hiddenWorkflowAlert = document.createElement( 'input' );
            hiddenWorkflowAlert.type = 'hidden';
            hiddenWorkflowAlert.name = "hidden_workflowAlertCheck";
            var allvals = [];
			$("input:checked.workflowalertcheck", workflowalerttable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenWorkflowAlert.value = allvals;
            $('form').append(hiddenWorkflowAlert);
			var hiddenRecordType = document.createElement( 'input' );
			hiddenRecordType.type = 'hidden';
			hiddenRecordType.name = "hidden_recordTypeCheck";
            var allvals = [];
			$("input:checked#recordtypecheck", recordtypetable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenRecordType.value = allvals;
            $('form').append(hiddenRecordType);

            var hiddenProfiles = document.createElement( 'input' );
            hiddenProfiles.type = 'hidden';
            hiddenProfiles.name = "hidden_profileCheck";
            var allvals = [];
			$("input:checked.profileCheck", profileTable.rows().nodes()).each(function(index, nRow){

	            allvals.push($(nRow).val());
	            
	        });
	        hiddenProfiles.value = allvals;
            $('form').append(hiddenProfiles);

			var hiddenApexComponent = document.createElement( 'input' );
            hiddenApexComponent.type = 'hidden';
            hiddenApexComponent.name = "hidden_apexComponentCheck";
            allvals = [];
			$("input:checked.apexComponentCheck", apexComponentTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenApexComponent.value = allvals;
            $('form').append(hiddenApexComponent);
            
            var hiddenApprovalProcess = document.createElement( 'input' );
            hiddenApprovalProcess.type = 'hidden';
            hiddenApprovalProcess.name = "hidden_approvalProcessCheck";
            allvals = [];
			$("input:checked.approvalProcessCheck", approvalProcessTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenApprovalProcess.value = allvals;
			$('form').append(hiddenApprovalProcess);
			
			var hiddenPermissionSet = document.createElement( 'input' );
            hiddenPermissionSet.type = 'hidden';
            hiddenPermissionSet.name = "hidden_permissionSetCheck";
            allvals = [];
			$("input:checked.permissionSetCheck", permissionSetTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenPermissionSet.value = allvals;
			$('form').append(hiddenPermissionSet);

			var hiddenbusinessProcess = document.createElement( 'input' );
            hiddenbusinessProcess.type = 'hidden';
            hiddenbusinessProcess.name = "hidden_businessProcessCheck";
            allvals = [];
			$("input:checked.businessProcessCheck", businessProcessTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenbusinessProcess.value = allvals;
			$('form').append(hiddenbusinessProcess);

			var hiddenRole = document.createElement( 'input' );
            hiddenRole.type = 'hidden';
            hiddenRole.name = "hidden_roleCheck";
            allvals = [];
			$("input:checked#roleCheck", roleTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenRole.value = allvals;
            $('form').append(hiddenRole);
            
            var hiddenEmailTemplate = document.createElement( 'input' );
            hiddenEmailTemplate.type = 'hidden';
            hiddenEmailTemplate.name = "hidden_emailTemplateCheck";
            allvals = [];
			$("input:checked.emailTemplateCheck", emailTemplateTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenEmailTemplate.value = allvals;
            $('form').append(hiddenEmailTemplate);


            var hiddenCustomSetting = document.createElement( 'input' );
            hiddenCustomSetting.type = 'hidden';
            hiddenCustomSetting.name = "hidden_customSettingCheck";
            allvals = [];
			$("input:checked.customsettingcheck", customsettingstable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenCustomSetting.value = allvals;
            $('form').append(hiddenCustomSetting);

            
            var hiddenPageLayout = document.createElement( 'input' );
            hiddenPageLayout.type = 'hidden';
            hiddenPageLayout.name = "hidden_pageLayoutCheck";
            allvals = [];
			$("input:checked#pageLayoutCheck", pageLayoutTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenPageLayout.value = allvals;
            $('form').append(hiddenPageLayout);
            

            var hiddenAssignmentRule = document.createElement( 'input' );
            hiddenAssignmentRule.type = 'hidden';
            hiddenAssignmentRule.name = "hidden_assignmentRuleCheck";
            allvals = [];
			$("input:checked.assignmentRuleCheck", assignmentRuleTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenAssignmentRule.value = allvals;
            $('form').append(hiddenAssignmentRule);
            
            var hiddenCustomLabel = document.createElement( 'input' );
            hiddenCustomLabel.type = 'hidden';
            hiddenCustomLabel.name = "hidden_customLabelCheck";
            allvals = [];
			$("input:checked.customLabelCheck", customLabelTable.rows().nodes()).each(function(index, nRow){
				//Add to form

	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenCustomLabel.value = allvals;
			$('form').append(hiddenCustomLabel);


			var hiddenSharingRule = document.createElement( 'input' );
            hiddenSharingRule.type = 'hidden';
            hiddenSharingRule.name = "hidden_sharingRulesCheck";
            allvals = [];
			$("input:checked#sharingRulesCheck", sharingRulesTable.rows().nodes()).each(function(index, nRow){
				//Add to form
	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenSharingRule.value = allvals;
			$('form').append(hiddenSharingRule);
			
			var hiddenWorkflowTask = document.createElement( 'input' );
			hiddenWorkflowTask.type = 'hidden';
            hiddenWorkflowTask.name = "hidden_workflowTaskCheck";
            allvals = [];
			$("input:checked#workflowTaskCheck", workflowTaskTable.rows().nodes()).each(function(index, nRow){
				//Add to form
	            //Assuming there is one input per row
	            allvals.push($(nRow).val());
	        });
			hiddenWorkflowTask.value = allvals;
            $('form').append(hiddenWorkflowTask);

		});
		
		function removeHiddenElements(){
			$('form').find('input[type="hidden"][name^="hidden_"]').remove();
		}
		
		
		function insertLoginDetails(){
			
			$.ajax({
				type: "POST", 
				url : 'FirstPageClass',
				data : {
					action : "insertUserDetails",
					username:$('#userNamePanel').text(),
					clientName:$('#clientName').val(),
					projectName:$('#projectName').val(),						
				}
			});
			
		}
		


		function validateForm(){
			// Clear error message.
			$('#errorMsg').html('').hide();
			var clientName = $('#clientName').val();
			if(isNullOrUndefined(clientName) || clientName.trim() === ''){
				$('#errorMsg').html('Please enter client name').show();
				return false;
			}
			var projName = $('#projectName').val();
			if(isNullOrUndefined(projName) || projName.trim() === ''){
				$('#errorMsg').html('Please enter project name').show();
				return false;
			}
			var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}$/;
			var startDate = $('#startDate').val();
			if(isNullOrUndefined(startDate) || startDate.trim() === '' || dateReg.test(startDate) === false){
				$('#errorMsg').html('Please enter a valid Start Date').show();
				return false;
			}
			var endDate = $('#endDate').val();
			if(!isNullOrUndefined(endDate) && endDate.trim() != '' && dateReg.test(endDate) === false){
				$('#errorMsg').html('Please enter a valid End Date').show();
				return false;
			}
			if(!isNullOrUndefined(endDate) && endDate.trim() != '' && Date.parse(endDate) < Date.parse(startDate)){
				$('#errorMsg').html('End Date cannot be less than Start Date').show();
				return false;
			}
			if( $("#selected-components option").length == 0 ) {
				$('#errorMsg').html('At least one component must be selected').show();
				return false;
			}
			return true;
		}
		
		function isNullOrUndefined(toCheck){
	        if(toCheck == null || typeof toCheck == 'undefined')
	            return true;
	        return false;
	    }
		
		var widthOfList = function() {
		    var itemsWidth = 0;
		    $('.list li').each(function() {
		        var itemWidth = $(this).outerWidth();
		        itemsWidth += itemWidth;
		    });
		    return itemsWidth;
		};
		var widthOfHidden = function(scrollBarWidths) {
		    return (($('.wrapper').outerWidth()) - widthOfList() - getLeftPosi()) - scrollBarWidths;
		};
		var widthOfWrapper = function() {
		    return $('.wrapper').outerWidth();
		}
		var getLeftPosi = function() {
		    return $('.list').position().left;
		};
		var reAdjust = function() {
		    if (($('.wrapper').outerWidth()) < widthOfList()) {
		        $('.scroller-right').show();
		    } else {
		        $('.scroller-right').hide();
		    }
		    if (getLeftPosi() < 0) {
		        $('.scroller-left').show();
		    } else {
		        $('.item').animate({
		            left: "-=" + getLeftPosi() + "px"
		        }, 'slow');
		        $('.scroller-left').hide();
		    }
		}
	</script>
</body>
</html>