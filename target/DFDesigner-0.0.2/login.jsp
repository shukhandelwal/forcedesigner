<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html lang="en-us">
    <meta charset="utf-8" />
    <head>
        <title>Deloitte Force Designer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.ico" type="image/gif" sizes="16x16">

        <link type="text/css" href="css/bootstrap.min.css" rel="stylesheet">        
        <link type="text/css" href="css/font-awesome.min.css" rel="stylesheet">
        <link type="text/css" href="css/loginCSS.css" rel="stylesheet">

    </head>
    <body>
        <header>
            
        </header>
        <div class="container" >
            <div class="card card-container">
                <div class="image-container row">
                    <img id="dd-logo"
                        class="dd-logo" src="img/DD_RGB_white-black.png"></img>
                </div>
                <div class="header-content">
                    <img id="profile-img" class="profile-img-card"
                        src="img/DFDesignerLogo_transparent.png"></img> 
                        <!--<h2>Deloitte Force Designer</h2>-->
                </div>
                
                
                <div class="clear"></div>
                <div id="response-msg" class="alert alert-danger"
                        style="display: none;"></div>

                <div id="response-msg-oauth" class="alert alert-danger"
                        style="display: none;"></div>
                        
                <div id ="normalDiv">
                    <p id="profile-name" class="profile-name-card">
                        
                        Login with Salesforce
                    </p>
                    <form class="form-signin" name="normalForm">
                        <span id="reauth-email" class="reauth-email"></span> 
                        
                        <input type="text" id="inputLogin" class="form-control"
                        placeholder="Username" required autofocus /> 

                        <input
                        type="password" id="inputPassword" class="form-control"
                        placeholder="Password" required /> 

                        <select
                        id="inputEnvironment" class="form-control" onChange="showHideMyDomainInput()" required>
                            <option value="">&nbsp; --Environment--</option>
                            <option value="Production">Production</option>
                            <option value="Sandbox">Sandbox</option>
                            <option value="MyDomain">My Domain</option>
                        </select>

                        <input type="text" id="inputDomain" class="form-control"
                            placeholder="Domain URL" style="display:none"/> 

                        <button class="btn btn-lg btn-primary btn-block btn-signin"
                        type="submit" id="submitBtn">Log In</button>
                    
                        <!--<button class="btn btn-lg btn-primary btn-block btn-signin"
                             id="oauth2loginSwitch">Switch to OAuth 2.0 Login</button>  -->
                    </form>
                </div>


                <div id ="oauthDiv" hidden="true">
                
                    <p id="profile-name" class="profile-name-card">Login with OAuth 2.0</p>
                    <form class="oauth-form-signin" name="oauthForm">
                    
                      <input type="text" id="inputClientId" class="form-control" placeholder="Client Id/Consumer Key" required autofocus>   
                      <input type="text" id="inputConsumerSecret" class="form-control" placeholder="Consumer Secret" required autofocus>
                        <select
                        id="inputEnvironmentOauth" class="form-control" required>
                            <option value="">&nbsp; --Login Environment--</option>
                            <option value="Production">Production</option>
                            <option value="Sandbox">Sandbox</option>
                        </select>
                        <p> Ensure that https://deloitteforce-designer-dev.herokuapp.com/OAuth2Authorization is added as Callback URL in Connected App   </p> 
                        <button class="btn btn-lg btn-primary btn-block btn-signin"
                            type="submit" id="oauth2login">Log In</button>
                       
                        <button class="btn btn-lg btn-primary btn-block btn-signin"
                             id="normalloginSwitch">Switch to Salesforce Login</button>  
                                
                    </form>
                        
                </div>
                
                
                <!-- /form -->
                <br/>
            </div>
            <!-- /card-container -->
        </div>
        <!-- /container -->


        <div id="spinner" class="spinner" style="display: none;">
            <i class="fa fa-spinner fa-pulse fa-4x"></i>
        </div>
        <div class="cover" style="display: none;"/>    
        <!-- /container -->

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript">



        var j$ = jQuery.noConflict();

        function showHideMyDomainInput() {
            if (j$("#inputEnvironment").val() == "MyDomain") {
                j$("#inputDomain").show();
            }
            else {
                j$("#inputDomain").hide();
                j$("#inputDomain").val("");
            }

        }


j$(document).ready(function() {
    j$('button[id="normalloginSwitch"]').click(function(){
        j$('div[id="oauthDiv"]').hide();
        j$('div[id="normalDiv"]').show();
    });        
    j$('button[id="oauth2loginSwitch"]').click(function(){
       j$('div[id="normalDiv"]').hide();
       j$('div[id="oauthDiv"]').show();
    });
    j$('.form-signin').submit(function(ev) {
        ev.preventDefault();
        j$('#response-msg').hide();
        // j$('.form-signin').submit();
        j$.ajax({
            type: "POST",
            url: 'MainClassHeroku',
            data: {
                userName: j$('#inputLogin').val(),
                password: j$('#inputPassword').val(),
                environment: j$('#inputEnvironment').val(),
                domainURL: j$('#inputDomain').val()
            },
            dataType: 'json',
            beforeSend: function(jqXHR,settings ){
                showSpinner();
            },
            success: function(response) {
                if (response.success == true) {
                    window.location.replace('FirstPage.jsp');
                } else {
                    j$('#response-msg').text(response.msg);
                    j$('#response-msg').show();
                }
                hideSpinner();
            }
        });
    });
    j$('.oauth-form-signin').submit(function(ev) {
        ev.preventDefault();
        j$('#response-msg-oauth').hide();
        // j$('.oauth-form-signin').submit();
        j$.ajax({
            type: "POST",
            url: 'OAuth2Initialization',
            data: {
                clientId: j$('#inputClientId').val(),
                consumerSecret: j$('#inputConsumerSecret').val(),
                environmentOauth: j$('#inputEnvironmentOauth').val()
            },
            dataType: 'json',
            beforeSend: function(jqXHR,settings ){
                showSpinner();
            },
            success: function(response) {
                if (response.success == true) {
                    window.location.replace(response.msg);
                } else {
                    j$('#response-msg-oauth').text(response.msg);
                    j$('#response-msg-oauth').show();
                }
                hideSpinner();
            }
        });
    });
    
});
    function showSpinner(){
        // Start spinner
        j$('#spinner').show();
        j$(".cover").show();
    }
    function hideSpinner(){
        // Start spinner
        j$('#spinner').hide();
        j$(".cover").hide();
    }
        </script>
    </body>
</html>