package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.WorkflowOutboundMessage;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDWorkflowOutboundMessage extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDWorkflowOutboundMessage.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDWorkflowOutboundMessage() {
		type = DDConstants.TYPE_WORKFLOW_OUTBOUNDMESSAGE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> queryRecords = util.queryRecords(toolingConnection, soql);
		if (queryRecords != null && !queryRecords.isEmpty()) {
			List<WorkflowOutboundMessage> workflowList = DDUtilClass.generateWorkflowOutboundMessageList(queryRecords);
			saveToSession(csd, workflowList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_OUTBOUNDMESSAGE, workflowList);
			logger.fine("DFDWorkflowOutboundMessage>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_OUTBOUNDMESSAGE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<WorkflowOutboundMessage> apexComponentObject) {
		csd.setWorkflowOutboundMessageList(new Gson().toJson(apexComponentObject));
	}
}
