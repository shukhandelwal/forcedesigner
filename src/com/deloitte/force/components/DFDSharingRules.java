package com.deloitte.force.components;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.metadata.FileProperties;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDSharingRules extends DFDMetadataComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDSharingRules.class.getName());
	
	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDSharingRules() {
		type = DDConstants.TYPE_SHARING_RULE;
		jsonResult = new JSONObject();
	}
	
	@Override
	public JSONObject getComponents(Map<String, String> soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<FileProperties> sharingRulesObject = util.queryRecordsMetadataComponents(metadataConnection, soql,
				DDConstants.TYPE_SHARING_OWNER_RULE, "fetch");
		sharingRulesObject.addAll(util.queryRecordsMetadataComponents(metadataConnection, soql,DDConstants.TYPE_SHARING_CRITERIA_RULE, "fetch"));
		if (sharingRulesObject != null && !sharingRulesObject.isEmpty()) {
			List<FileProperties> sharingRulelist = DDUtilClass.generateSharingRulesList(sharingRulesObject,
					metadataConnection, soql, "fetch");
			saveToSession(csd, sharingRulelist);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_SHARING_RULE, sharingRulelist);
			logger.info("DFDSharingRule>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_SHARING_RULE, null);
		}
		return jsonResult;
	}
	
	public void saveToSession(ComponentSessionDetails csd, List<FileProperties> sharingRulesObject) {
		csd.setSharingRulesList(new Gson().toJson(sharingRulesObject));
	}
}
