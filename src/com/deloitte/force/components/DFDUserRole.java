package com.deloitte.force.components;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.metadata.FileProperties;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDUserRole extends DFDMetadataComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDApprovalProcess.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDUserRole() {
		type = DDConstants.TYPE_USER_ROLE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(Map<String, String> soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<FileProperties> userRoleObject = util.queryRecordsMetadataComponents(metadataConnection, soql,
				DDConstants.TYPE_USER_ROLE, "fetch");
		

		if (userRoleObject != null && !userRoleObject.isEmpty()) {
			List<FileProperties> userrolelist = DDUtilClass.generateRoleList(userRoleObject,
					metadataConnection, soql, "fetch");
			saveToSession(csd, userrolelist);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_USER_ROLE, userrolelist);
			logger.info("DFDUserRole>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_USER_ROLE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<FileProperties> roleObject) {
		csd.setRoleList(new Gson().toJson(roleObject));
	}
}
