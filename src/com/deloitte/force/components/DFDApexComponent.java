package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.ApexComponent;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDApexComponent extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDApexComponent.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDApexComponent() {
		type = DDConstants.TYPE_APEX_COMPONENT;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> apexComponentObject = util.queryRecords(toolingConnection, soql);
		if (apexComponentObject != null && !apexComponentObject.isEmpty()) {
			List<ApexComponent> apexComponentlist = DDUtilClass.generateApexComponentList(apexComponentObject);
			saveToSession(csd, apexComponentlist);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_COMPONENT, apexComponentlist);
			logger.fine("DFDApexComponents>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_COMPONENT, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<ApexComponent> apexComponentObject) {
		csd.setApexComponentList(new Gson().toJson(apexComponentObject));
	}
}
