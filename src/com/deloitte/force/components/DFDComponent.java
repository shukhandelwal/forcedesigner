package com.deloitte.force.components;

import com.sforce.soap.tooling.SoapConnection;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public abstract class DFDComponent {

	public String type;
	public JSONObject jsonResult;
	public SoapConnection toolingConnection;

	public abstract JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException;

	public JSONObject getResult() {
		return jsonResult;
	}

	public void setResult(JSONObject result) {
		this.jsonResult = result;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SoapConnection getToolingConnection() {
		return toolingConnection;
	}

	public void setToolingConnection(SoapConnection toolingConnection) {
		this.toolingConnection = toolingConnection;
	}

}
