package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.WorkflowFieldUpdate;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDWorkflowFieldUpdate extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDWorkflowFieldUpdate.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDWorkflowFieldUpdate() {
		type = DDConstants.TYPE_WORKFLOW_FIELDUPDATE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> queryRecords = util.queryRecords(toolingConnection, soql);
		if (queryRecords != null && !queryRecords.isEmpty()) {
			List<WorkflowFieldUpdate> workflowList = DDUtilClass.generateWorkflowFieldUpdateList(queryRecords);
			saveToSession(csd, workflowList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_FIELDUPDATE, workflowList);
			logger.fine("DFDWorkflowFieldUpdate>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_FIELDUPDATE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<WorkflowFieldUpdate> apexComponentObject) {
		csd.setWorkflowFieldUpdateList(new Gson().toJson(apexComponentObject));
	}
}
