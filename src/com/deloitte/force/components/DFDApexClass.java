package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.ApexClass;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDApexClass extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDApexClass.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDApexClass() {
		type = DDConstants.TYPE_APEX_CLASS;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> apexClasssObject = util.queryRecords(toolingConnection, soql);
		if (apexClasssObject != null && !apexClasssObject.isEmpty()) {
			List<ApexClass> apexClassList = DDUtilClass.generateApexClassList(apexClasssObject);
			saveToSession(csd, apexClassList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_CLASS, apexClassList);
			logger.fine("DFDApexClass>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_CLASS, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<ApexClass> apexComponentObject) {
		csd.setApexClassList(new Gson().toJson(apexComponentObject));
	}
}
