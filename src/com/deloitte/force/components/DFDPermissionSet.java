package com.deloitte.force.components;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.tooling.ApexClass;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDPermissionSet extends DFDMetadataComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDPermissionSet.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDPermissionSet() {
		type = DDConstants.TYPE_PERMISSION_SET;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(Map<String, String> soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<FileProperties> permissionSetObject = util.queryRecordsMetadataComponents(metadataConnection, soql,
				DDConstants.TYPE_PERMISSION_SET, "fetch");

		if (permissionSetObject != null && !permissionSetObject.isEmpty()) {
			List<FileProperties> permissionSetlist = DDUtilClass.generatePermissionSetList(permissionSetObject,
					metadataConnection, soql, "fetch");
			saveToSession(csd, permissionSetlist);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_PERMISSION_SET, permissionSetlist);
			logger.info("DFDPermissionSet>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_PERMISSION_SET, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<FileProperties> apexComponentObject) {
		csd.setPermissionSetList(new Gson().toJson(apexComponentObject));
	}
}
