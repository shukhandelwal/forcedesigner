package com.deloitte.force.components;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.tooling.ApexClass;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDApprovalProcess extends DFDMetadataComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDApprovalProcess.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDApprovalProcess() {
		type = DDConstants.TYPE_APPROVAL_PROCESS;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(Map<String, String> soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<FileProperties> approvalProcessObject = util.queryRecordsMetadataComponents(metadataConnection, soql,
				DDConstants.TYPE_APPROVAL_PROCESS, "fetch");

		if (approvalProcessObject != null && !approvalProcessObject.isEmpty()) {
			List<FileProperties> approvalProcesslist = DDUtilClass.generateApprovalProcessList(approvalProcessObject,
					metadataConnection, soql, "fetch");
			saveToSession(csd, approvalProcesslist);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APPROVAL_PROCESS, approvalProcesslist);
			logger.info("DFDApprovalProcess>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APPROVAL_PROCESS, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<FileProperties> apexComponentObject) {
		csd.setApprovalProcessList(new Gson().toJson(apexComponentObject));
	}
}
