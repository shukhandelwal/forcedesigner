package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.RecordType;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDRecordType extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDRecordType.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDRecordType() {
		type = DDConstants.TYPE_RECORDTYPE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> queryRecords = util.queryRecords(toolingConnection, soql);
		if (queryRecords != null && !queryRecords.isEmpty()) {
			List<RecordType> recordTypeList = DDUtilClass.generateRecordTypeList(queryRecords);
			saveToSession(csd, recordTypeList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_RECORDTYPE, recordTypeList);
			logger.fine("DFDRecordType>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_RECORDTYPE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<RecordType> apexComponentObject) {
		csd.setRecordTypeList(new Gson().toJson(apexComponentObject));
	}
}
