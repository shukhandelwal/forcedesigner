package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;
import java.util.Map;

import com.google.gson.Gson;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.metadata.CustomObject;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;
import com.sforce.soap.metadata.ReadResult;
import com.sforce.soap.metadata.Metadata;
import com.sforce.soap.metadata.*;

import net.sf.json.JSONObject;
public class DFDCustomSettings extends DFDMetadataComponent {
	private final static Logger logger = Logger.getLogger(DFDCustomObjects.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDCustomSettings() {
		type = DDConstants.JSON_KEY_TYPE_CUSTOM_OBJECT;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(Map<String, String> soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<FileProperties> customSettingObject = util.queryRecordsMetadataComponents(metadataConnection, soql,
				DDConstants.TYPE_CUSTOM_OBJECT, "fetch");
		if (customSettingObject != null && !customSettingObject.isEmpty()) {
			List<FileProperties> customSettingsList = DDUtilClass.generateCustomSettingList(customSettingObject,metadataConnection, soql, "fetch");
			
			logger.info("==CustomSetting=="+customSettingsList);
			saveToSession(csd, customSettingsList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_CUSTOM_SETTING, customSettingsList);
			logger.fine("DFDCustomObjects>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_PAGE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<FileProperties> apexComponentObject) {
		csd.setCustomSettingsList(new Gson().toJson(apexComponentObject));
	}
}