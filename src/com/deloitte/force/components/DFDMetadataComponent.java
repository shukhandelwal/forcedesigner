package com.deloitte.force.components;

import java.util.Map;

import com.sforce.soap.metadata.MetadataConnection;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public abstract class DFDMetadataComponent {

	public String type;
	public JSONObject jsonResult;
	public MetadataConnection metadataConnection;

	public abstract JSONObject getComponents(Map<String, String> soql, ComponentSessionDetails csd) throws DFDException;

	public JSONObject getResult() {
		return jsonResult;
	}

	public void setResult(JSONObject result) {
		this.jsonResult = result;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public MetadataConnection getMetadataConnection() {
		return metadataConnection;
	}

	public void setMetadataConnection(MetadataConnection metadataConnection) {
		this.metadataConnection = metadataConnection;
	}
}
