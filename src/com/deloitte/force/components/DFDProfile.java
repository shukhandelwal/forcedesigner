package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.Profile;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDProfile extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDProfile.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDProfile() {
		type = DDConstants.TYPE_PROFILE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException  {
		DDUtilClass util = new DDUtilClass();

		List<SObject> profileObject = util.queryRecords(toolingConnection, soql);

		if (profileObject != null && !profileObject.isEmpty()) {
			List<Profile> profileList = DDUtilClass.generateProfileList(profileObject);
			saveToSession(csd, profileList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_PROFILE, profileList);
			logger.info("DFDApprovalProcess>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_PROFILE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<Profile> apexComponentObject) {
		csd.setProfileList(new Gson().toJson(apexComponentObject));
	}
}
