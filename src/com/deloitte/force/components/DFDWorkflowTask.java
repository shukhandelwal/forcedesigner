package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.EmailTemplate;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.WorkflowTask;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDWorkflowTask extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDEmailTemplate.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDWorkflowTask() {
		type = DDConstants.TYPE_WORKFLOW_TASK;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> workflowTaskObject = util.queryRecords(toolingConnection, soql);
		if (workflowTaskObject != null && !workflowTaskObject.isEmpty()) {
			List<WorkflowTask> workflowTasklist = DDUtilClass.generateWorkflowTaskList(workflowTaskObject);
			saveToSession(csd, workflowTasklist);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_TASK, workflowTasklist);
			logger.fine("DFDWorkflowTask>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_TASK, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<WorkflowTask> workflowTaskObject) {
		csd.setWorkflowTaskList(new Gson().toJson(workflowTaskObject));
	}
}
