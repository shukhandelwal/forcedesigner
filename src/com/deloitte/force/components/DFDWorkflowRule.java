package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.WorkflowRule;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDWorkflowRule extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDWorkflowRule.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDWorkflowRule() {
		type = DDConstants.TYPE_WORKFLOW_RULE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> queryRecords = util.queryRecords(toolingConnection, soql);
		if (queryRecords != null && !queryRecords.isEmpty()) {
			List<WorkflowRule> workflowRuleList = DDUtilClass.generateWorkflowRuleList(queryRecords);
			saveToSession(csd, workflowRuleList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_RULE, workflowRuleList);
			logger.fine("DFDWorkflowRule>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_RULE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<WorkflowRule> apexComponentObject) {
		csd.setWorkflowRuleList(new Gson().toJson(apexComponentObject));
	}
}
