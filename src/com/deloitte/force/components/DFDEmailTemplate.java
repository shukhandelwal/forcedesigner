package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.EmailTemplate;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDEmailTemplate extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDEmailTemplate.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDEmailTemplate() {
		type = DDConstants.TYPE_EMAIL_TEMPLATE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> emailTemplateObject = util.queryRecords(toolingConnection, soql);
		if (emailTemplateObject != null && !emailTemplateObject.isEmpty()) {
			List<EmailTemplate> emailTemplatelist = DDUtilClass.generateEmailTemplateList(emailTemplateObject);
			saveToSession(csd, emailTemplatelist);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_EMAIL_TEMPLATE, emailTemplatelist);
			logger.fine("DFDEmailTemplates>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_EMAIL_TEMPLATE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<EmailTemplate> apexComponentObject) {
		csd.setEmailTemplateList(new Gson().toJson(apexComponentObject));
	}
}
