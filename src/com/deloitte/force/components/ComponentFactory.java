package com.deloitte.force.components;

import com.tooling.custom.DDConstants;

public class ComponentFactory {

	public static DFDComponent createComponent(String type) {
		DFDComponent component = null;
		if (DDConstants.TYPE_APEX_CLASS.equals(type)) {
			component = new DFDApexClass();
		} else if (DDConstants.TYPE_APEX_TRIGGER.equals(type)) {
			component = new DFDApexTrigger();
		} else if (DDConstants.TYPE_CUSTOM_FIELD.equals(type)) {
			component = new DFDCustomField();
		} else if (DDConstants.TYPE_VALIDATION_RULE.equals(type)) {
			component = new DFDValidationRule();
		} else if (DDConstants.TYPE_APEX_PAGE.equals(type)) {
			component = new DFDVisualForceClass();
		} else if (DDConstants.TYPE_CUSTOM_TAB.equals(type)) {
			component = new DFDCustomTab();
		} else if (DDConstants.TYPE_STATIC_RESOURCE.equals(type)) {
			component = new DFDStaticResource();
		} else if (DDConstants.TYPE_WORKFLOW_RULE.equals(type)) {
			component = new DFDWorkflowRule();
		} else if (DDConstants.TYPE_WORKFLOW_FIELDUPDATE.equals(type)) {
			component = new DFDWorkflowFieldUpdate();
		} else if (DDConstants.TYPE_WORKFLOW_OUTBOUNDMESSAGE.equals(type)) {
			component = new DFDWorkflowOutboundMessage();
		} else if (DDConstants.TYPE_WORKFLOW_ALERT.equals(type)) {
			component = new DFDWorkflowAlert();
		} else if (DDConstants.TYPE_APEX_COMPONENT.equals(type)) {
			component = new DFDApexComponent();
		} else if (DDConstants.TYPE_ASSIGNMENT_RULE.equals(type)) {
			component = new DFDAssignmentRule();
		} else if (DDConstants.TYPE_EMAIL_TEMPLATE.equals(type)) {
			component = new DFDEmailTemplate();
		} else if (DDConstants.TYPE_PROFILE.equals(type)) {
			component = new DFDProfile();
		} else if (DDConstants.TYPE_RECORDTYPE.equals(type)) {
			component = new DFDRecordType();
		} else if (DDConstants.TYPE_WORKFLOW_TASK.equals(type)) {
			component = new DFDWorkflowTask();
		} else if (DDConstants.TYPE_PAGE_LAYOUT.equals(type)) {
			component = new DFDPageLayout();
		}

		return component;
	}

	public static DFDMetadataComponent createMetadataComponent(String type) {
		DFDMetadataComponent metadataComponent = null;
		if (DDConstants.TYPE_APPROVAL_PROCESS.equals(type)) {
			metadataComponent = new DFDApprovalProcess();
		} //Added By Nitin
		else if (DDConstants.TYPE_BUSINESS_PROCESS.equals(type)) {
			metadataComponent = new DFDBusinessProcess();
		} else if (DDConstants.TYPE_CUSTOM_LABEL.equals(type)) {
			metadataComponent = new DFDCustomLabel();
		} else  if (DDConstants.TYPE_SHARING_RULE.equals(type)) {
			metadataComponent = new DFDSharingRules();
		} else if (DDConstants.TYPE_CUSTOM_SETTING.equals(type)) {
			metadataComponent = new DFDCustomSettings();
		}else if (DDConstants.TYPE_CUSTOM_OBJECT.equals(type)) {
			metadataComponent = new DFDCustomObjects();
		} else if (DDConstants.TYPE_USER_ROLE.equals(type)) {
			metadataComponent = new DFDUserRole();
		} else if (DDConstants.TYPE_PERMISSION_SET.equals(type)) {
			metadataComponent = new DFDPermissionSet();
		} 
		return metadataComponent;
	}
}
