package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.WorkflowAlert;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDWorkflowAlert extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDWorkflowAlert.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDWorkflowAlert() {
		type = DDConstants.TYPE_WORKFLOW_ALERT;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> queryRecords = util.queryRecords(toolingConnection, soql);
		if (queryRecords != null && !queryRecords.isEmpty()) {
			List<WorkflowAlert> workflowList = DDUtilClass.generateWorkflowAlertList(queryRecords);
			saveToSession(csd, workflowList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_ALERT, workflowList);
			logger.fine("DFDWorkflowAlert>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_WORKFLOW_ALERT, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<WorkflowAlert> apexComponentObject) {
		csd.setWorkflowAlertList(new Gson().toJson(apexComponentObject));
	}

}
