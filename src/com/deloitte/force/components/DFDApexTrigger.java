package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.ApexTrigger;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDApexTrigger extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDApexTrigger.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDApexTrigger() {
		type = DDConstants.TYPE_APEX_TRIGGER;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> queryRecords = util.queryRecords(toolingConnection, soql);
		if (queryRecords != null && !queryRecords.isEmpty()) {
			List<ApexTrigger> apexTriggerList = DDUtilClass.generateApexTriggerList(queryRecords);
			saveToSession(csd, apexTriggerList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_TRIGGER, apexTriggerList);
			logger.fine("DFDApexTrigger>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_TRIGGER, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<ApexTrigger> apexComponentObject) {
		csd.setApexTriggerList(new Gson().toJson(apexComponentObject));
	}
}
