/**
 * package 
 */
package com.deloitte.force.components;

import java.io.Serializable;

import com.sforce.soap.metadata.MetadataConnection;

/**
 * A bean for storing data in user's session. Rohit: Implements serializable as
 * the session needs to store only serializable records to be "distributable".
 * Here's what it means to be distributable:
 * http://wiki.metawerx.net/wiki/Web.xml.Distributable
 * 
 * @author pratsaxena, rmacherla
 *
 */
@SuppressWarnings("serial")
public class UserSessionDetails implements Serializable {
	public String toolingSessionId;
	public String partnerSessionId;
	public String userName;
	public LoginDetails loginDetails;
	public ComponentSessionDetails componentSessionDetails;

	public ComponentSessionDetails getComponentSessionDetails() {
		return componentSessionDetails;
	}

	public void setComponentSessionDetails(ComponentSessionDetails componentSessionDetails) {
		this.componentSessionDetails = componentSessionDetails;
	}

	public String metadataSessionId;

	public String getMetadataSessionId() {
		return metadataSessionId;
	}

	public void setMetadataSessionId(String metadataSessionId) {
		this.metadataSessionId = metadataSessionId;
	}

	public MetadataConnection getMetadataConnection() {
		return metadataConnection;
	}

	public void setMetadataConnection(MetadataConnection metadataConnection) {
		this.metadataConnection = metadataConnection;
	}

	public MetadataConnection metadataConnection;

	/**
	 * @return the toolingSessionId
	 */
	public String getToolingSessionId() {
		return toolingSessionId;
	}

	/**
	 * @param toolingSessionId
	 *            the toolingSessionId to set
	 */
	public void setToolingSessionId(String toolingSessionId) {
		this.toolingSessionId = toolingSessionId;
	}

	/**
	 * @return the partnerSessionId
	 */
	public String getPartnerSessionId() {
		return partnerSessionId;
	}

	/**
	 * @param partnerSessionId
	 *            the partnerSessionId to set
	 */
	public void setPartnerSessionId(String partnerSessionId) {
		this.partnerSessionId = partnerSessionId;
	}

	/**
	 * @return the loginDetails
	 */
	public LoginDetails getLoginDetails() {
		return loginDetails;
	}

	/**
	 * @param loginDetails
	 *            the loginDetails to set
	 */
	public void setLoginDetails(LoginDetails loginDetails) {
		this.loginDetails = loginDetails;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
