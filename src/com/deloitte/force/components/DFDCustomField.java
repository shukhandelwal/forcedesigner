package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.sforce.soap.tooling.CustomField;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDCustomField extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDCustomField.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDCustomField() {
		type = DDConstants.TYPE_CUSTOM_FIELD;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> customFieldObject = util.queryRecords(toolingConnection, soql);
		if (customFieldObject != null && !customFieldObject.isEmpty()) {
			List<CustomField> customfieldlist = DDUtilClass.generateCustomFieldList(customFieldObject);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_CUSTOM_FIELD, customfieldlist);
			logger.info("DFDCustomField>getComponents> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_CUSTOM_FIELD, null);
		}
		return jsonResult;
	}

}
