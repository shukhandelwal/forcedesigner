package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.Layout;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDPageLayout extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDPageLayout.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDPageLayout() {
		type = DDConstants.TYPE_PAGE_LAYOUT;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> pageLayoutObject = util.queryRecords(toolingConnection, soql);
		
		if (pageLayoutObject != null && !pageLayoutObject.isEmpty()) {
			List<Layout> PageLayoutList = DDUtilClass.generatePageLayoutList(pageLayoutObject);
			saveToSession(csd, PageLayoutList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_PAGE_LAYOUT, PageLayoutList);
			logger.fine("DFDEmailTemplates>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_PAGE_LAYOUT, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<Layout> pageLayoutObject) {
		csd.setPageLayoutList(new Gson().toJson(pageLayoutObject));
	}
}
