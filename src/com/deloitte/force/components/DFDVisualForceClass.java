package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.ApexPage;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDVisualForceClass extends DFDComponent {

	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDVisualForceClass.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDVisualForceClass() {
		type = DDConstants.TYPE_APEX_PAGE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> apexPageObject = util.queryRecords(toolingConnection, soql);
		if (apexPageObject != null && !apexPageObject.isEmpty()) {
			List<ApexPage> apexPageList = DDUtilClass.generateApexPagesList(apexPageObject);
			saveToSession(csd, apexPageList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_PAGE, apexPageList);
			logger.fine("DFDVisualForceClass>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_APEX_PAGE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<ApexPage> apexComponentObject) {
		csd.setVfPageList(new Gson().toJson(apexComponentObject));
	}

}
