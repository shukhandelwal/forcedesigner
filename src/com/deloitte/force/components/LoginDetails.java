package com.deloitte.force.components;

import java.io.Serializable;

import com.sforce.soap.metadata.MetadataConnection;
import com.tooling.custom.LoginLogic;

@SuppressWarnings("serial")
public class LoginDetails implements Serializable {
	public String token;
	public String userName;
	public String userOrganization;
	public String loginResult;
	public String partnerServiceEndpoint;
	public String toolingServiceEndpoint;
	public MetadataConnection metadataConnection;
	public String metadataToken;
	public String metadataServiceEndpoint;
	public String clientId;
	public String consumerSecret;
	public Boolean isProd;
	
	public String getMetadataToken() {
		return metadataToken;
	}

	public void setMetadataToken(String metadataToken) {
		this.metadataToken = metadataToken;
	}

	public String getToken() {
		return token;
	}

	public MetadataConnection getMetadataConnection() {
		return metadataConnection;
	}

	public void setMetadataConnection(MetadataConnection metadataConnection) {
		this.metadataConnection = metadataConnection;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserOrganization() {
		return userOrganization;
	}

	public void setUserOrganization(String userOrganization) {
		this.userOrganization = userOrganization;
	}

	public String getLoginResult() {
		return loginResult;
	}

	public void setLoginResult(String loginResult) {
		this.loginResult = loginResult;
	}

	public String getToolingServiceEndpoint() {
		return toolingServiceEndpoint;
	}

	public void setToolingServiceEndpoint(String serviceEndpoint) {
		this.toolingServiceEndpoint = serviceEndpoint;
	}

	public String getPartnerServiceEndpoint() {
		return partnerServiceEndpoint;
	}

	public void setPartnerServiceEndpoint(String partnerServiceEndpoint) {
		this.partnerServiceEndpoint = partnerServiceEndpoint;
	}

	public String getMetadataServiceEndpoint() {
		return metadataServiceEndpoint;
	}

	public void setMetadataServiceEndpoint(String metadataServiceEndpoint) {
		this.metadataServiceEndpoint = metadataServiceEndpoint;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}

	public Boolean getIsProd() {
		return isProd;
	}

	public void setIsProd(Boolean isProd) {
		this.isProd = isProd;
	}

}
