package com.deloitte.force.components;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Color;

import com.google.gson.Gson;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.tooling.ApexClass;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;
import com.sforce.soap.metadata.ReadResult;
import com.sforce.soap.metadata.Metadata;
import com.sforce.soap.metadata.*;

import net.sf.json.JSONObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

public class DFDBusinessProcess extends DFDMetadataComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDBusinessProcess.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDBusinessProcess() {
		type = DDConstants.TYPE_BUSINESS_PROCESS;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(Map<String, String> soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<FileProperties> businessProcessObject = util.queryRecordsMetadataComponents(metadataConnection, soql,
				DDConstants.TYPE_BUSINESS_PROCESS, "fetch");
		logger.info("DFDBusinessProcess>queryRecords> values :" + businessProcessObject);
		
		if (businessProcessObject != null && !businessProcessObject.isEmpty()) {
			List<FileProperties> businessProcesslist = DDUtilClass.generateBusinessProcessList(businessProcessObject,
					metadataConnection, soql, "fetch");
			saveToSession(csd, businessProcesslist);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_BUSINESS_PROCESS, businessProcesslist);
			logger.info("DFDBusinessProcess>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_BUSINESS_PROCESS, null);
		}
		
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<FileProperties> apexComponentObject) {
		csd.setBusinessProcessList(new Gson().toJson(apexComponentObject));
	}
}