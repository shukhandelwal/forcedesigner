package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.StaticResource;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDStaticResource extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDStaticResource.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDStaticResource() {
		type = DDConstants.TYPE_STATIC_RESOURCE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> staticResourcesObject = util.queryRecords(toolingConnection, soql);
		if (staticResourcesObject != null && !staticResourcesObject.isEmpty()) {
			List<StaticResource> staticResourceList = DDUtilClass.generateStaticResourceList(staticResourcesObject);
			saveToSession(csd, staticResourceList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_STATIC_RESOURCE, staticResourceList);
			logger.fine("DFDStaticResource>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_STATIC_RESOURCE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<StaticResource> apexComponentObject) {
		csd.setStaticResourceList(new Gson().toJson(apexComponentObject));
	}
}
