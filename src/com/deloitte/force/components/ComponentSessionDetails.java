package com.deloitte.force.components;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ComponentSessionDetails implements Serializable {
	public String apexClassList;
	public String apexComponentList;
	public String apexTriggerList;
	public String vfPageList;
	public String approvalProcessList;
	public String roleList;
	public String permissionSetList;
	public String assignmentRuleList;
	public String customObjectList;
	public String customLabelList;
	public String customTabList;
	public String emailTemplateList;
	public String pageLayoutList;
	public String staticResourceList;
	public String validationRuleList;
	public String workflowAlertList;
	public String workflowFieldUpdateList;
	public String workflowOutboundMessageList;
	public String workflowRuleList;
	public String profileList;
	public String recordTypeList;
	public String sharingRulesList;
	public String workflowTaskList;
	public String homePageComponentList;
	public String businessProcessList;
	public String customSettingsList;

	public String getWorkflowTaskList() {
		return workflowTaskList;
	}

	public void setWorkflowTaskList(String workflowTaskList) {
		this.workflowTaskList = workflowTaskList;
	}

	public String getSharingRulesList() {
		return sharingRulesList;
	}

	public void setSharingRulesList(String sharingRulesList) {
		this.sharingRulesList = sharingRulesList;
	}

	public String getApexClassList() {
		return apexClassList;
	}

	public void setApexClassList(String apexClassList) {
		this.apexClassList = apexClassList;
	}

	public String getApexComponentList() {
		return apexComponentList;
	}

	public void setApexComponentList(String apexComponentList) {
		this.apexComponentList = apexComponentList;
	}

	public String getApexTriggerList() {
		return apexTriggerList;
	}

	public void setApexTriggerList(String apexTriggerList) {
		this.apexTriggerList = apexTriggerList;
	}

	public String getVfPageList() {
		return vfPageList;
	}

	public void setVfPageList(String vfPageList) {
		this.vfPageList = vfPageList;
	}

	public String getCustomObjectList() {
		return customObjectList;
	}

	public void setCustomObjectList(String customObjectList) {
		this.customObjectList = customObjectList;
	}

	public String getPermissionSetList() {
		return permissionSetList;
	}

	public void setPermissionSetList(String permissionSetList) {
		this.permissionSetList = permissionSetList;
	}

	public String getCustomSettingsList() {
		return customSettingsList;
	}

	public void setCustomSettingsList(String customSettingList) {
		this.customSettingsList = customSettingList;
	}
	public String getCustomTabList() {
		return customTabList;
	}

	public void setCustomTabList(String customTabList) {
		this.customTabList = customTabList;
	}

	public String getEmailTemplateList() {
		return emailTemplateList;
	}
	
	/*Added by Nitin**/
	public String getBusinessProcessList() {
		return businessProcessList;
	}
	public void setBusinessProcessList(String businessProcessList) {
		this.businessProcessList = businessProcessList;
	}

	public void setEmailTemplateList(String emailTemplateList) {
		this.emailTemplateList = emailTemplateList;
	}

	/**
	 * @return the pageLayoutList
	 */
	public String getPageLayoutList() {
		return pageLayoutList;
	}

	/**
	 * @param pageLayoutList the pageLayoutList to set
	 */
	public void setPageLayoutList(String pageLayoutList) {
		this.pageLayoutList = pageLayoutList;
	}
	public String getStaticResourceList() {
		return staticResourceList;
	}

	public void setStaticResourceList(String staticResourceList) {
		this.staticResourceList = staticResourceList;
	}

	public String getValidationRuleList() {
		return validationRuleList;
	}

	public void setValidationRuleList(String validationRuleList) {
		this.validationRuleList = validationRuleList;
	}

	public String getWorkflowAlertList() {
		return workflowAlertList;
	}

	public void setWorkflowAlertList(String workflowAlertList) {
		this.workflowAlertList = workflowAlertList;
	}

	public String getWorkflowFieldUpdateList() {
		return workflowFieldUpdateList;
	}

	public void setWorkflowFieldUpdateList(String workflowFieldUpdateList) {
		this.workflowFieldUpdateList = workflowFieldUpdateList;
	}

	public String getWorkflowOutboundMessageList() {
		return workflowOutboundMessageList;
	}

	public void setWorkflowOutboundMessageList(String workflowOutboundMessageList) {
		this.workflowOutboundMessageList = workflowOutboundMessageList;
	}

	public String getWorkflowRuleList() {
		return workflowRuleList;
	}

	public void setWorkflowRuleList(String workflowRuleList) {
		this.workflowRuleList = workflowRuleList;
	}

	public String getApprovalProcessList() {
		return approvalProcessList;
	}

	public void setApprovalProcessList(String approvalProcessList) {
		this.approvalProcessList = approvalProcessList;
	}

	/**
	 * @return the roleList
	 */
	public String getRoleList() {
		return roleList;
	}

	/**
	 * @param roleList the roleList to set
	 */
	public void setRoleList(String roleList) {
		this.roleList = roleList;
	}

	public String getAssignmentRuleList() {
		return assignmentRuleList;
	}

	public void setAssignmentRuleList(String assignmentRuleList) {
		this.assignmentRuleList = assignmentRuleList;
	}

	public String getCustomLabelList() {
		return customLabelList;
	}

	public void setCustomLabelList(String customLabelList) {
		this.customLabelList = customLabelList;
	}

	public String getProfileList() {
		return profileList;
	}

	public void setProfileList(String profileList) {
		this.profileList = profileList;
	}

	public String getRecordTypeList() {
		return recordTypeList;
	}

	public void setRecordTypeList(String recordTypeList) {
		this.recordTypeList = recordTypeList;
	}
	
}
