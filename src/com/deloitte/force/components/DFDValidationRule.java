package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.ValidationRule;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDValidationRule extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDValidationRule.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDValidationRule() {
		type = DDConstants.TYPE_VALIDATION_RULE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> validationRuleObject = util.queryRecords(toolingConnection, soql);
		if (validationRuleObject != null && !validationRuleObject.isEmpty()) {
			List<ValidationRule> validationRuleList = DDUtilClass.generateValidationRuleList(validationRuleObject);
			saveToSession(csd, validationRuleList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_VALIDATION_RULE, validationRuleList);
			logger.fine("DFDValidationRule>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_VALIDATION_RULE, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<ValidationRule> apexComponentObject) {
		csd.setValidationRuleList(new Gson().toJson(apexComponentObject));
	}
}
