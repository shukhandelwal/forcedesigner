package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.tooling.CustomTab;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDCustomTab extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDCustomTab.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDCustomTab() {
		type = DDConstants.TYPE_CUSTOM_TAB;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> customTabssObject = util.queryRecords(toolingConnection, soql);
		if (customTabssObject != null && !customTabssObject.isEmpty()) {
			List<CustomTab> customTabList = DDUtilClass.generateCustomTabList(customTabssObject);
			saveToSession(csd, customTabList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_CUSTOM_TAB, customTabList);
			logger.fine("DFDCustomTab>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_CUSTOM_TAB, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<CustomTab> apexComponentObject) {
		csd.setCustomTabList(new Gson().toJson(apexComponentObject));
	}
}
