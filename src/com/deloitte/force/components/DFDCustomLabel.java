package com.deloitte.force.components;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.tooling.ApexClass;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDCustomLabel extends DFDMetadataComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDCustomLabel.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDCustomLabel() {
		type = DDConstants.TYPE_CUSTOM_LABEL;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(Map<String, String> soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<FileProperties> customLabelObject = util.queryRecordsMetadataComponents(metadataConnection, soql,
				DDConstants.TYPE_CUSTOM_LABEL, "fetch");

		if (customLabelObject != null && !customLabelObject.isEmpty()) {
			List<FileProperties> customLabelList = DDUtilClass.generateCustomLabelList(customLabelObject,
					metadataConnection, soql, "fetch");
			saveToSession(csd, customLabelList);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_CUSTOM_LABEL, customLabelList);
			logger.info("DFDCustomLabel>getComponents> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_CUSTOM_LABEL, null);
		}
		return jsonResult;
	}

	public void saveToSession(ComponentSessionDetails csd, List<FileProperties> apexComponentObject) {
		csd.setCustomLabelList(new Gson().toJson(apexComponentObject));
	}
}
