package com.deloitte.force.components;

import java.util.List;
import java.util.logging.Logger;

import com.sforce.soap.tooling.AssignmentRule;
import com.sforce.soap.tooling.SObject;
import com.tooling.custom.DDConstants;
import com.tooling.custom.DDUtilClass;
import com.tooling.custom.DFDException;

import net.sf.json.JSONObject;

public class DFDAssignmentRule extends DFDComponent {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DFDAssignmentRule.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public DFDAssignmentRule() {
		type = DDConstants.TYPE_ASSIGNMENT_RULE;
		jsonResult = new JSONObject();
	}

	@Override
	public JSONObject getComponents(String soql, ComponentSessionDetails csd) throws DFDException {
		DDUtilClass util = new DDUtilClass();
		List<SObject> assignmentRuleObject = util.queryRecords(toolingConnection, soql);
		if (assignmentRuleObject != null && !assignmentRuleObject.isEmpty()) {
			List<AssignmentRule> assignmentRulelist = DDUtilClass.generateAssignmentRuleList(assignmentRuleObject);
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_ASSIGNMENT_RULE, assignmentRulelist);
			logger.info("DFDAssignmentRule>queryRecords> values :" + jsonResult.toString());
		} else {
			jsonResult.accumulate(DDConstants.JSON_KEY_TYPE_ASSIGNMENT_RULE, null);
		}
		return jsonResult;
	}

}
