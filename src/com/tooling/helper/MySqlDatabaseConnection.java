package com.tooling.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlDatabaseConnection {
	
	static final String DriverName="com.mysql.jdbc.Driver";
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		
		Class.forName(DriverName);
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/dfdesigner","root","root");
		return con;
		
	}
	

}
