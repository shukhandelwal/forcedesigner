package com.tooling.custom;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.deloitte.force.components.ComponentSessionDetails;
import com.google.gson.Gson;
import com.sforce.soap.metadata.MetadataConnection;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.SoapConnection;
import com.sforce.soap.tooling.User;
import com.tooling.helper.MySqlDatabaseConnection;
import com.deloitte.force.components.UserSessionDetails;

@SuppressWarnings("serial")
public class FirstPageClass extends HttpServlet {
	// Java util logging
	private final static Logger logger = Logger.getLogger(FirstPageClass.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	// Used to fetch the Users
	public void doGet(HttpServletRequest request, HttpServletResponse response) {

		String actionName = request.getParameter("action");
		String userNameFilter = request.getParameter("userNameFilter");
		logger.info("FirstPageClass>doGet> actionName :" + actionName);
		DFDesignerResponse dfdr = null;
		DDUtilClass dutil = new DDUtilClass();
		try {
			SoapConnection toolingConnection = DDUtilClass.getToolingConnectionFromSession(request);
			if (toolingConnection == null) {
				// TODO: Redirect to login page
			}
			if ("fetchuserlist".equals(actionName)) {
				String userQuery = "SELECT Id, Name FROM User ";
				if (userNameFilter != null && userNameFilter.trim().length() > 0) {
					logger.info("userNameFilter : " + userNameFilter);
					userQuery += " where Name like '%" + userNameFilter.trim() + "%' ";
				}
				userQuery += " order by Name Limit 1000";
				List<SObject> sobjList = dutil.queryRecords(toolingConnection, userQuery);
				if (sobjList != null && sobjList.size() > 0) {
					List<User> userList = new ArrayList<User>();
					for (SObject sobj : sobjList) {
						userList.add((User) sobj);
					}

					Map<String, String> userNameMap = new HashMap<String, String>();
					for (User a : userList) {
						userNameMap.put(a.getId(), a.getName());
					}
					String userNameListJSON = new Gson().toJson(userList);
					// logger.info("input values" + userNameListJSON);
					dfdr = new DFDesignerResponse();
					dfdr.setSuccess(true);
					dfdr.setMsg(DDConstants.SUCCESS);
					dfdr.setResult(userNameListJSON);
				} else {
					dfdr = new DFDesignerResponse();
					dfdr.setSuccess(true);
					dfdr.setMsg(DDConstants.SUCCESS);
				}
			} else if ("fetchCurrentUser".equals(actionName)) {
				UserSessionDetails usd = dutil.getUserSessionDetails(request);
				dfdr = new DFDesignerResponse();
				dfdr.setSuccess(true);
				dfdr.setMsg(DDConstants.SUCCESS);
				dfdr.setResult(usd.userName);
			} else if ("signoutUser".equals(actionName)) {
				HttpSession session = request.getSession(false);
				session.invalidate();
				dfdr = new DFDesignerResponse();
				dfdr.setSuccess(true);
				dfdr.setMsg("Signing out");
				dfdr.setResult(DDConstants.SUCCESS);
				logger.info("Signing out User : ");
			}
			String responseJSON = new Gson().toJson(dfdr);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(responseJSON);
		} catch (DFDException e) {
			// TODO: Redirect to login page
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		for (String param : request.getParameterMap().keySet()) {
			logger.info("FirstPageClass>doPost> param ::" + param + ":: value ::"
					+ (request.getParameterMap().get(param)[0]));
		}

		try {
			SoapConnection toolingConnection = DDUtilClass.getToolingConnectionFromSession(request);
			MetadataConnection metadataConnection = DDUtilClass.getMetadataConnectionFromSession(request);
			PartnerConnection partnerConnection = DDUtilClass.getPartnerConnectionFromSession(request);
			if (toolingConnection == null) {
				// TODO: Redirect to login page
			}
			String actionName = request.getParameter("action");
			String clientName = request.getParameter("clientName");
			String projectName = request.getParameter("projectName");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			String componentPrefix = request.getParameter("componentPrefix");
			String compolist = request.getParameter("componentlist");
			String userList = request.getParameter("userlist");
			String userName = request.getParameter("username");
			
			
			//Shubham:insert login details to the database on click of fetch button 
			if ("insertUserDetails".equals(actionName)) {
				Connection con=null;
				try{
				con = MySqlDatabaseConnection.getConnection();
				String query = "insert into login_log(username,clientname,projectname) values(?,?,?)";
				logger.info("FirstPageClass>doPost> insertLogin Details ");
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setString(1, userName);
				stmt.setString(2, clientName);
				stmt.setString(3, projectName);
				stmt.execute();
				con.close();
				}catch(ClassNotFoundException e){
					logger.info(e.getMessage());
				}
				catch(SQLException e){
					logger.info(e.getMessage());
				}
				finally{
					if(con!=null){
						con.close();
					}
				}
				
			}

			if ("fetchcomplist".equals(actionName)) {
				// Fetch the components
				logger.info("FirstPageClass>doPost>request.getParameterMap() :" + request.getParameterMap());
				DDUtilClass dutil = new DDUtilClass();
				logger.info("FirstPageClass>doPost> clientName :" + clientName);
				logger.info("FirstPageClass>doPost> startDate :" + startDate);
				logger.info("FirstPageClass>doPost> endDate :" + endDate);
				logger.info("FirstPageClass>doPost> componentPrefix :" + componentPrefix);
				String jsonString = dutil.fetchSelectedComponentData(compolist, userList, startDate, endDate,
						componentPrefix, toolingConnection, request, metadataConnection);

				response.setContentType("json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(jsonString);
			} else {
				// Generate TDD or Data Dictionary

				logger.info("request.getParameter(generateTDDChk) :" + request.getParameter("generateTDDChk"));
				logger.info("request.getParameter(generateDDChk) :" + request.getParameter("generateDDChk"));

				ComponentSessionDetails csd = DDUtilClass.getComponentSessionDetails(request);
				// if
				// (request.getParameterMap().containsKey(DDConstants.PARAM_GENERATE_TDD_BUTTON))
				// {
				if (request.getParameter("generateTDDChk") != null) {
					ServletContext servletContext = getServletContext();
					DocumentGeneratorUtil dgutil = new DocumentGeneratorUtil();
					dgutil.generateTDD(request, response, servletContext, csd, metadataConnection);
				}
				// else if
				// (request.getParameterMap().containsKey(DDConstants.PARAM_GENERATE_DD_BUTTON))
				// {
				else if (request.getParameter("generateDDChk") != null) {
					DDUtilClass.writeToExcel(request, response, getServletContext(), projectName, toolingConnection,
							partnerConnection);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			DFDesignerResponse dfdr = new DFDesignerResponse();
			dfdr.setSuccess(false);
			dfdr.setMsg("Sorry, something went wrong :" + e.getMessage());
			String responseJSON = new Gson().toJson(dfdr);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			try {
				response.getWriter().write(responseJSON);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

}
