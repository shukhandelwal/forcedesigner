package com.tooling.custom;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.deloitte.force.components.LoginDetails;
import com.deloitte.force.components.UserSessionDetails;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class OAuth2Authorization extends HttpServlet {
	// Java util logging
	private final static Logger logger = Logger.getLogger(OAuth2Authorization.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	/**
	 * 
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		logger.info("IN SERVLET ");
		UserSessionDetails usd = DDUtilClass.getUserSessionDetails(request);
		if(usd == null){
			sendLoginResponse(false, "Sorry, something went wrong", response);
		}
		LoginDetails ld = usd.getLoginDetails();
		Boolean isProd = ld.getIsProd();
		String clientId = ld.getClientId();
		String consumerSecret = ld.getConsumerSecret();

		// String environment = "Sandbox";
		// String clientId =
		// "3MVG9Y6d_Btp4xp6oNcM5kgFHxoQ7dd4yQztLO9ycUQdy8jLutEzQjB43ND8T1zWcbOJUkjfY8ji8wo8z7RwR";
		// String consumerSecret = "8937897665924637675";
		String code = request.getParameter("code").trim();

		System.out.println("code:" + code + "clientId:" + clientId);

		// logger.info("input values"+ userName + " " + password + " "
		// +environment );
		if ((isProd == null) || (clientId == null || clientId.isEmpty())
				|| (consumerSecret == null || consumerSecret.isEmpty())) {
			sendLoginResponse(false, "Please enter valid information", response);
		} else {
			logger.info("OAuth2Authorization>doPost> input data okay");
			try {
				LoginLogic l = new LoginLogic();
				LoginDetails result = l.initMethodOauth(clientId, consumerSecret, isProd, code);
				logger.info("OAuth2Authorization>doPost> result :" + result);
				// If it's success, redirect.
				if (DDConstants.SUCCESS.equals(result.loginResult)) {
					LoginLogic.doSessionStuff(request, result);
					logger.info("OAuth2Authorization>doPost> Going to redirect");
					sendLoginResponse(true, "Success", response);
					RequestDispatcher dispatcher = request.getRequestDispatcher("FirstPage.jsp");
					dispatcher.forward(request, response);
				} else {
					// Else, send the error response.
					logger.info("OAuth2Authorization> Sending back error message :" + result);
					sendLoginResponse(false, result.loginResult, response);
				}
			} catch (Exception e) {
				logger.severe("OAuth2Authorization> Exception :" + e);
				e.printStackTrace();
			}
		}

		logger.info("IN SERVLET END ");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	public void sendLoginResponse(Boolean success, String msg, HttpServletResponse response) {
		try {
			if (msg == null || response == null) {
				throw new Exception("Bad input");
			}
			DFDesignerResponse lr = new DFDesignerResponse();
			lr.setSuccess(success);
			lr.setMsg(msg);

			Gson gson = new Gson();
			String jsonResponse = gson.toJson(lr);
			logger.info("OAuth2Authorization>sendLoginResponse>jsonResponse :" + jsonResponse);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonResponse);
		} catch (IOException e) {
			logger.severe("OAuth2Authorization>sendLoginResponse> IOException :" + e);
			e.printStackTrace();
		} catch (Exception ex) {
			logger.severe("OAuth2Authorization>sendLoginResponse> Exception :" + ex);
			ex.printStackTrace();
		}
	}
}
