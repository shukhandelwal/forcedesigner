package com.tooling.custom;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.deloitte.force.components.LoginDetails;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class OAuth2Initialization extends HttpServlet {
	// Java util logging
	private final static Logger logger = Logger.getLogger(OAuth2Initialization.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	/**
	 * 
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		logger.info("IN SERVLET ");

		String clientId = request.getParameter("clientId").trim();
		String consumerSecret = request.getParameter("consumerSecret").trim();
		String environment = request.getParameter("environmentOauth").trim();

		// logger.info("input values"+ userName + " " + password + " "
		// +environment );
		if ((clientId == null || clientId.isEmpty()) || (consumerSecret == null || consumerSecret.isEmpty())
				|| (environment == null || environment.isEmpty())) {
			sendLoginResponse(false, "Please enter valid information", response);
		} else {
			logger.info("OAuth2Initialization>doPost> input data okay");
			try {
				Boolean isProd = false;
				if (DDConstants.ENV_PROD.equals(environment)) {
					isProd = true;
				}

				String sfdcUrl = "https://test.salesforce.com";
				if (isProd) {
					sfdcUrl = "https://login.salesforce.com";
				}

				// String code =
				// "aPrxaSyVmC8fBbceXiRnvOQySNJmqkGhvx.vIPcCjs57HAwtEUVuSBPBTzgvfPkJpvXj61mV1Q";
				String grantService = "/services/oauth2/authorize?response_type=code&client_id=";
				// String redirect_uri =
				// "http://localhost:8080/HerokuProject/OAuth2Authorization";
				String redirect_uri = System.getenv(DDConstants.ENV_VAR_NAME_OAUTH_REDIRECT_URL);
				String loginURL = sfdcUrl + grantService + clientId + "&redirect_uri=" + redirect_uri
						+ "&state=mystate";

				LoginDetails loginRec = new LoginDetails();
				loginRec.loginResult = DDConstants.SUCCESS;
				loginRec.clientId = clientId;
				loginRec.consumerSecret = consumerSecret;
				loginRec.isProd = isProd;
				LoginLogic.doSessionStuff(request, loginRec);
				sendLoginResponse(true, loginURL, response);

			} catch (Exception e) {
				logger.severe("OAuth2Initialization> Exception :" + e);
				e.printStackTrace();
			}
		}

		logger.info("IN SERVLET END ");
	}

	public void sendLoginResponse(Boolean success, String msg, HttpServletResponse response) {
		try {
			if (msg == null || response == null) {
				throw new Exception("Bad input");
			}
			DFDesignerResponse lr = new DFDesignerResponse();
			lr.setSuccess(success);
			lr.setMsg(msg);

			Gson gson = new Gson();
			String jsonResponse = gson.toJson(lr);
			logger.info("OAuth2Initialization>sendLoginResponse>jsonResponse :" + jsonResponse);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonResponse);
		} catch (IOException e) {
			logger.severe("OAuth2Initialization>sendLoginResponse> IOException :" + e);
			e.printStackTrace();
		} catch (Exception ex) {
			logger.severe("OAuth2Initialization>sendLoginResponse> Exception :" + ex);
			ex.printStackTrace();
		}
	}
}
