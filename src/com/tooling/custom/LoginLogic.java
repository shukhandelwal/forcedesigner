package com.tooling.custom;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.wml.ContentAccessor;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;

import com.deloitte.force.components.ComponentSessionDetails;
import com.deloitte.force.components.LoginDetails;
import com.deloitte.force.components.UserSessionDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.metadata.MetadataConnection;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.partner.Connector;
import com.sforce.soap.partner.FieldType;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.PicklistEntry;
import com.sforce.soap.partner.fault.ApiFault;
import com.sforce.soap.tooling.ApexClass;
import com.sforce.soap.tooling.ApexComponent;
import com.sforce.soap.tooling.ApexPage;
import com.sforce.soap.tooling.ApexTrigger;
import com.sforce.soap.tooling.CustomField;
import com.sforce.soap.metadata.CustomObject;
import com.sforce.soap.tooling.CustomTab;
import com.sforce.soap.tooling.EmailTemplate;
import com.sforce.soap.tooling.Layout;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.SoapConnection;
import com.sforce.soap.tooling.StaticResource;
import com.sforce.soap.tooling.ValidationRule;
import com.sforce.soap.tooling.WorkflowAlert;
import com.sforce.soap.tooling.Profile;
import com.sforce.soap.tooling.WorkflowFieldUpdate;
import com.sforce.soap.tooling.WorkflowOutboundMessage;
import com.sforce.soap.tooling.WorkflowRule;
import com.sforce.soap.tooling.WorkflowTask;
import com.sforce.soap.tooling.RecordType;
import com.sforce.soap.tooling.BusinessProcess;
import com.sforce.soap.tooling.metadata.WorkflowEmailRecipient;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONObject;
import net.sf.json.util.JSONTokener;

public class LoginLogic {
	// Java util logging
	private final static Logger logger = Logger.getLogger(LoginLogic.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	// String loginStatus;
	// com.sforce.soap.partner.LoginResult result = null;

	public LoginDetails initMethod(String userName, String password, String domainURL, Boolean isProd) {
		PartnerConnection connection;
		com.sforce.soap.tooling.SoapConnection toolingConnection = null;
		com.sforce.soap.metadata.MetadataConnection metadataConnection = null;
		ConnectorConfig toolingConfig = new ConnectorConfig();
		logger.info("LoginLogic>initMethod entered");
		// logger.info("LoginLogic>initMethod entered");
		LoginDetails loginRec = new LoginDetails();
		ConnectorConfig partnerConfig = null;
		ConnectorConfig mdConfig = null;
		try {
			String endpoint = "https://test.salesforce.com/services/Soap/u/36.0";
			if(domainURL.trim().length() > 0) {
				endpoint = domainURL + "/services/Soap/u/36.0";
			}			
			else if (isProd) {
				endpoint = "https://login.salesforce.com/services/Soap/u/36.0";
			}
			partnerConfig = new ConnectorConfig();
			partnerConfig.setUsername(userName);
			partnerConfig.setPassword(password);
			partnerConfig.setAuthEndpoint(endpoint);
			connection = Connector.newConnection(partnerConfig); // Partner
																	// Connections
			logger.info("LoginLogic>initMethod connector.END_POINT :" + Connector.END_POINT);
			logger.info("LoginLogic>initMethod User logged in");
			// display some current settings
			/*
			 * LOGGER.info("Auth EndPoint: " +partnerConfig.getAuthEndpoint());
			 * LOGGER.info( "Service EndPoint: "
			 * +partnerConfig.getServiceEndpoint()); LOGGER.info("Username: "
			 * +partnerConfig.getUsername()); LOGGER.info("SessionId: "
			 * +partnerConfig.getSessionId());
			 */

			toolingConfig.setSessionId(partnerConfig.getSessionId()); // Configuring
																		// ToolingConfig
			toolingConfig.setServiceEndpoint(partnerConfig.getServiceEndpoint().toString().replace('u', 'T'));
			logger.info("LoginLogic>initMethod Getting tooling connection");

			toolingConnection = com.sforce.soap.tooling.Connector.newConnection(toolingConfig); // Tooling
																								// Connection
			logger.info("LoginLogic>initMethod Got tooling connection");

			// Metadata Connection
			mdConfig = new ConnectorConfig();
			mdConfig.setUsername(userName);
			mdConfig.setPassword(password);
			// mdConfig.setAuthEndpoint(endpoint.toString().replace("/u/",
			// "/c/"));
			mdConfig.setSessionId(partnerConfig.getSessionId());
			logger.info("-- Metada config service endpoint before--" + partnerConfig.getServiceEndpoint().toString());
			mdConfig.setServiceEndpoint(partnerConfig.getServiceEndpoint().toString().replace("/u/", "/m/"));
			logger.info("-- Metada config service endpoint after--" + mdConfig.getServiceEndpoint());

			// metadataConfig.setSessionId(partnerConfig.getSessionId());
			// metadataConfig.setServiceEndpoint((new
			// EnterpriseConnection(mdConfig)).login(userName,password).getMetadataServerUrl());

			// metadataConfig.setSessionId((new
			// EnterpriseConnection(mdConfig)).login(userName,password).getSessionId());
			// metadataConfig.setServiceEndpoint((new
			// EnterpriseConnection(mdConfig)).login(userName,password).getMetadataServerUrl());

			logger.info("LoginLogic>initMethod Getting Metadata connection");

			metadataConnection = com.sforce.soap.metadata.Connector.newConnection(mdConfig);
			// metadataConnection = new MetadataConnection(mdConfig);
			logger.info("LoginLogic>initMethod Got Metadata connection");

		} catch (ConnectionException e) {
			logger.info("LoginLogic> Exception :" + e);
			ApiFault apiFault = (ApiFault) e;
			e.printStackTrace();
			loginRec.loginResult = apiFault.getExceptionMessage();
			return loginRec;
		}
		logger.info(
				"LoginLogic>initMethod toolingConnection:--- " + toolingConnection.getSessionHeader().getSessionId()); // Tooling
																														// Connection
																														// User
		logger.info(
				"LoginLogic>initMethod metadataConnection:--- " + metadataConnection.getSessionHeader().getSessionId()); // ID
		if (toolingConnection.getSessionHeader().getSessionId() != null) {
			loginRec.loginResult = DDConstants.SUCCESS;
			loginRec.token = toolingConnection.getSessionHeader().getSessionId();
			loginRec.setToolingServiceEndpoint(toolingConfig.getServiceEndpoint());
			loginRec.setPartnerServiceEndpoint(partnerConfig.getServiceEndpoint());
			try {
				loginRec.userName = toolingConnection.getUserInfo().getUserFullName().toString();
				loginRec.userOrganization = toolingConnection.getUserInfo().getOrganizationName().toString();
			} catch (ConnectionException e) {
				e.printStackTrace();
			}
		} else {
			loginRec.loginResult = "error";
		}
		if (metadataConnection.getSessionHeader().getSessionId() != null) {
			loginRec.metadataToken = metadataConnection.getSessionHeader().getSessionId();
			loginRec.metadataServiceEndpoint = mdConfig.getServiceEndpoint();
		}

		return loginRec;
	}

	public LoginDetails initMethodOauth(String clientId, String consumerSecret, Boolean isProd, String code) {
		logger.info("LoginLogic>initMethod entered");
		// logger.info("LoginLogic>initMethod entered");
		LoginDetails loginRec = new LoginDetails();
		ConnectorConfig partnerConfig = new ConnectorConfig();
		ConnectorConfig mdConfig = null;
		ConnectorConfig toolingConfig = new ConnectorConfig();
		ConnectorConfig metadataConfig = new ConnectorConfig();
		PartnerConnection connection;
		com.sforce.soap.tooling.SoapConnection toolingConnection = null;
		com.sforce.soap.metadata.MetadataConnection metadataConnection = null;
		try {
			String endpoint = "https://test.salesforce.com/services/Soap/u/34.0";
			if (isProd) {
				endpoint = "https://login.salesforce.com/services/Soap/u/34.0";
			}
			Map<String, String> instanceMap = getAccessToken(clientId, consumerSecret, isProd, code);
			String token = "";
			String tokenURL = "";
			if (!instanceMap.isEmpty()) {
				token = instanceMap.get("loginAccessToken");
				tokenURL = instanceMap.get("loginInstanceUrl");
			}

			logger.info("token: " + token);
			logger.info("loginInstanceUrl: " + tokenURL);
			partnerConfig.setSessionId(token);

			partnerConfig.setAuthEndpoint(endpoint);
			connection = Connector.newConnection(partnerConfig);
			logger.info("LoginLogic>initMethod connector.END_POINT :" + Connector.END_POINT);
			logger.info("LoginLogic>initMethod User logged in");

			toolingConfig.setSessionId(partnerConfig.getSessionId());

			String toolingEndpoint = tokenURL + "/services/Soap/T/34.0";
			toolingConfig.setServiceEndpoint(toolingEndpoint);

			// toolingConfig.setServiceEndpoint(partnerConfig.getServiceEndpoint().toString().replace('u',
			// 'T'));
			logger.info("LoginLogic>initMethod Getting tooling connection");

			toolingConnection = com.sforce.soap.tooling.Connector.newConnection(toolingConfig);
			logger.info("LoginLogic>initMethod Got tooling connection");
			logger.info("LoginLogic>initMethod Got tooling connection");

			// Metadata Connection
			mdConfig.setSessionId(partnerConfig.getSessionId());
			logger.info("-- Metada config service endpoint before--" + partnerConfig.getServiceEndpoint().toString());
			mdConfig.setServiceEndpoint(partnerConfig.getServiceEndpoint().toString().replace("/u/", "/m/"));
			logger.info("-- Metada config service endpoint after--" + mdConfig.getServiceEndpoint());
			logger.info("LoginLogic>initMethod Getting Metadata connection");

			metadataConnection = com.sforce.soap.metadata.Connector.newConnection(mdConfig);
			logger.info("LoginLogic>initMethod Got Metadata connection");

		} catch (ConnectionException e) {
			logger.info("LoginLogic> Exception :" + e);
			ApiFault apiFault = (ApiFault) e;
			e.printStackTrace();
			loginRec.loginResult = apiFault.getExceptionMessage();
			return loginRec;
		}
		logger.info(
				"LoginLogic>initMethod toolingConnection:--- " + toolingConnection.getSessionHeader().getSessionId());
		if (toolingConnection.getSessionHeader().getSessionId() != null) {
			loginRec.loginResult = DDConstants.SUCCESS;
			loginRec.token = toolingConnection.getSessionHeader().getSessionId();
			loginRec.setToolingServiceEndpoint(toolingConfig.getServiceEndpoint());
			loginRec.setPartnerServiceEndpoint(partnerConfig.getServiceEndpoint());
			try {
				loginRec.userName = toolingConnection.getUserInfo().getUserFullName().toString();
				loginRec.userOrganization = toolingConnection.getUserInfo().getOrganizationName().toString();
			} catch (ConnectionException e) {
				e.printStackTrace();
			}
		} else {
			loginRec.loginResult = "error";
		}
		if (metadataConnection.getSessionHeader().getSessionId() != null) {
			loginRec.metadataToken = metadataConnection.getSessionHeader().getSessionId();
			loginRec.metadataServiceEndpoint = mdConfig.getServiceEndpoint();
		}
		return loginRec;
	}

	public static Map<String, String> getAccessToken(String clientId, String consumerSecret, Boolean isProd,
			String code) {

		// HttpClient httpclient = new HttpClient();
		Map<String, String> returnMap = new HashMap<String, String>();
		String sfdcUrl = "https://test.salesforce.com";
		if (isProd) {
			sfdcUrl = "https://login.salesforce.com";
		}

		// String code =
		// "aPrxaSyVmC8fBbceXiRnvOQySNJmqkGhvx.vIPcCjs57HAwtEUVuSBPBTzgvfPkJpvXj61mV1Q";
		String oauthCode = "";
		oauthCode = code;
		// String redirect_uri =
		// "http://localhost:8080/HerokuProject/OAuth2Authorization";
		String redirect_uri = System.getenv(DDConstants.ENV_VAR_NAME_OAUTH_REDIRECT_URL);
		String grantService = "/services/oauth2/token?grant_type=authorization_code&code=" + oauthCode;
		String loginURL = sfdcUrl + grantService + "&client_id=" + clientId + "&client_secret=" + consumerSecret
				+ "&redirect_uri=" + redirect_uri;
		// String loginURL = sfdcUrl +grantService + "&redirect_uri=" +
		// redirect_uri;
		logger.info("loginURL: " + loginURL);

		HttpClient httpclient = new DefaultHttpClient();
		// Login requests must be POSTs
		HttpPost httpPost = new HttpPost(loginURL);
		HttpResponse response = null;
		try {
			// Execute the login POST request
			response = httpclient.execute(httpPost);
		} catch (ClientProtocolException cpException) {
			cpException.printStackTrace();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}

		final int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode != HttpStatus.SC_OK) {
			logger.info(
					"Error authenticating to Force.com: " + statusCode + response.getStatusLine().getReasonPhrase());
			// Error is in EntityUtils.toString(response.getEntity())
			return returnMap;
		}
		String getResult = null;
		try {
			getResult = EntityUtils.toString(response.getEntity());
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
		JSONObject jsonObject = null;
		String loginAccessToken = null;
		String loginInstanceUrl = null;
		try {
			jsonObject = (JSONObject) new JSONTokener(getResult).nextValue();
			loginAccessToken = jsonObject.getString("access_token");
			loginInstanceUrl = jsonObject.getString("instance_url");
		} catch (JSONException jsonException) {
			jsonException.printStackTrace();
		}
		logger.info("jsonObject:" + jsonObject);
		// logger.info(response.getStatusLine());
		logger.info("Successful login");
		logger.info("  instance URL: " + loginInstanceUrl);
		logger.info("  access token/session ID: " + loginAccessToken);
		// release connection
		// httpPost.releaseConnection();
		returnMap.put("loginAccessToken", loginAccessToken);
		returnMap.put("loginInstanceUrl", loginInstanceUrl);
		return returnMap;
	}

	public void replacePlaceholder(WordprocessingMLPackage template, String name, String placeholder) {
		List<Object> texts = getAllElementFromObject(template.getMainDocumentPart(), Text.class);

		for (Object text : texts) {
			Text textElement = (Text) text;
			if (textElement.getValue().equals(placeholder)) {
				textElement.setValue(name);
			}
		}
	}

	public void fetchApexClassMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchApexClassMethod> IN CLASS METHOD" + s);
		if (compSessionDetails == null)
			return;
		// Now convert the JSON string back to list of SObjects
		Type type = new TypeToken<List<ApexClass>>() {
		}.getType();
		List<ApexClass> classList = new Gson().fromJson(compSessionDetails.getApexClassList(), type);
		logger.fine("LoginLogic>fetchApexClassMethod> QueryResult SIZE:--- " + classList);

		List<Map<String, String>> apexClassMapList = new ArrayList<Map<String, String>>();
		for (ApexClass a : classList) {
			ApexClass classRecord = (ApexClass) a;
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();
				documentPlaceHolderApexClassMap.put("FirstClass", classRecord.getName());
				documentPlaceHolderApexClassMap.put("ClassStatus", classRecord.getStatus().toString());
				documentPlaceHolderApexClassMap.put("ApiVersion", classRecord.getApiVersion().toString());
				apexClassMapList.add(documentPlaceHolderApexClassMap);
			}
		}

		try {
			replaceTable(new String[] { "FirstClass", "ClassStatus", "ApiVersion" }, apexClassMapList, wordMLPackage);
			logger.info("LoginLogic>fetchApexClassMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchApexPagesMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchApexPagesMethod> " + s);
		if (compSessionDetails == null)
			return;
		// Now convert the JSON string back to list of ApexPages
		Type type = new TypeToken<List<ApexPage>>() {
		}.getType();
		List<ApexPage> sobjList = new Gson().fromJson(compSessionDetails.getVfPageList(), type);
		logger.fine("LoginLogic>fetchApexPagesMethod> QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> apexPageMapList = new ArrayList<Map<String, String>>();
		for (ApexPage a : sobjList) {
			ApexPage classRecord = (ApexPage) a;
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();
				documentPlaceHolderApexClassMap.put("FirstPage", classRecord.getName().toString());
				if (classRecord.getDescription() != null && classRecord.getDescription() != "") {
					documentPlaceHolderApexClassMap.put("PageDescription", classRecord.getDescription().toString());
				} else {
					documentPlaceHolderApexClassMap.put("PageDescription", "");
				}
				if (classRecord.getControllerKey() != null && classRecord.getControllerKey() != "") {
					documentPlaceHolderApexClassMap.put("PageController", classRecord.getControllerKey().toString());
				} else {
					documentPlaceHolderApexClassMap.put("PageController", "");
				}
				apexPageMapList.add(documentPlaceHolderApexClassMap);
			}
		}

		try {
			replaceTable(new String[] { "FirstPage", "PageDescription", "PageController" }, apexPageMapList,
					wordMLPackage);
			logger.info("LoginLogic>fetchApexPagesMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void fetchcustomtabsMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchApexPagesMethod> " + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<CustomTab>>() {
		}.getType();
		List<CustomTab> sobjList = new Gson().fromJson(compSessionDetails.getCustomTabList(), type);
		logger.fine("LoginLogic>fetchcustomtabsMethod> QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> cusTabMapList = new ArrayList<Map<String, String>>();
		for (CustomTab custTab : sobjList) {
			if( s.contains( custTab.getId() ) ) {
				Map<String, String> documentPlaceHoldercusTabMap = new HashMap<String, String>();
				documentPlaceHoldercusTabMap.put("TabName", custTab.getFullName());
				String tabLabel = "";
				if (custTab.getDeveloperName() != null)
					tabLabel = custTab.getDeveloperName();
				documentPlaceHoldercusTabMap.put("TabLabel", tabLabel);
				String tabDesc = "";
				if (custTab.getDescription() != null)
					tabDesc = custTab.getDescription();
				documentPlaceHoldercusTabMap.put("TabDescription", tabDesc);
				cusTabMapList.add(documentPlaceHoldercusTabMap);
			}
		}

		try {
			replaceTable(new String[] { "TabName", "TabLabel", "TabDescription" }, cusTabMapList, wordMLPackage);
			logger.info("LoginLogic>fetchcustomtabsMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchstaticResourcesMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchstaticResourcesMethod> " + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<StaticResource>>() {
		}.getType();
		List<StaticResource> sobjList = new Gson().fromJson(compSessionDetails.getStaticResourceList(), type);
		logger.fine("LoginLogic>fetchstaticResourcesMethod> QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> srMapList = new ArrayList<Map<String, String>>();
		for (StaticResource classRecord : sobjList) {
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHoldersrMap = new HashMap<String, String>();
				documentPlaceHoldersrMap.put("SRName", classRecord.getName());
				String desc = "";
				if (classRecord.getDescription() != null)
					desc = classRecord.getDescription();
				documentPlaceHoldersrMap.put("SRDescription", desc);
				documentPlaceHoldersrMap.put("SRContentType", classRecord.getContentType());
				documentPlaceHoldersrMap.put("SRCacheControl", classRecord.getCacheControl());
				srMapList.add(documentPlaceHoldersrMap);
			}
		}

		try {
			replaceTable(new String[] { "SRName", "SRDescription", "SRContentType", "SRCacheControl" }, srMapList,
					wordMLPackage);
			logger.info("LoginLogic>fetchstaticResourcesMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Map<String, List<String>> fetchCustomFieldsMethod(Set<String> fieldIdSet, ServletContext servletContextLocal,
			SoapConnection toolingConnection, PartnerConnection partnerConnection) {
		//logger.info("LoginLogic>fetchCustomFieldsMethod> Entered");
		if (fieldIdSet == null || fieldIdSet.size() == 0)
			return null;
		Map<String, List<String>> fieldMap = null;
		logger.info("LoginLogic>fetchCustomFieldsMethod> fieldIdSet size :" + fieldIdSet.size());
		try {
			JSONObject metadataList = new JSONObject();
			DDUtilClass dutil = new DDUtilClass();
			List<String> fieldIdList = new ArrayList<String>();
			fieldIdList.addAll(fieldIdSet);
			Set<String> newFieldIdSet = new HashSet<String>();
			Map<String, com.sforce.soap.tooling.metadata.CustomField> metaDataMap = new HashMap<String, com.sforce.soap.tooling.metadata.CustomField>();
			/*String fieldIdString = dutil.quoteKeySet(fieldIdSet);
			logger.info("LoginLogic>fetchCustomFieldsMethod> fieldIdString :" + fieldIdString);
			List<SObject> fieldList = dutil.queryRecords(toolingConnection,
					dutil.componentNameQueryMap.get(DDConstants.TYPE_CUSTOM_FIELD) + " WHERE Id IN " + fieldIdString);
			*/
			int componentCount = fieldIdList.size();
			int j = componentCount / 500;
			int k1 = 0;
			if (componentCount % 500 == 0)
				k1 = j;
			else
				k1 = j + 1;
			int startIndex = 0;
			int endIndex = 0;
			List<SObject> fieldList = new ArrayList<SObject>();
			for(int i = 0;i<k1;i++){
				if(i == (k1-1))
					endIndex = componentCount;
				else
					endIndex = startIndex+500;
				
				newFieldIdSet = new HashSet<String>();
				newFieldIdSet.addAll(fieldIdList.subList(startIndex, endIndex));
				String fieldIdString = dutil.quoteKeySet(newFieldIdSet);
				fieldList.addAll(dutil.queryRecords(toolingConnection,
					dutil.componentNameQueryMap.get(DDConstants.TYPE_CUSTOM_FIELD) + " WHERE Id IN " + fieldIdString));
				startIndex = startIndex + 500;
			}
			
			logger.info("LoginLogic>fetchCustomFieldsMethod> fetched all field info from Tooling API");
			// describeGlobal() call
			// com.sforce.soap.partner.DescribeGlobalResult describeGlobalResult
			// =
			// LoginLogic.connection.describeGlobal();
			Map<String, SObject> toolingFieldMap;
			toolingFieldMap = new HashMap<String, SObject>();
			for (SObject a : fieldList) {
				CustomField fieldRecord = (CustomField) a;
				String key = fieldRecord.getEntityDefinition().getQualifiedApiName() + fieldRecord.getDeveloperName();
				toolingFieldMap.put(key, fieldRecord);
				//logger.info("key in toolingFieldMap is: " + key);

			}
			// Get the sObjects from the describe global result
			// DescribeGlobalSObjectResult[] sobjectResults =
			// describeGlobalResult.getSobjects();
			// Write the name of each sObject to the console

			Set<String> objNames = new HashSet<String>();
			for (SObject s : fieldList) {
				
				CustomField f = (CustomField) s;
				if( !f.getEntityDefinition().getQualifiedApiName().equalsIgnoreCase( "Activity" ) ) {
					objNames.add(f.getEntityDefinition().getQualifiedApiName());	
				}
				
			}
			//String[] arrayObj = objNames.toArray(new String[objNames.size()]);
			// String[] arrayObj = (String[])objNames.toArray();

			List<String> allObjNames = new ArrayList<String>();
			List<String> subListObjNames = new ArrayList<String>();

			allObjNames.addAll( objNames );

			componentCount = allObjNames.size();
			
			logger.info("LoginLogic>allObjNames.size :" + allObjNames.size());
			logger.info("LoginLogic>allObjNames :" + allObjNames);

			j = componentCount / 100;
			k1 = 0;
			if ( componentCount % 100 == 0 )
				k1 = j;
			else
				k1 = j + 1;
			startIndex = 0;
			endIndex = 0;


			for( int i = 0; i < k1; i++ ) {
				if( i == ( k1-1 ) )
					endIndex = componentCount;
				else
					endIndex = startIndex + 100;
				
				subListObjNames = allObjNames.subList( startIndex, endIndex );
				logger.info("LoginLogic>subListObjNames :" + subListObjNames);
				com.sforce.soap.partner.DescribeSObjectResult[] describeSObjectResults = partnerConnection
					.describeSObjects(subListObjNames.toArray(new String[subListObjNames.size()]));
				// Map<String, com.sforce.soap.partner.Field>
				// partnerFieldMap;

				fieldMap = new HashMap<String, List<String>>();
				// Iterate through the list of describe sObject results
				for (com.sforce.soap.partner.DescribeSObjectResult desObj : describeSObjectResults) {
					// Get the name of the sObject
					String objectName = desObj.getName();
					//logger.info(
					//		"LoginLogic>fetchCustomFieldsMethod> DescribeSObjectResult loop sObject name: " + objectName);

					// For each described sObject, get the fields
					com.sforce.soap.partner.Field[] fields = desObj.getFields();
					// logger.info("LoginLogic>fetchCustomFieldsMethod>
					// DescribeSObjectResult loop Returned fields size " +
					// fields.length);

					// Get some other properties
					// if (desObj.getActivateable())
					// logger.info("LoginLogic>fetchCustomFieldsMethod>
					// DescribeSObjectResult loop \tActivateable");

					// Iterate through the fields to get properties for each field
					for (com.sforce.soap.partner.Field field : fields) {
						String fieldName = field.getName();
						// Remove __c from the end to match the field from Tooling
						// API
						if (fieldName.endsWith("__c"))
							fieldName = fieldName.substring(0, fieldName.lastIndexOf("__c"));
						//logger.info("LoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field loop key is : "
						//		+ objectName + fieldName);
						if (toolingFieldMap.containsKey(objectName + fieldName)) {
							CustomField fieldRecord = (CustomField) toolingFieldMap.get(objectName + fieldName);
							//logger.info(
							//		"LoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field loop fieldRecord.getId() :"
							//				+ fieldRecord.getId());
							if( fieldIdSet.contains( fieldRecord.getId() ) ) {
								List<String> itemList = new ArrayList<String>();
								itemList.add(field.getLabel());
								// itemList.add(fieldRecord.getMetadata().getLabel());
								itemList.add(fieldRecord.getDeveloperName());
								// logger.info("LoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field
								// loop API name is " + fieldRecord.getDeveloperName());
								itemList.add(fieldRecord.getEntityDefinition().getLabel());
								itemList.add(fieldRecord.getEntityDefinition().getQualifiedApiName());
								// itemList.add(metaDataMap.get(fieldRecord.getId()).getType().toString());
								itemList.add(field.getType().toString());
								if (null != fieldRecord.getLength())
									itemList.add(fieldRecord.getLength().toString());
								else
									itemList.add(null);
								if (field.getNillable())
									itemList.add("False");
								else
									itemList.add("True");
								/*
								 * if(metaDataMap.get(fieldRecord.getId()).getRequired()
								 * ){ itemList.add("Yes"); } else itemList.add("No");
								 */
								itemList.add(fieldRecord.getDescription());

								// }

								field.getReferenceTargetField();
								// if (field.isCustom())
								// logger.info("\t\tLoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field
								// loop This is a custom field.");
								//logger.info("\t\tLoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field loop Type: "
								//		+ field.getType());
								// if (field.getLength() > 0)
								// logger.info("\t\tLoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field
								// loop Length: " + field.getLength());
								// if (field.getPrecision() > 0)
								// logger.info("\t\tLoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field
								// loop Precision: " + field.getPrecision());

								String picklistLabels = null;
								// Determine whether this is a picklist field
								if (field.getType() == FieldType.picklist) {
									// Determine whether there are picklist values
									PicklistEntry[] picklistValues = field.getPicklistValues();
									if (picklistValues != null && picklistValues.length > 0 && picklistValues[0] != null) {
										// logger.info("\t\tLoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field
										// loop Picklist values = ");
										for (int k = 0; k < picklistValues.length; k++) {
											// logger.info("\t\t\tLoginLogic>fetchCustomFieldsMethod>DescribeSObjectResult>Field
											// loop Item: " +
											// picklistValues[k].getLabel());
											if (null == picklistLabels) {
												picklistLabels = picklistValues[k].getLabel();
												continue;
											}
											picklistLabels = picklistLabels + "," + picklistValues[k].getLabel();
										}
										picklistLabels.replaceFirst(",", "");
									}
								}
								itemList.add(picklistLabels);
								itemList.add(field.getInlineHelpText());
								if (field.isExternalId())
									itemList.add("True");
								else
									itemList.add("False");
								// fieldRecord.getMetadata().getTrackHistory();
								fieldMap.put(fieldRecord.getId(), itemList);
							}
						}
					}
					metadataList.accumulateAll(metaDataMap);
					//logger.info("LoginLogic>fetchCustomFieldsMethod>metadataList :" + metadataList);
				}
				startIndex = startIndex + 100;

			}


			
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		return fieldMap;
	}

	public void fetchApexTriggerMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchApexTriggerMethod IN TRIGGER METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<ApexTrigger>>() {
		}.getType();
		List<ApexTrigger> sobjList = new Gson().fromJson(compSessionDetails.getApexTriggerList(), type);
		logger.fine("LoginLogic>fetchApexTriggerMethod QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> apexClassMapList = new ArrayList<Map<String, String>>();
		for (ApexTrigger classRecord : sobjList) {
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();
				String triggerConditions = "";
				if (classRecord.getUsageBeforeInsert()) {
					triggerConditions += "BeforeInsert; ";
				}
				if (classRecord.getUsageBeforeUpdate()) {
					triggerConditions += "BeforeUpdate; ";
				}
				if (classRecord.getUsageBeforeDelete()) {
					triggerConditions += "BeforeDelete; ";
				}
				if (classRecord.getUsageAfterInsert()) {
					triggerConditions += "AfterInsert; ";
				}
				if (classRecord.getUsageAfterUpdate()) {
					triggerConditions += "AfterUpdate; ";
				}
				if (classRecord.getUsageAfterDelete()) {
					triggerConditions += "AfterDelete; ";
				}
				if (classRecord.getUsageAfterUndelete()) {
					triggerConditions += "AfterUndelete; ";
				}

				documentPlaceHolderApexClassMap.put("TriggerName", classRecord.getName());
				documentPlaceHolderApexClassMap.put("TriggerObjName", classRecord.getTableEnumOrId().toString());
				String stat = "";
				if (classRecord.getStatus() != null)
					stat = classRecord.getStatus();
				documentPlaceHolderApexClassMap.put("TriggerStatus", stat);
				documentPlaceHolderApexClassMap.put("TriggerConditions", triggerConditions);
				apexClassMapList.add(documentPlaceHolderApexClassMap);
				logger.info("LoginLogic> Trigger status :" + classRecord.getStatus());
			}
			
		}

		try {
			replaceTable(new String[] { "TriggerName", "TriggerObjName", "TriggerStatus", "TriggerConditions" },
					apexClassMapList, wordMLPackage);
			logger.info("Done ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchWorkflowRuleMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchWorkflowRuleMethod IN WorkflowRule METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<WorkflowRule>>() {
		}.getType();
		List<WorkflowRule> sobjList = new Gson().fromJson(compSessionDetails.getWorkflowRuleList(), type);
		logger.fine("LoginLogic>fetchWorkflowRuleMethod QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> apexClassMapList = new ArrayList<Map<String, String>>();
		for (WorkflowRule classRecord : sobjList) {
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();
				documentPlaceHolderApexClassMap.put("WorkflowRuleName", classRecord.getName());
				String desc = "";
				if(classRecord.getMetadata() != null && classRecord.getMetadata().getDescription() != null && !classRecord.getMetadata().getDescription().isEmpty())
					desc = classRecord.getMetadata().getDescription();
				documentPlaceHolderApexClassMap.put("WorkflowRuleDescription", desc);
				documentPlaceHolderApexClassMap.put("WorkflowRuleObject", classRecord.getTableEnumOrId());
				if(classRecord.getMetadata() != null) {
					documentPlaceHolderApexClassMap.put("WorkflowRuleisActive",	String.valueOf(classRecord.getMetadata().getActive()));
				}
				String workflowFormula = "";
				if (classRecord.getMetadata() != null && classRecord.getMetadata().getFormula() != null && !classRecord.getMetadata().getFormula().isEmpty())
					workflowFormula = classRecord.getMetadata().getFormula();
				documentPlaceHolderApexClassMap.put("WorkflowRuleCriteria", workflowFormula);
				apexClassMapList.add(documentPlaceHolderApexClassMap);
			}
		}

		try {
			replaceTable(new String[] { "WorkflowRuleName", "WorkflowRuleDescription", "WorkflowRuleObject",
					"WorkflowRuleisActive", "WorkflowRuleCriteria" }, apexClassMapList, wordMLPackage);
			logger.info("Done ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchWorkflowFieldUpdateMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>WorkflowFieldUpdateMethod IN WorkflowFieldUpdate METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<WorkflowFieldUpdate>>() {
		}.getType();
		List<WorkflowFieldUpdate> sobjList = new Gson().fromJson(compSessionDetails.getWorkflowFieldUpdateList(), type);
		logger.fine("LoginLogic>fetchWorkflowFieldUpdateMethod QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> apexClassMapList = new ArrayList<Map<String, String>>();
		for (WorkflowFieldUpdate classRecord : sobjList) {
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();

				documentPlaceHolderApexClassMap.put("WorkflowFieldUpdateName", classRecord.getName());
				String desc = "";
				if (classRecord.getMetadata() != null && classRecord.getMetadata().getDescription() != null)
					desc = classRecord.getMetadata().getDescription();
				documentPlaceHolderApexClassMap.put("WorkflowFieldUpdateDescription", desc);
				documentPlaceHolderApexClassMap.put("WorkflowFieldUpdateObject",
						classRecord.getSourceTableEnumOrId().toString());
				String field = "";
				if (classRecord.getMetadata() != null && classRecord.getMetadata().getField() != null)
					field = classRecord.getMetadata().getField();
				documentPlaceHolderApexClassMap.put("WorkflowFieldUpdateFieldToUpdate", field);
				String fieldValue = "";
				if (classRecord.getMetadata() != null && classRecord.getMetadata().getLiteralValue() != null)
					fieldValue = classRecord.getMetadata().getLiteralValue();
				documentPlaceHolderApexClassMap.put("WorkflowFieldUpdateFieldValue", fieldValue);
				apexClassMapList.add(documentPlaceHolderApexClassMap);
			}
		}

		try {
			replaceTable(new String[] { "WorkflowFieldUpdateName", "WorkflowFieldUpdateDescription",
					"WorkflowFieldUpdateObject", "WorkflowFieldUpdateFieldToUpdate", "WorkflowFieldUpdateFieldValue" },
					apexClassMapList, wordMLPackage);
			logger.info("Done ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchWorkflowOutboundMessageMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>WorkflowOutboundMessageMethod IN WorkflowOutboundMessage METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<WorkflowOutboundMessage>>() {
		}.getType();
		List<WorkflowOutboundMessage> sobjList = new Gson()
				.fromJson(compSessionDetails.getWorkflowOutboundMessageList(), type);
		logger.fine("LoginLogic>fetchWorkflowOutboundMessageMethod QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> apexClassMapList = new ArrayList<Map<String, String>>();
		for (WorkflowOutboundMessage classRecord : sobjList) {
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();
				List<String> fieldsList = Arrays.asList(classRecord.getMetadata().getFields());
				documentPlaceHolderApexClassMap.put("WorkflowOutboundMessageName", classRecord.getName());
				String desc = "";
				if (classRecord.getMetadata() != null && classRecord.getMetadata().getDescription() != null)
					desc = classRecord.getMetadata().getDescription();
				documentPlaceHolderApexClassMap.put("WorkflowOutboundMessageDescription", desc);
				documentPlaceHolderApexClassMap.put("WorkflowOutboundMessageObject",
						classRecord.getEntityDefinition().getFullName());
				if(classRecord.getMetadata() != null) {
					documentPlaceHolderApexClassMap.put("WorkflowOutboundMessageEndpointUrl", classRecord.getMetadata().getEndpointUrl());
				}
				String msgFields = "";
				if (fieldsList.toString() != null && fieldsList.size() > 0)
					msgFields = fieldsList.toString();
				documentPlaceHolderApexClassMap.put("WorkflowOutboundMessageFields", msgFields);
				apexClassMapList.add(documentPlaceHolderApexClassMap);
			}
		}

		try {
			replaceTable(new String[] { "WorkflowOutboundMessageName", "WorkflowOutboundMessageDescription",
					"WorkflowOutboundMessageObject", "WorkflowOutboundMessageEndpointUrl",
					"WorkflowOutboundMessageFields" }, apexClassMapList, wordMLPackage);
			logger.info("Done ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchWorkflowAlertMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>WorkflowAlertMethod IN WorkflowAlert METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<WorkflowAlert>>() {
		}.getType();
		List<WorkflowAlert> sobjList = new Gson().fromJson(compSessionDetails.getWorkflowAlertList(), type);
		logger.fine("LoginLogic>fetchWorkflowAlertMethod QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> apexClassMapList = new ArrayList<Map<String, String>>();
		for (WorkflowAlert classRecord : sobjList) {
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();
				documentPlaceHolderApexClassMap.put("WorkflowAlertName", classRecord.getDeveloperName());
				String desc = "";
				if (classRecord.getMetadata() != null && classRecord.getMetadata().getDescription() != null)
					desc = classRecord.getMetadata().getDescription();
				documentPlaceHolderApexClassMap.put("WorkflowAlertDescription", desc);
				documentPlaceHolderApexClassMap.put("WorkflowAlertObject", classRecord.getEntityDefinition().getFullName());
				String template = "", templateFinal = "";
				if (classRecord.getMetadata() != null && classRecord.getMetadata().getTemplate() != null)
					template = classRecord.getMetadata().getTemplate();
				templateFinal = template.substring(template.indexOf("/") + 1);
				documentPlaceHolderApexClassMap.put("WorkflowAlertTemplateName", templateFinal);
				String senderType = "";
				if (classRecord.getSenderType() != null)
					senderType = classRecord.getSenderType();
				documentPlaceHolderApexClassMap.put("WorkflowAlertFromEmailAddress", senderType);
				String recipientsList = "";
				if(classRecord.getMetadata() != null) {
					WorkflowEmailRecipient[] werList = classRecord.getMetadata().getRecipients();
					String recipients = "";
					for (WorkflowEmailRecipient wer : werList) {
						
						if (wer.getRecipient() != null) {
							recipients = wer.getRecipient() + "; ";
						} else if (wer.getField() != null) {
							recipients = wer.getField() + "; ";
						} else if (wer.getType() != null) {
							recipients = wer.getType() + "; ";
						}
					}
					recipientsList = recipientsList + recipients;
				}
				documentPlaceHolderApexClassMap.put("WorkflowAlertEmailRecipients", recipientsList);
				apexClassMapList.add(documentPlaceHolderApexClassMap);
			}
		}

		try {
			replaceTable(new String[] { "WorkflowAlertName", "WorkflowAlertDescription", "WorkflowAlertObject",
					"WorkflowAlertTemplateName", "WorkflowAlertFromEmailAddress", "WorkflowAlertEmailRecipients" },
					apexClassMapList, wordMLPackage);
			logger.info("Done ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchValidationRuleMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info(" IN TRIGGER METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<ValidationRule>>() {
		}.getType();
		List<ValidationRule> sobjList = new Gson().fromJson(compSessionDetails.getValidationRuleList(), type);
		logger.fine("LoginLogic>fetchValidationRuleMethod QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> apexClassMapList = new ArrayList<Map<String, String>>();
		for (ValidationRule classRecord : sobjList) {
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();
				documentPlaceHolderApexClassMap.put("ValidationName", classRecord.getValidationName().toString());
				documentPlaceHolderApexClassMap.put("ValidationDescription", classRecord.getDescription());
				documentPlaceHolderApexClassMap.put("ValidationObject",
						classRecord.getEntityDefinition().getFullName().toString());
				documentPlaceHolderApexClassMap.put("ValidationErrorCondition",
						classRecord.getMetadata().getErrorConditionFormula().toString());
				documentPlaceHolderApexClassMap.put("ValiErrorMessage", classRecord.getErrorMessage().toString());
				apexClassMapList.add(documentPlaceHolderApexClassMap);
			}
		}

		try {
			replaceTable(new String[] { "ValidationName", "ValidationDescription", "ValidationObject",
					"ValidationErrorCondition", "ValiErrorMessage" }, apexClassMapList, wordMLPackage);
			logger.info("Done ");
		} catch (Exception e) {
			e.printStackTrace();
			logger.severe(e.getMessage());
		}
	}

	public void fetchApexComponentMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchApexComponentMethod> IN CLASS METHOD" + s);
		Set<String> filterRecordsSet = new HashSet<String>(Arrays.asList(s.split(",")));
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<ApexComponent>>() {
		}.getType();
		List<ApexComponent> componentList = new Gson().fromJson(compSessionDetails.getApexComponentList(), type);
		logger.fine("LoginLogic>fetchApexComponentMethod> QueryResult SIZE:--- " + componentList);

		List<Map<String, String>> apexComponentMapList = new ArrayList<Map<String, String>>();
		for (ApexComponent componentRecord : componentList) {
			if( s.contains( componentRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexComponentMap = new HashMap<String, String>();
				documentPlaceHolderApexComponentMap.put("Component Label", componentRecord.getMasterLabel());
				documentPlaceHolderApexComponentMap.put("Component Name", componentRecord.getName());
				documentPlaceHolderApexComponentMap.put("Description", componentRecord.getDescription());
	//			documentPlaceHolderApexComponentMap.put("CreatedBy", componentRecord.getCreatedBy().getName());
				apexComponentMapList.add(documentPlaceHolderApexComponentMap);
			}
		}

		try {
			replaceTable(new String[] { "Component Label", "Component Name", "Description" },
					apexComponentMapList, wordMLPackage);
			logger.info("LoginLogic>fetchApexComponentMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchProfilesMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchProfilesMethod> IN CLASS METHOD" + s);
		Set<String> filterRecordsSet = new HashSet<String>(Arrays.asList(s.split(",")));
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<Profile>>() {
		}.getType();
		List<Profile> profileList = new Gson().fromJson(compSessionDetails.getProfileList(), type);
		logger.fine("LoginLogic>fetchProfilesMethod> QueryResult SIZE:--- " + profileList);

		List<Map<String, String>> profileMapList = new ArrayList<Map<String, String>>();
		for (Profile profileRecord : profileList) {
			if( s.contains( profileRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderProfileMap = new HashMap<String, String>();
				
				documentPlaceHolderProfileMap.put("ProfileName", profileRecord.getFullName());
				documentPlaceHolderProfileMap.put("ProfileDescription", profileRecord.getDescription());
				logger.info("Profile = " + profileRecord.getFullName() + profileRecord.getDescription());
				profileMapList.add(documentPlaceHolderProfileMap);
			}
		}

		try {
			replaceTable(new String[] { "ProfileName", "ProfileDescription" },
					profileMapList, wordMLPackage);
			logger.info("LoginLogic>fetchProfilesMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// USer Roles
	public void fetchRoleMethod(Map<String, String> s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails,
			MetadataConnection metadataConnection) {
		logger.info("LoginLogic>fetchRoleMethod> IN CLASS METHOD" + s);
		List<FileProperties> rolelist = new ArrayList<FileProperties>();
		
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<FileProperties>>() {
		}.getType();
		List<FileProperties> roleObject = new Gson().fromJson(compSessionDetails.getRoleList(),
				type);
		if (roleObject != null && !roleObject.isEmpty()) {
			rolelist = DDUtilClass.generateRoleList(roleObject, metadataConnection, s,
					"generate");
		}		
		logger.info("LoginLogic>fetchRoleMethod> QueryResult SIZE:--- " + rolelist.size());
		String componentIds = s.get("componentId");

		List<Map<String, String>> roleMapList = new ArrayList<Map<String, String>>();
		for (FileProperties a : rolelist) {
			if( componentIds.contains( a.getId() ) ) {
				String nameDesc[] = (a.getFullName()).split("\\|");
				Map<String, String> documentPlaceHolderRoleMap = new HashMap<String, String>();
				documentPlaceHolderRoleMap.put("RoleName", nameDesc.length > 0 ? nameDesc[0] : "" );
				documentPlaceHolderRoleMap.put("RoleLabel", nameDesc.length > 1 ? nameDesc[1] : "");
				documentPlaceHolderRoleMap.put("ParentRole", nameDesc.length > 1 ? nameDesc[1] : "");
				roleMapList.add(documentPlaceHolderRoleMap);
			}
		}

		try {
			replaceTable(new String[] { "RoleName", "RoleLabel", "ParentRole" }, roleMapList,
					wordMLPackage);
			logger.info("LoginLogic>fetchRoleMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//Added By Nitin
	public void fetchCustomSettingsMethod(Map<String, String> s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails,
			MetadataConnection metadataConnection){
		List<FileProperties> customSettinglist = new ArrayList<FileProperties>();
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<FileProperties>>() {
		}.getType();
		List<FileProperties> customSettingObject = new Gson().fromJson(compSessionDetails.getCustomSettingsList(),
				type);
		/*if (customSettingObject != null && !customSettingObject.isEmpty()) {
			customSettinglist = DDUtilClass.generateCustomSettingList(customSettingObject, metadataConnection, s,
					"generate");
		}*/
		
		String componentIds = s.get("componentId");

		List<Map<String, String>> customSettingMapList = new ArrayList<Map<String, String>>();
		for (FileProperties a : customSettingObject) {
			if( componentIds.contains( a.getId() ) ) {
				String nameDesc[] = (a.getFullName()).split("\\|");
				String labelDesc[] = (a.getFileName()).split("\\|");
				Map<String, String> documentPlaceHolderCustomSettingMap = new HashMap<String, String>();
				documentPlaceHolderCustomSettingMap.put("CSLabel", labelDesc.length > 2 ? labelDesc[2] : "");
				documentPlaceHolderCustomSettingMap.put("CSName", nameDesc.length > 0 ? nameDesc[0] : "");
				documentPlaceHolderCustomSettingMap.put("CSType", labelDesc.length > 1 ? labelDesc[1] : "");
				documentPlaceHolderCustomSettingMap.put("CSVisibility", labelDesc.length > 3 ? labelDesc[3] : "");
				documentPlaceHolderCustomSettingMap.put("CSDescription", nameDesc.length > 1 ? nameDesc[1] : "");
				customSettingMapList.add(documentPlaceHolderCustomSettingMap);
			}
		}

		try {
			logger.info("LoginLogic>fetchCustomSettingMethod> first=="+customSettingMapList);
			
			
			replaceTable(new String[] { "CSLabel", "CSName","CSType","CSVisibility","CSDescription" }, customSettingMapList,
					wordMLPackage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void fetchcustomobjsMethod(Map<String, String> s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails,
			MetadataConnection metadataConnection) {
		logger.info("LoginLogic>fetchcustomobjsMethod> " + s);
		List<FileProperties> customObjectlist = new ArrayList<FileProperties>();
		if (compSessionDetails == null)
			return;
		// Now convert the JSON string back to list of custom objects
		Type type = new TypeToken<List<FileProperties>>() {
		}.getType();
		List<FileProperties> sobjList = new Gson().fromJson(compSessionDetails.getCustomObjectList(), type);
		/*if (sobjList != null && !sobjList.isEmpty()) {
			customObjectlist = DDUtilClass.generateCustomObjectList(sobjList, metadataConnection, s,
					"generate");
		}*/
		String componentIds = s.get("componentId");

		List<Map<String, String>> customObjectMapList = new ArrayList<Map<String, String>>();
		for (FileProperties a : sobjList) {
			if( componentIds.contains( a.getId() ) ) {
				String nameDesc[] = (a.getFullName()).split("\\|");
				String labelDesc[] = (a.getFileName()).split("\\|");
				Map<String, String> documentPlaceHolderCustomObjectMap = new HashMap<String, String>();
				documentPlaceHolderCustomObjectMap.put("ObjectLabel", labelDesc.length > 1 ? labelDesc[1] : "");
				documentPlaceHolderCustomObjectMap.put("ObjectName", nameDesc.length > 0 ? nameDesc[0] : "");
				documentPlaceHolderCustomObjectMap.put("ObjectDescription", nameDesc.length > 1 ? nameDesc[1] : "");
				customObjectMapList.add(documentPlaceHolderCustomObjectMap);
			}
		}

		try {
			logger.info("LoginLogic>fetchCustomSettingMethod> first=="+customObjectMapList);
			
			
			replaceTable(new String[] { "ObjectLabel", "ObjectName","ObjectDescription"}, customObjectMapList,
					wordMLPackage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchBusinessProcessMethod(Map<String, String> s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails,
			MetadataConnection metadataConnection) {
		logger.info(" In Business Process Method" + s);
		List<FileProperties> businessProcesslist = new ArrayList<FileProperties>();
		
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<FileProperties>>() {
		}.getType();
		List<FileProperties> businessProcessObject = new Gson().fromJson(compSessionDetails.getBusinessProcessList(),
				type);
		if (businessProcessObject != null && !businessProcessObject.isEmpty()) {
			businessProcesslist = DDUtilClass.generateBusinessProcessList(businessProcessObject, metadataConnection, s,
					"generate");
		}
		
		String componentIds = s.get("componentId");

		List<Map<String, String>> businessProcessMapList = new ArrayList<Map<String, String>>();
		for (FileProperties a : businessProcesslist) {
			if( componentIds.contains( a.getId() ) ) {
				String nameDesc[] = (a.getFullName()).split("\\|");
				Map<String, String> documentPlaceHolderBusinessProcessMap = new HashMap<String, String>();
				documentPlaceHolderBusinessProcessMap.put("BPName", nameDesc.length > 0 ? nameDesc[0] : "");
				documentPlaceHolderBusinessProcessMap.put("BPDescription", nameDesc.length > 1 ? nameDesc[1] : "");
				businessProcessMapList.add(documentPlaceHolderBusinessProcessMap);
			}
		}

		try {
			logger.info("LoginLogic>fetchBusinessProcessMethod> first=="+businessProcessMapList);
			
			
			replaceTable(new String[] { "BPName", "BPDescription" }, businessProcessMapList,
					wordMLPackage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void fetchApprovalProcessMethod(Map<String, String> s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails,
			MetadataConnection metadataConnection) {
		logger.info("LoginLogic>fetchApprovalProcessMethod> IN CLASS METHOD" + s);
		List<FileProperties> approvalProcesslist = new ArrayList<FileProperties>();
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<FileProperties>>() {
		}.getType();
		List<FileProperties> approvalProcessObject = new Gson().fromJson(compSessionDetails.getApprovalProcessList(),
				type);
		if (approvalProcessObject != null && !approvalProcessObject.isEmpty()) {
			approvalProcesslist = DDUtilClass.generateApprovalProcessList(approvalProcessObject, metadataConnection, s,
					"generate");
		}
		// } catch (DFDException e1) {
		// e1.printStackTrace();
		// }
		logger.info("LoginLogic>fetchApprovalProcessMethod> QueryResult SIZE:--- " + approvalProcesslist.size());
		String componentIds = s.get("componentId");

		List<Map<String, String>> approvalProcessMapList = new ArrayList<Map<String, String>>();
		for (FileProperties a : approvalProcesslist) {
			if( componentIds.contains( a.getId() ) ) {
				String nameDesc[] = (a.getFullName()).split("\\|");
				Map<String, String> documentPlaceHolderApprovalProcessMap = new HashMap<String, String>();
				documentPlaceHolderApprovalProcessMap.put("Full Name", nameDesc.length > 0 ? nameDesc[0] : "");
				documentPlaceHolderApprovalProcessMap.put("Description", nameDesc.length > 1 ? nameDesc[1] : "");
	//			documentPlaceHolderApprovalProcessMap.put("CreatedBy", a.getCreatedByName());
				approvalProcessMapList.add(documentPlaceHolderApprovalProcessMap);
			}
		}

		try {
			replaceTable(new String[] { "Full Name", "Description" }, approvalProcessMapList,
					wordMLPackage);
			logger.info("LoginLogic>fetchApprovalProcessMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void fetchPermissionSetMethod(Map<String, String> s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails,
			MetadataConnection metadataConnection) {
		logger.info("LoginLogic>fetchPermissionSetMethod> IN CLASS METHOD" + s);
		List<FileProperties> permissionSetlist = new ArrayList<FileProperties>();
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<FileProperties>>() {
		}.getType();
		List<FileProperties> permissionSetObject = new Gson().fromJson(compSessionDetails.getPermissionSetList(),
				type);
		if (permissionSetObject != null && !permissionSetObject.isEmpty()) {
			permissionSetlist = DDUtilClass.generatePermissionSetList(permissionSetObject, metadataConnection, s,
					"generate");
		}
		// } catch (DFDException e1) {
		// e1.printStackTrace();
		// }
		logger.info("LoginLogic>fetchPermissionSetMethod> QueryResult SIZE:--- " + permissionSetlist.size());
		String componentIds = s.get("componentId");

		List<Map<String, String>> permissionSetMapList = new ArrayList<Map<String, String>>();
		for (FileProperties a : permissionSetlist) {
			if( componentIds.contains( a.getId() ) ) {
				String nameDesc[] = (a.getFullName()).split("\\|");
				Map<String, String> documentPlaceHolderPermissionSetMap = new HashMap<String, String>();
				
				documentPlaceHolderPermissionSetMap.put("PSName", nameDesc.length > 0 ? nameDesc[0] : "");
				documentPlaceHolderPermissionSetMap.put("PSDescription", nameDesc.length > 1 ? nameDesc[1] : "");

				permissionSetMapList.add(documentPlaceHolderPermissionSetMap);
			}
		}

		try {
			replaceTable(new String[] { "PSName", "PSDescription" }, permissionSetMapList,
					wordMLPackage);
			logger.info("LoginLogic>fetchPermissionSetMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchRecordTypeMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchRecordTypeMethod IN RecordType METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<RecordType>>() {
		}.getType();
		List<RecordType> sobjList = new Gson().fromJson(compSessionDetails.getRecordTypeList(), type);
		logger.fine("LoginLogic>fetchRecordTypeMethod QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> apexClassMapList = new ArrayList<Map<String, String>>();
		for (RecordType classRecord : sobjList) {
			if( s.contains( classRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderApexClassMap = new HashMap<String, String>();
				
				documentPlaceHolderApexClassMap.put("RTName", classRecord.getName().contains("\\") ? classRecord.getName().split("\\.")[1] : classRecord.getName());
				documentPlaceHolderApexClassMap.put("RTDescription", classRecord.getDescription());
				String desc = "";
				if (classRecord.getDescription() != null)
					desc = classRecord.getDescription();
				documentPlaceHolderApexClassMap.put("RTDescription", desc);				
				documentPlaceHolderApexClassMap.put("RTObject", classRecord.getSobjectType().toString());
				documentPlaceHolderApexClassMap.put("RTActive",classRecord.getIsActive().toString()); 

				apexClassMapList.add(documentPlaceHolderApexClassMap);

			}
		}

		try {
			replaceTable(new String[] {"RTName", "RTDescription", "RTObject","RTActive"}, apexClassMapList, wordMLPackage);
			logger.info("Done ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	


	public void fetchPageLayoutMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchPageLayoutMethod> IN CLASS METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<Layout>>() {
		}.getType();
		List<Layout> sobjList = new Gson().fromJson(compSessionDetails.getPageLayoutList(), type);
		logger.fine("LoginLogic>fetchPageLayoutMethod> QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> pageLayoutMapList = new ArrayList<Map<String, String>>();
		for (Layout pageLayoutRecord : sobjList) {
			Map<String, String> documentPlaceHolderPageLayoutMap = new HashMap<String, String>();
			documentPlaceHolderPageLayoutMap.put("PageLayoutName", pageLayoutRecord.getName());
			documentPlaceHolderPageLayoutMap.put("PageLayoutObject", pageLayoutRecord.getFullName().split("-")[0]);
			
			pageLayoutMapList.add(documentPlaceHolderPageLayoutMap);
		}

		try {
			replaceTable(new String[] { "PageLayoutName", "PageLayoutObject" },
					pageLayoutMapList, wordMLPackage);
			logger.info("LoginLogic>fetchPageLayoutMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void fetchEmailTemplateMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchEmailTemplateMethod> IN CLASS METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<EmailTemplate>>() {
		}.getType();
		List<EmailTemplate> sobjList = new Gson().fromJson(compSessionDetails.getEmailTemplateList(), type);
		logger.fine("LoginLogic>fetchEmailTemplateMethod> QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> emailTemplateMapList = new ArrayList<Map<String, String>>();
		for (EmailTemplate emailTemplateRecord : sobjList) {
			if( s.contains( emailTemplateRecord.getId() ) ) {
				Map<String, String> documentPlaceHolderEmailTemplateMap = new HashMap<String, String>();
				documentPlaceHolderEmailTemplateMap.put("EmailTemplateName", emailTemplateRecord.getName());
				documentPlaceHolderEmailTemplateMap.put("EmailTemplateDescription", emailTemplateRecord.getDescription());
	//			documentPlaceHolderEmailTemplateMap.put("EmailTemplateCreatedBy",
	//					emailTemplateRecord.getCreatedBy().getName());
				emailTemplateMapList.add(documentPlaceHolderEmailTemplateMap);
			}
		}

		try {
			replaceTable(new String[] { "EmailTemplateName", "EmailTemplateDescription"},
					emailTemplateMapList, wordMLPackage);
			logger.info("LoginLogic>fetchEmailTemplateMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * public void fetchAssignmentRuleMethod(String s, ServletContext
	 * servletContextLocal, WordprocessingMLPackage wordMLPackage,
	 * ComponentSessionDetails compSessionDetails) { DDUtilClass dutil = new
	 * DDUtilClass(); logger.info(
	 * "LoginLogic>fetchAssignmentRuleMethod> IN CLASS METHOD" + s);
	 * List<SObject> sobjList = new ArrayList<SObject>(); // try { //
	 * componentList = dutil.queryRecords(LoginLogic.toolingConnection, //
	 * dutil.componentNameQueryMap.get(DDConstants.TYPE_ASSIGNMENT_RULE) +
	 * " WHERE Id IN " + s); // } catch (DFDException e1) { //
	 * e1.printStackTrace(); // } if(compSessionDetails == null) return;
	 * sobjList = compSessionDetails.getAssignmentRuleList(); logger.info(
	 * "LoginLogic>fetchAssignmentRuleMethod> QueryResult SIZE:--- " +
	 * sobjList);
	 * 
	 * List<Map<String, String>> assignmentRuleMapList = new
	 * ArrayList<Map<String, String>>(); for (SObject a : sobjList) {
	 * AssignmentRule assignmentRuleRecord = (AssignmentRule) a; Map<String,
	 * String> documentPlaceHolderAssignmentRuleMap = new HashMap<String,
	 * String>(); documentPlaceHolderAssignmentRuleMap.put("AssignmentRuleName",
	 * assignmentRuleRecord.getName());
	 * documentPlaceHolderAssignmentRuleMap.put("AssignmentRuleObject",
	 * assignmentRuleRecord.getEntityDefinition().getFullName());
	 * documentPlaceHolderAssignmentRuleMap.put("AssignmentRuleCreatedBy",
	 * assignmentRuleRecord.getCreatedBy().getName());
	 * assignmentRuleMapList.add(documentPlaceHolderAssignmentRuleMap); }
	 * 
	 * try { replaceTable(new String[] { "AssignmentRuleName",
	 * "AssignmentRuleObject", "AssignmentRuleCreatedBy" },
	 * assignmentRuleMapList, wordMLPackage); logger.info(
	 * "LoginLogic>fetchEmailTemplateMethod> Done"); } catch (Exception e) {
	 * e.printStackTrace(); } }
	 */

	public void fetchCustomLabelMethod(Map<String, String> s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails,
			MetadataConnection metadataConnection) {
		logger.info("LoginLogic>fetchCustomLabelMethod> IN CLASS METHOD" + s);
		List<FileProperties> customLabellist = new ArrayList<FileProperties>();
//		try {
//			List<FileProperties> customLabelObject = dutil.queryRecordsMetadataComponents(LoginLogic.metadataConnection,
//					s, DDConstants.TYPE_CUSTOM_LABEL, "generate");

			if (compSessionDetails == null)
				return;
			Type type = new TypeToken<List<FileProperties>>() {
			}.getType();
			List<FileProperties> customLabelObject = new Gson().fromJson(compSessionDetails.getCustomLabelList(),
					type);
			if (customLabelObject != null && !customLabelObject.isEmpty()) {
				customLabellist = DDUtilClass.generateCustomLabelList(customLabelObject, metadataConnection,
						s, "generate");
			}

//		} catch (DFDException e1) {
//			e1.printStackTrace();
//		}
		logger.info("LoginLogic>fetchCustomLabelMethod> QueryResult SIZE:--- " + customLabellist.size());

		List<Map<String, String>> customLabelMapList = new ArrayList<Map<String, String>>();
		for (FileProperties a : customLabellist) {
			String nameDesc[] = (a.getFullName()).split("\\|");
			Map<String, String> documentPlaceHolderCustomLabelMap = new HashMap<String, String>();
			
			documentPlaceHolderCustomLabelMap.put("CustomLabelName", nameDesc.length > 0 ? nameDesc[0] : "");
			documentPlaceHolderCustomLabelMap.put("CustomLabelCategory", nameDesc.length > 1 ? nameDesc[1] : "");
			documentPlaceHolderCustomLabelMap.put("CustomLabelDescription", nameDesc.length > 2 ? nameDesc[2] : "");
			documentPlaceHolderCustomLabelMap.put("CustomLabelValue", nameDesc.length > 3 ? nameDesc[3] : "");
//			documentPlaceHolderCustomLabelMap.put("CustomLabelCreatedBy", a.getCreatedByName());
			customLabelMapList.add(documentPlaceHolderCustomLabelMap);
		}

		try {
			replaceTable(new String[] { "CustomLabelName", "CustomLabelCategory", "CustomLabelDescription",
					"CustomLabelValue"}, customLabelMapList, wordMLPackage);
			logger.info("LoginLogic>fetchCustomLabelMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchSharingRulesMethod(Map<String, String> s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails, MetadataConnection metadataConnection) {
		logger.info("LoginLogic>fetchSharingRulesMethod> IN CLASS METHOD" + s);
		List<FileProperties> sharingRuleslist = new ArrayList<FileProperties>();
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<FileProperties>>() {
		}.getType();
		List<FileProperties> sharingRulesObject = new Gson().fromJson(compSessionDetails.getSharingRulesList(),
				type);
		if (sharingRulesObject != null && !sharingRulesObject.isEmpty()) {
			sharingRuleslist = DDUtilClass.generateSharingRulesList(sharingRulesObject, metadataConnection, s,
					"generate");
		}
		
		logger.info("LoginLogic>fetchSharingRulesMethod> QueryResult SIZE:--- " + sharingRuleslist.size());
		String componentIds = s.get("componentId");

		List<Map<String, String>> sharingRulesMapList = new ArrayList<Map<String, String>>();
		for (FileProperties a : sharingRuleslist) {
			if( componentIds.contains( a.getId() ) ) {
				String nameDesc[] = (a.getFullName()).split("\\.");
				Map<String, String> documentPlaceHolderSharingRulesMap = new HashMap<String, String>();
				documentPlaceHolderSharingRulesMap.put("SharingRuleName", nameDesc.length > 1 ? nameDesc[1] : "");
				documentPlaceHolderSharingRulesMap.put("SharingRuleObject", nameDesc.length > 0 ? nameDesc[0] : "");
				documentPlaceHolderSharingRulesMap.put("SharingRuleType", a.getType());
				sharingRulesMapList.add(documentPlaceHolderSharingRulesMap);
			}
		}

		try {
			replaceTable(new String[] { "SharingRuleName", "SharingRuleObject", "SharingRuleType" }, sharingRulesMapList,
					wordMLPackage);
			logger.info("LoginLogic>fetchSharingRulesMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void fetchWorkflowTaskMethod(String s, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage, ComponentSessionDetails compSessionDetails) {
		logger.info("LoginLogic>fetchWorkflowTaskMethod> IN CLASS METHOD" + s);
		if (compSessionDetails == null)
			return;
		Type type = new TypeToken<List<WorkflowTask>>() {
		}.getType();
		List<WorkflowTask> sobjList = new Gson().fromJson(compSessionDetails.getWorkflowTaskList(), type);
		logger.fine("LoginLogic>fetchWorkflowTaskMethod> QueryResult SIZE:--- " + sobjList);

		List<Map<String, String>> workflowTaskMapList = new ArrayList<Map<String, String>>();
		/*for (WorkflowTask workflowTaskRecord : sobjList) {
			Map<String, String> documentPlaceHolderWorkflowTaskMap = new HashMap<String, String>();
			String nameDesc[] = workflowTaskRecord.getFullName().split("\\.");
			documentPlaceHolderWorkflowTaskMap.put("WorkflowTaskName", nameDesc.length > 1 ? nameDesc[1] : "");
			documentPlaceHolderWorkflowTaskMap.put("WorkflowTaskObject", nameDesc.length > 0 ? nameDesc[0] : "");
			documentPlaceHolderWorkflowTaskMap.put("WorkflowTaskPriority", workflowTaskRecord.getPriority());
			documentPlaceHolderWorkflowTaskMap.put("WorkflowTaskType",workflowTaskRecord.getEntityDefinition().getLabel());
			workflowTaskMapList.add(documentPlaceHolderWorkflowTaskMap);
		}
		*/
		try {
			replaceTable(new String[] { "WorkflowTaskName", "WorkflowTaskObject", "WorkflowTaskPriority", "WorkflowTaskType" },
					workflowTaskMapList, wordMLPackage);
			logger.info("LoginLogic>fetchWorkflowTaskMethod> Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static List<Object> getAllElementFromObject(Object obj, Class<?> toSearch) {
		List<Object> result = new ArrayList<Object>();
		if (obj instanceof JAXBElement)
			obj = ((JAXBElement<?>) obj).getValue();
		
		if (obj.getClass().equals(toSearch)) {
			result.add(obj);
		} else if (obj instanceof ContentAccessor) {
			List<?> children = ((ContentAccessor) obj).getContent();
			for (Object child : children) {
				result.addAll(getAllElementFromObject(child, toSearch));
			}

		}
		
		return result;
	}

	private void replaceTable(String[] placeholders, List<Map<String, String>> textToAdd,
			WordprocessingMLPackage template) throws Docx4JException, JAXBException {
		List<Object> tables = getAllElementFromObject(template.getMainDocumentPart(), Tbl.class);

		// 1. find the table
		Tbl tempTable = getTemplateTable(tables, placeholders[0]);
		List<Object> rows = getAllElementFromObject(tempTable, Tr.class);

		// first row is header, second row is content
		if (rows.size() == 2) {
			// this is our template row
			Tr templateRow = (Tr) rows.get(1);
			for (Map<String, String> replacements : textToAdd) {
				// 2 and 3 are done in this method
				addRowToTable(tempTable, templateRow, replacements);
			}

			// 4. remove the template row
			tempTable.getContent().remove(templateRow);
		}
	}

	private Tbl getTemplateTable(List<Object> tables, String templateKey) throws Docx4JException, JAXBException {
		// LOGGER.info("In getTemplateTable method "+templateKey);
		for (Iterator<Object> iterator = tables.iterator(); iterator.hasNext();) {
			Object tbl = iterator.next();
			List<?> textElements = getAllElementFromObject(tbl, Text.class);
			for (Object text : textElements) {
				Text textElement = (Text) text;
				if (textElement.getValue() != null && textElement.getValue().equals(templateKey)) {
					return (Tbl) tbl;
				}
			}
		}
		return null;
	}

	private static void addRowToTable(Tbl reviewtable, Tr templateRow, Map<String, String> replacements) {
		// LOGGER.info("***************addRowToTable ****************8");
		Tr workingRow = (Tr) XmlUtils.deepCopy(templateRow);
		List<?> textElements = getAllElementFromObject(workingRow, Text.class);
		for (Object object : textElements) {
			Text text = (Text) object;

			String replacementValue = (String) replacements.get(text.getValue());
			if (replacementValue != null) {
				text.setValue(replacementValue);
			} else {
				text.setValue("");
			}
		}
		reviewtable.getContent().add(workingRow);
	}

	public static UserSessionDetails getSessionDetails(HttpServletRequest req) {
		if (req == null)
			return null;
		HttpSession session = req.getSession(false);
		if (session != null)
			return (UserSessionDetails) session.getAttribute(DDConstants.SESSION_KEY);

		return null;
	}

	public static void doSessionStuff(HttpServletRequest request, LoginDetails result) {
		// Logic For Storing Session
		UserSessionDetails userSessionDetail = LoginLogic.getSessionDetails(request);
		logger.info("LoginLogic>doSessionStuff> userSessionDetail :" + userSessionDetail);
		if (null == userSessionDetail) {
			// New User : Object not exist in Session
			userSessionDetail = new UserSessionDetails();
		}
		userSessionDetail.partnerSessionId = result.token;
		userSessionDetail.toolingSessionId = result.token;
		userSessionDetail.userName = result.userName;
		userSessionDetail.metadataSessionId = result.metadataToken;
		userSessionDetail.setLoginDetails(result);

		ComponentSessionDetails compSessionDetails = new ComponentSessionDetails();
		userSessionDetail.setComponentSessionDetails(compSessionDetails);

		// Saving In session. If there's no session, create one.
		HttpSession session = request.getSession();
		session.setAttribute(DDConstants.SESSION_KEY, userSessionDetail);
	}

	public void resetVariablesMethod(String component, ServletContext servletContextLocal,
			WordprocessingMLPackage wordMLPackage) {
		logger.info("LoginLogic>resetVariablesMethod> IN CLASS METHOD");

		// Initializing Class Variables

		List<Map<String, String>> variablesMapList = new ArrayList<Map<String, String>>();
		Map<String, String> documentPlaceHolderMap = new HashMap<String, String>();
		try {
			if (component == DDConstants.TYPE_APEX_CLASS) {
				documentPlaceHolderMap.put("FirstClass", "");
				documentPlaceHolderMap.put("ClassStatus", "");
				documentPlaceHolderMap.put("ApiVersion", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "FirstClass", "ClassStatus", "ApiVersion" }, variablesMapList,
						wordMLPackage);
			} else if (component == DDConstants.TYPE_APEX_TRIGGER) {
				documentPlaceHolderMap.put("TriggerName", "");
				documentPlaceHolderMap.put("TriggerObjName", "");
				documentPlaceHolderMap.put("TriggerStatus", "");
				documentPlaceHolderMap.put("TriggerConditions", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "TriggerName", "TriggerObjName", "TriggerStatus", "TriggerConditions" },
						variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_APEX_PAGE) {
				documentPlaceHolderMap.put("FirstPage", "");
				documentPlaceHolderMap.put("PageDescription", "");
				documentPlaceHolderMap.put("PageController", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "FirstPage", "PageDescription", "PageController" }, variablesMapList,
						wordMLPackage);
			} else if (component == DDConstants.TYPE_VALIDATION_RULE) {
				documentPlaceHolderMap.put("ValidationName", "");
				documentPlaceHolderMap.put("ValidationDescription", "");
				documentPlaceHolderMap.put("ValidationObject", "");
				documentPlaceHolderMap.put("ValidationErrorCondition", "");
				documentPlaceHolderMap.put("ValiErrorMessage", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "ValidationName", "ValidationDescription", "ValidationObject",
						"ValidationErrorCondition", "ValiErrorMessage" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_WORKFLOW_RULE) {
				documentPlaceHolderMap.put("WorkflowRuleName", "");
				documentPlaceHolderMap.put("WorkflowRuleDescription", "");
				documentPlaceHolderMap.put("WorkflowRuleObject", "");
				documentPlaceHolderMap.put("WorkflowRuleisActive", "");
				documentPlaceHolderMap.put("WorkflowRuleCriteria", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "WorkflowRuleName", "WorkflowRuleDescription", "WorkflowRuleObject",
						"WorkflowRuleisActive", "WorkflowRuleCriteria" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_WORKFLOW_FIELDUPDATE) {
				documentPlaceHolderMap.put("WorkflowFieldUpdateName", "");
				documentPlaceHolderMap.put("WorkflowFieldUpdateDescription", "");
				documentPlaceHolderMap.put("WorkflowFieldUpdateObject", "");
				documentPlaceHolderMap.put("WorkflowFieldUpdateFieldToUpdate", "");
				documentPlaceHolderMap.put("WorkflowFieldUpdateFieldValue", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "WorkflowFieldUpdateName", "WorkflowFieldUpdateDescription",
						"WorkflowFieldUpdateObject", "WorkflowFieldUpdateFieldToUpdate",
						"WorkflowFieldUpdateFieldValue" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_WORKFLOW_OUTBOUNDMESSAGE) {
				documentPlaceHolderMap.put("WorkflowOutboundMessageName", "");
				documentPlaceHolderMap.put("WorkflowOutboundMessageDescription", "");
				documentPlaceHolderMap.put("WorkflowOutboundMessageObject", "");
				documentPlaceHolderMap.put("WorkflowOutboundMessageEndpointUrl", "");
				documentPlaceHolderMap.put("WorkflowOutboundMessageFields", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "WorkflowOutboundMessageName", "WorkflowOutboundMessageDescription",
						"WorkflowOutboundMessageObject", "WorkflowOutboundMessageEndpointUrl",
						"WorkflowOutboundMessageFields" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_APEX_COMPONENT) {
				documentPlaceHolderMap.put("Component Label", "");
				documentPlaceHolderMap.put("Component Name", "");
				documentPlaceHolderMap.put("Description", "");
				documentPlaceHolderMap.put("CreatedBy", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "Component Label", "Component Name", "Description", "CreatedBy" },
						variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_STATIC_RESOURCE) {
				documentPlaceHolderMap.put("SRName", "");
				documentPlaceHolderMap.put("SRDescription", "");
				documentPlaceHolderMap.put("SRContentType", "");
				documentPlaceHolderMap.put("SRCacheControl", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "SRName", "SRDescription", "SRContentType", "SRCacheControl" },
						variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_CUSTOM_OBJECT) {
				documentPlaceHolderMap.put("ObjectLabel", "");
				documentPlaceHolderMap.put("ObjectName", "");
				documentPlaceHolderMap.put("ObjectDescription", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "ObjectLabel", "ObjectName", "ObjectDescription" }, variablesMapList,
						wordMLPackage);
			} else if (component == DDConstants.TYPE_PROFILE) {
				documentPlaceHolderMap.put("ProfileName", "");
				documentPlaceHolderMap.put("ProfileDescription", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "ProfileName", "ProfileDescription" }, variablesMapList,
						wordMLPackage);
			} else if (component == DDConstants.TYPE_APPROVAL_PROCESS) {
				documentPlaceHolderMap.put("Full Name", "");
				documentPlaceHolderMap.put("Description", "");
				documentPlaceHolderMap.put("CreatedBy", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "Full Name", "Description", "CreatedBy" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_USER_ROLE) {
				documentPlaceHolderMap.put("Role Name", "");
				documentPlaceHolderMap.put("Label", "");
				documentPlaceHolderMap.put("Parent Role", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "Role Name", "Label", "Parent Role" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_ASSIGNMENT_RULE) {
				documentPlaceHolderMap.put("AssignmentRuleName", "");
				documentPlaceHolderMap.put("AssignmentRuleObject", "");
				documentPlaceHolderMap.put("AssignmentRuleCreatedBy", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "AssignmentRuleName", "AssignmentRuleObject", "AssignmentRuleCreatedBy" },
						variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_CUSTOM_TAB) {
				documentPlaceHolderMap.put("TabName", "");
				documentPlaceHolderMap.put("TabLabel", "");
				documentPlaceHolderMap.put("TabDescription", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "TabName", "TabLabel", "TabDescription" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_EMAIL_TEMPLATE) {
				documentPlaceHolderMap.put("EmailTemplateName", "");
				documentPlaceHolderMap.put("EmailTemplateDescription", "");
				documentPlaceHolderMap.put("EmailTemplateCreatedBy", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "EmailTemplateName", "EmailTemplateDescription", "EmailTemplateCreatedBy" },
						variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_CUSTOM_LABEL) {
				documentPlaceHolderMap.put("CustomLabelName", "");
				documentPlaceHolderMap.put("CustomLabelCategory", "");
				documentPlaceHolderMap.put("CustomLabelDescription", "");
				documentPlaceHolderMap.put("CustomLabelValue", "");
				documentPlaceHolderMap.put("CustomLabelCreatedBy", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "CustomLabelName", "CustomLabelCategory", "CustomLabelDescription",
						"CustomLabelValue", "CustomLabelCreatedBy" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_WORKFLOW_ALERT) {
				documentPlaceHolderMap.put("WorkflowAlertName", "");
				documentPlaceHolderMap.put("WorkflowAlertDescription", "");
				documentPlaceHolderMap.put("WorkflowAlertObject", "");
				documentPlaceHolderMap.put("WorkflowAlertTemplateName", "");
				documentPlaceHolderMap.put("WorkflowAlertFromEmailAddress", "");
				documentPlaceHolderMap.put("WorkflowAlertEmailRecipients", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "WorkflowAlertName", "WorkflowAlertDescription", "WorkflowAlertObject",
						"WorkflowAlertTemplateName", "WorkflowAlertFromEmailAddress", "WorkflowAlertEmailRecipients" },
						variablesMapList, wordMLPackage);
			} else if ( component == DDConstants.TYPE_PAGE_LAYOUT ) {
				documentPlaceHolderMap.put("PageLayoutName", "");
				documentPlaceHolderMap.put("PageLayoutObject", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "PageLayoutName", "PageLayoutObject"},
						variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_RECORDTYPE) {
				documentPlaceHolderMap.put("RTName", "");
				documentPlaceHolderMap.put("RTDescription", "");
				documentPlaceHolderMap.put("RTObject", "");
				documentPlaceHolderMap.put("RTActive", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "RTName", "RTDescription", "RTObject","RTActive"},
						variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_SHARING_RULE) {
				documentPlaceHolderMap.put("SharingRuleName", "");
				documentPlaceHolderMap.put("SharingRuleObject", "");
				documentPlaceHolderMap.put("SharingRuleType", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "SharingRuleName", "SharingRuleObject", "SharingRuleType" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_WORKFLOW_TASK) {
				documentPlaceHolderMap.put("WorkflowTaskName", "");
				documentPlaceHolderMap.put("WorkflowTaskObject", "");
				documentPlaceHolderMap.put("WorkflowTaskPriority", "");
				documentPlaceHolderMap.put("WorkflowTaskType", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "WorkflowTaskName", "WorkflowTaskObject", "WorkflowTaskPriority", "WorkflowTaskType" }, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_PERMISSION_SET) {
				documentPlaceHolderMap.put("PSName", "");
				documentPlaceHolderMap.put("PSDescription", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "PSName", "PSDescription"}, variablesMapList, wordMLPackage);
			} else if (component == DDConstants.TYPE_BUSINESS_PROCESS) {
				documentPlaceHolderMap.put("BPName", "");
				documentPlaceHolderMap.put("BPDescription", "");
				variablesMapList.add(documentPlaceHolderMap);
				replaceTable(new String[] { "BPName", "BPDescription"}, variablesMapList, wordMLPackage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
