package com.tooling.custom;

@SuppressWarnings("serial")
public class DFDException extends Exception {
	public DFDException() {
		super();
	}

	public DFDException(String message) {
		super(message);
	}
}
