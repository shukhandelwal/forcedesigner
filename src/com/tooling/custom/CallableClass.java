package com.tooling.custom;
import com.deloitte.force.components.DFDComponent;
import com.deloitte.force.components.DFDMetadataComponent;
import com.deloitte.force.components.ComponentSessionDetails;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.json.JSONObject;

//Rabi: Feb 10, 2016: Created wrapper class to invoke queries using multi-threading
public class CallableClass implements Callable<JSONObject> {
	
	private DFDComponent comp = null;
	private DFDMetadataComponent metaDatacomp = null;
	private String componentType;
	private String compKey;
	private Map<String, String> map;
	private ComponentSessionDetails compSessionDetails;
	
	// Java util logging
	private final static Logger logger = Logger.getLogger(CallableClass.class.getName());

	static {
		CallableClass.setLogLevel(logger);
	}
	
	public CallableClass(DFDComponent comp, String compKey, ComponentSessionDetails compSessionDetails){
		this.comp = comp;
		this.componentType = "DFDComponent";
		this.compKey = compKey;
		this.compSessionDetails = compSessionDetails;
	}
	
	public CallableClass(DFDMetadataComponent metaDatacomp, Map<String, String> map, ComponentSessionDetails compSessionDetails){
		this.metaDatacomp = metaDatacomp;
		this.componentType = "DFDMetadataComponent";
		this.map = map;
		this.compSessionDetails = compSessionDetails;
	}	


	@Override
	public JSONObject call() throws Exception {
		
		logger.info("Inside CallableClass: Thread Name" + Thread.currentThread().getName());
		logger.info("Component is of type ::: " + componentType);
		JSONObject result = new JSONObject();
		if(componentType.equals("DFDComponent")){
			result = comp.getComponents(compKey, compSessionDetails);
		}		
		else{
			result = metaDatacomp.getComponents(map, compSessionDetails);
		}
				
		return result;
	}
	
	// Set the logging level
	public static void setLogLevel(Logger logger) {
		Level logLevel;
		String envLogLevel = System.getenv(DDConstants.ENV_LOG_LEVEL);
		if (envLogLevel != null && !envLogLevel.isEmpty() && DDConstants.LOG_LEVEL_MAP.containsKey(envLogLevel)) {
			logLevel = DDConstants.LOG_LEVEL_MAP.get(envLogLevel);
		} else {
			logLevel = DDConstants.LOG_LEVEL_MAP.get(DDConstants.DEFAULT_LOG_LEVEL);
		}
		logger.setLevel(logLevel);
	}
}
