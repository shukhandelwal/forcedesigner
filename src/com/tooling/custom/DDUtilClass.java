package com.tooling.custom;

import java.io.IOException;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Color;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.deloitte.force.components.ComponentFactory;
import com.deloitte.force.components.ComponentSessionDetails;
import com.deloitte.force.components.DFDComponent;
import com.deloitte.force.components.DFDMetadataComponent;
import com.deloitte.force.components.UserSessionDetails;
import com.google.gson.Gson;

import com.sforce.soap.metadata.ApprovalProcess;
import com.sforce.soap.metadata.PermissionSet;
import com.sforce.soap.metadata.CustomLabel;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.metadata.ListMetadataQuery;
import com.sforce.soap.metadata.CustomObject;
import com.sforce.soap.metadata.Metadata;
import com.sforce.soap.metadata.MetadataConnection;
import com.sforce.soap.metadata.ReadResult;
import com.sforce.soap.metadata.Role;
import com.sforce.soap.metadata.SharingCriteriaRule;
import com.sforce.soap.metadata.SharingOwnerRule;
import com.sforce.soap.metadata.BusinessProcess;

import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.tooling.ApexClass;
import com.sforce.soap.tooling.ApexComponent;
import com.sforce.soap.tooling.ApexPage;
import com.sforce.soap.tooling.ApexTrigger;
import com.sforce.soap.tooling.Profile;
import com.sforce.soap.tooling.AssignmentRule;
import com.sforce.soap.tooling.CustomField;
import com.sforce.soap.tooling.CustomTab;
import com.sforce.soap.tooling.EmailTemplate;
import com.sforce.soap.tooling.Layout;
import com.sforce.soap.tooling.QueryResult;
import com.sforce.soap.tooling.SObject;
import com.sforce.soap.tooling.SoapConnection;
import com.sforce.soap.tooling.StaticResource;
import com.sforce.soap.tooling.ValidationRule;
import com.sforce.soap.tooling.WorkflowAlert;
import com.sforce.soap.tooling.WorkflowFieldUpdate;
import com.sforce.soap.tooling.WorkflowOutboundMessage;
import com.sforce.soap.tooling.WorkflowRule;
import com.sforce.soap.tooling.WorkflowTask;
import com.sforce.soap.tooling.RecordType;
//import com.sforce.soap.tooling.HomePageComponent;

import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import net.sf.json.JSONObject;


public class DDUtilClass {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DDUtilClass.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public Map<String, String> componentNameQueryMap = new HashMap<String, String>();
	public Set<String> componentMetadataSet = new HashSet<String>();
	public Map<String, String> componentFullNameMap = new HashMap<String, String>();
	public Map<String, String> metadataComponentQueryDetails = new HashMap<String, String>();
	public Map<String, Map<String, String>> componentNameMetadataQueryMap = new HashMap<String, Map<String, String>>();

	public DDUtilClass() {
		componentNameQueryMap.put(DDConstants.TYPE_APEX_CLASS,
				"SELECT Id, Name, SymbolTable, Status, LastModifiedDate, LastModifiedById, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, ApiVersion FROM ApexClass");
		componentNameQueryMap.put(DDConstants.TYPE_APEX_TRIGGER,
				"SELECT Id, Name, TableEnumOrId , Status, UsageAfterDelete,UsageAfterInsert,UsageAfterUndelete,UsageAfterUpdate,UsageBeforeDelete,UsageBeforeInsert,UsageBeforeUpdate,CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM ApexTrigger");
		componentNameQueryMap.put(DDConstants.TYPE_APEX_PAGE,
				"SELECT Id, Name, CreatedBy.Name, LastModifiedById, LastModifiedDate, CreatedDate, LastModifiedBy.Name, Description, ControllerType , ControllerKey FROM ApexPage");
		componentNameQueryMap.put(DDConstants.TYPE_CUSTOM_FIELD,
				"Select Id, Description, DeveloperName, EntityDefinition.DeveloperName, EntityDefinition.label, EntityDefinition.QualifiedApiName, Length, Scale, SummaryOperation, NamespacePrefix, TableEnumOrId, LastModifiedDate, CreatedDate, CreatedBy.Name, LastModifiedBy.Name From CustomField");
		componentNameQueryMap.put(DDConstants.TYPE_VALIDATION_RULE,
				"SELECT Id, Description, Active, ValidationName, EntityDefinition.FullName, ErrorMessage, CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM ValidationRule");
		componentNameQueryMap.put(DDConstants.TYPE_METADATA_FIELDS, "SELECT Id, Metadata FROM CustomField");
		/*componentNameQueryMap.put(DDConstants.TYPE_CUSTOM_OBJECT,
				"SELECT Id, Description, NamespacePrefix, DeveloperName, CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM CustomObject");
		*/
		componentNameQueryMap.put(DDConstants.TYPE_CUSTOM_TAB,
				"SELECT Id, Description, Type, URL, DeveloperName, MasterLabel, CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM CustomTab");
		componentNameQueryMap.put(DDConstants.TYPE_STATIC_RESOURCE,
				"SELECT Id, Description, Name, ContentType, CacheControl, CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM StaticResource");
		componentNameQueryMap.put(DDConstants.TYPE_WORKFLOW_RULE,
				"SELECT Id, Name, TableEnumOrId, CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM WorkflowRule");
		componentNameQueryMap.put(DDConstants.TYPE_WORKFLOW_FIELDUPDATE,
				"SELECT Id, Name, SourceTableEnumOrId, CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM WorkflowFieldUpdate");
		componentNameQueryMap.put(DDConstants.TYPE_WORKFLOW_OUTBOUNDMESSAGE,
				"SELECT Id, Name, EntityDefinition.FullName, EntityDefinition.label, EntityDefinition.QualifiedApiName,  CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM WorkflowOutboundMessage");
		componentNameQueryMap.put(DDConstants.TYPE_WORKFLOW_ALERT,
				"SELECT Id, DeveloperName, EntityDefinition.label, EntityDefinition.QualifiedApiName, SenderType, CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM WorkflowAlert");
		componentNameQueryMap.put(DDConstants.TYPE_APEX_COMPONENT,
				"SELECT Id, MasterLabel, Name, Description, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate FROM ApexComponent");
		componentNameQueryMap.put(DDConstants.TYPE_EMAIL_TEMPLATE,
				"SELECT Id, Name, Description, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate FROM EmailTemplate");
		componentNameQueryMap.put(DDConstants.TYPE_WORKFLOW_TASK,
				"SELECT Id, Subject, Status, Priority, EntityDefinition.Label, EntityDefinition.FullName, EntityDefinition.QualifiedApiName, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate FROM WorkflowTask");
		componentNameQueryMap.put(DDConstants.TYPE_CUSTOM_SETTING,
				"SELECT FullName, IsCustomSetting FROM EntityDefinition");
		componentNameQueryMap.put(DDConstants.TYPE_PROFILE,
				"SELECT Id, Name, Description, CreatedBy.Name, LastModifiedById, LastModifiedDate, CreatedDate, LastModifiedBy.Name FROM Profile");
		componentNameQueryMap.put(DDConstants.TYPE_RECORDTYPE,
				"SELECT Id, Name, IsActive, SobjectType, Description, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate FROM RecordType");
		componentNameQueryMap.put(DDConstants.TYPE_PAGE_LAYOUT,
				"SELECT Id, Name, EntityDefinition.Label, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate FROM Layout");
		/*componentNameQueryMap.put(DDConstants.TYPE_BUSINESS_PROCESS,
				"SELECT Id, IsActive, Description,  Name,CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, CreatedDate FROM BusinessProcess");
		*/
		// Adding the components for which Data is retrieved using Metadata API
		metadataComponentQueryDetails.put("userId", "");
		metadataComponentQueryDetails.put("startDate", "");
		metadataComponentQueryDetails.put("endDate", "");
		metadataComponentQueryDetails.put("componentPrefix", "");
		metadataComponentQueryDetails.put("Limit", "");

		componentNameMetadataQueryMap.put(DDConstants.TYPE_APPROVAL_PROCESS, metadataComponentQueryDetails);
		componentMetadataSet.add(DDConstants.TYPE_APPROVAL_PROCESS);
		
		componentNameMetadataQueryMap.put(DDConstants.TYPE_PERMISSION_SET, metadataComponentQueryDetails);
		componentMetadataSet.add(DDConstants.TYPE_PERMISSION_SET);

		componentNameMetadataQueryMap.put(DDConstants.TYPE_BUSINESS_PROCESS, metadataComponentQueryDetails);
		componentMetadataSet.add(DDConstants.TYPE_BUSINESS_PROCESS);

		componentNameMetadataQueryMap.put(DDConstants.TYPE_CUSTOM_SETTING, metadataComponentQueryDetails);
		componentMetadataSet.add(DDConstants.TYPE_CUSTOM_SETTING);

		componentNameMetadataQueryMap.put(DDConstants.TYPE_CUSTOM_OBJECT, metadataComponentQueryDetails);
		componentMetadataSet.add(DDConstants.TYPE_CUSTOM_OBJECT);
		
		componentNameMetadataQueryMap.put(DDConstants.TYPE_CUSTOM_LABEL, metadataComponentQueryDetails);
		componentMetadataSet.add(DDConstants.TYPE_CUSTOM_LABEL);
		
		componentNameMetadataQueryMap.put(DDConstants.TYPE_USER_ROLE, metadataComponentQueryDetails);
		componentMetadataSet.add(DDConstants.TYPE_USER_ROLE);

		componentNameMetadataQueryMap.put(DDConstants.TYPE_SHARING_RULE, metadataComponentQueryDetails);
		componentMetadataSet.add(DDConstants.TYPE_SHARING_RULE);
		// componentNameMetadataQueryMap.put(DDConstants.TYPE_CUSTOM_SETTING,
		// metadataComponentQueryDetails);
		// componentMetadataSet.add(DDConstants.TYPE_CUSTOM_SETTING);

		componentFullNameMap.put(DDConstants.TYPE_APEX_CLASS, "Name");
		componentFullNameMap.put(DDConstants.TYPE_CUSTOM_TAB, "Description");
		componentFullNameMap.put(DDConstants.TYPE_APEX_TRIGGER, "Name");
		componentFullNameMap.put(DDConstants.TYPE_APEX_PAGE, "Name");
		componentFullNameMap.put(DDConstants.TYPE_CUSTOM_FIELD, "Description");
		componentFullNameMap.put(DDConstants.TYPE_VALIDATION_RULE, "Description");
		componentFullNameMap.put(DDConstants.TYPE_CUSTOM_OBJECT, "FullName");
		componentFullNameMap.put(DDConstants.TYPE_PERMISSION_SET, "FullName");
		componentFullNameMap.put(DDConstants.TYPE_STATIC_RESOURCE, "Name");
		componentFullNameMap.put(DDConstants.TYPE_WORKFLOW_RULE, "Name");
		componentFullNameMap.put(DDConstants.TYPE_WORKFLOW_FIELDUPDATE, "Name");
		componentFullNameMap.put(DDConstants.TYPE_WORKFLOW_OUTBOUNDMESSAGE, "FullName");
		componentFullNameMap.put(DDConstants.TYPE_WORKFLOW_ALERT, "Name");
		componentFullNameMap.put(DDConstants.TYPE_APEX_COMPONENT, "Name");
		componentFullNameMap.put(DDConstants.TYPE_EMAIL_TEMPLATE, "Name");
		componentFullNameMap.put(DDConstants.TYPE_APPROVAL_PROCESS, "FullName");
		componentFullNameMap.put(DDConstants.TYPE_CUSTOM_LABEL, "Name");
		componentFullNameMap.put(DDConstants.TYPE_PROFILE, "Name");
		componentFullNameMap.put(DDConstants.TYPE_RECORDTYPE,"FullName");
		componentFullNameMap.put(DDConstants.TYPE_SHARING_RULE, "FullName");
		componentFullNameMap.put(DDConstants.TYPE_WORKFLOW_TASK, "Subject");
		componentFullNameMap.put(DDConstants.TYPE_PAGE_LAYOUT, "Name");
		componentFullNameMap.put(DDConstants.TYPE_BUSINESS_PROCESS, "FullName");
		componentFullNameMap.put(DDConstants.TYPE_CUSTOM_SETTING, "FullName");
		componentFullNameMap.put(DDConstants.TYPE_USER_ROLE, "FullName");
	}	

	public String fetchSelectedComponentData(String compolist, String userlist, String startDate, String endDate,
			String componentPrefix, SoapConnection toolingConnection, HttpServletRequest req,
			MetadataConnection metadataConnection) {
		logger.info("DDUtilClass>fetchSelectedComponentData>compolist :" + compolist);
		logger.fine("DDUtilClass>fetchSelectedComponentData>userlist :" + userlist);
		Map<String, String> componentList = new HashMap<String, String>();
		Map<String, Map<String, String>> metadataComponentList = new HashMap<String, Map<String, String>>();
		for (String s : compolist.split(",")) {
			if (componentMetadataSet.contains(s))
				metadataComponentList.put(s, metadataComponentQueryDetails);
			else
				componentList.put(s, componentNameQueryMap.get(s));
		}
		if (!userlist.trim().isEmpty()) {
			Set<String> userNameSet = new HashSet<String>();
			for (String s : userlist.split(",")) {
				logger.fine("DDUtilClass>fetchSelectedComponentData>splitted s :" + s);
				userNameSet.add(s);
			}
			logger.fine("userNameSet values" + userNameSet.toString());

			String userNameBindVariable = quoteKeySet(userNameSet);
			for (String s : compolist.split(",")) {
				// Added for Metadata API Components
				if (componentMetadataSet.contains(s)) {
					Map<String, String> metadataQueryDetails = metadataComponentList.get(s);
					metadataQueryDetails.put("userId", userNameBindVariable);
					metadataComponentList.put(s, metadataQueryDetails);
				} else
					componentList.put(s, componentList.get(s) + " WHERE LastModifiedById IN " + userNameBindVariable);
			}
		}
		logger.fine("-- componentList --" + componentList);
		if (startDate != null && !startDate.isEmpty()) {
			try {
				Date sd = (new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)).parse(startDate);
				DateFormat sdfFormatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
				String startDateFormatted = sdfFormatted.format(sd);
				for (String s : compolist.split(",")) {
					logger.info("DDUtilClass>fetchSelectedComponentData> s :" + s);
					String tempSOQL = componentList.get(s);
					if (componentMetadataSet.contains(s)) {
						Map<String, String> metadataQueryDetails = metadataComponentList.get(s);
						metadataQueryDetails.put("startDate", startDateFormatted);
						metadataComponentList.put(s, metadataQueryDetails);
					} else if (tempSOQL != null) {
						if (tempSOQL.indexOf(" WHERE ") == -1) {
							tempSOQL += " WHERE ";
						} else {
							tempSOQL += " AND ";
						}
						componentList.put(s, tempSOQL + "LastModifiedDate >= " + startDateFormatted);
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		if (endDate != null && !endDate.isEmpty()) {
			try {
				Date sd = (new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)).parse(endDate);

				DateFormat sdfFormatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
				String endDateFormatted = sdfFormatted.format(sd);

				for (String s : compolist.split(",")) {
					String tempSOQL = componentList.get(s);
					logger.fine("tempSql 3: " + tempSOQL);
					if (componentMetadataSet.contains(s)) {
						Map<String, String> metadataQueryDetails = metadataComponentList.get(s);
						metadataQueryDetails.put("endDate", endDateFormatted);
						metadataComponentList.put(s, metadataQueryDetails);
					} else {
						if (tempSOQL.indexOf(" WHERE ") == -1) {
							tempSOQL += " WHERE ";
						} else {
							tempSOQL += " AND ";
						}
						if (endDateFormatted != null && !endDateFormatted.isEmpty()) {
							componentList.put(s, tempSOQL + " LastModifiedDate <= " + endDateFormatted);
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		if (componentPrefix != null && !componentPrefix.isEmpty()) {
			try {
				for (String s : compolist.split(",")) {
					String tempSOQL = componentList.get(s);
					String nameSOQL = componentFullNameMap.get(s);
					logger.fine("s= " + s + " = " + componentFullNameMap);

					logger.fine("nameSOQL: " + nameSOQL);
					logger.fine("tempSql in compoenent before: " + tempSOQL);
					if (nameSOQL != null && nameSOQL != "") {
						if (componentMetadataSet.contains(s)) {
							Map<String, String> metadataQueryDetails = metadataComponentList.get(s);
							metadataQueryDetails.put("componentPrefix", componentPrefix);
							metadataComponentList.put(s, metadataQueryDetails);
						} else {
							if (tempSOQL.indexOf(" WHERE ") == -1) {
								tempSOQL += " WHERE ";
							} else {
								tempSOQL += " AND ";
							}
							componentList.put(s, tempSOQL + " (" + nameSOQL + " like '" + componentPrefix.toUpperCase()
									+ "%' OR " + nameSOQL + " like '" + componentPrefix.toLowerCase() + "%')");
						}
						logger.fine("After = " + componentList.get(s));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Fetch Component Session Details so that the fetched data can be
		// stored in session
		logger.fine("-- componentList2 --" + componentList);
		UserSessionDetails usrSessionDetails = LoginLogic.getSessionDetails(req);
		logger.info("-- User Session Details --" + usrSessionDetails);
		ComponentSessionDetails compSessionDetails = usrSessionDetails.getComponentSessionDetails();
		logger.info("-- Component Session Details --" + compSessionDetails);
		if (compSessionDetails == null) {
			compSessionDetails = new ComponentSessionDetails();
		}

		for (String s : compolist.split(",")) {
			if (DDConstants.TYPE_CUSTOM_FIELD.equals(s)) {
				String temp = componentList.get(s);
				if (temp.indexOf(" WHERE ") == -1) {
					temp += " WHERE ";
				} else {
					temp += " AND ";
				}
				//temp += " EntityDefinition.IsCustomizable = true AND EntityDefinition.IsQueryable = true AND NamespacePrefix = null";
				temp += " EntityDefinition.IsCustomizable = true AND NamespacePrefix = null ";
				temp += " AND (NOT DeveloperName LIKE '%_del__c') AND (NOT DeveloperName LIKE '%__x')";
				componentList.put(s, temp + " LIMIT " + DDConstants.DATA_DICTIONARY_SOQL_LIMIT);
				continue;
			}
			if (componentMetadataSet.contains(s)) {
				Map<String, String> metadataQueryDetails = metadataComponentList.get(s);
				metadataQueryDetails.put("Limit", String.valueOf(DDConstants.SOQL_LIMIT));
				metadataComponentList.put(s, metadataQueryDetails);
			}

			else
				componentList.put(s, componentList.get(s) + " LIMIT " + DDConstants.SOQL_LIMIT);
		}
		JSONObject json = new JSONObject();
		
		//Rabi: Feb 10, 2016: Modified logic to invoke queries using multi-threading
		ExecutorService executor = Executors.newFixedThreadPool(componentList.size() + metadataComponentList.size());
		List<Future<JSONObject>> resultList = new ArrayList<Future<JSONObject>>();		
				
		if (!componentList.isEmpty() || !metadataComponentList.isEmpty()) {
			try {
				if (!componentList.isEmpty()) {
					for (String compKey : componentList.keySet()) {
						DFDComponent comp = ComponentFactory.createComponent(compKey);
						comp.setToolingConnection(toolingConnection);
						if (comp != null) {
							Callable<JSONObject> callableTask = new CallableClass(comp, componentList.get(compKey), compSessionDetails);
							Future<JSONObject> future = executor.submit(callableTask);
							resultList.add(future);
							//JSONObject result = comp.getComponents(componentList.get(compKey), compSessionDetails);
							//json.accumulateAll(result);							
						}
					}
				}
				if (!metadataComponentList.isEmpty()) {
					for (String compKey : metadataComponentList.keySet()) {
						DFDMetadataComponent comp = ComponentFactory.createMetadataComponent(compKey);
						comp.setMetadataConnection(metadataConnection);
						if (comp != null) {
							Callable<JSONObject> callableTask = new CallableClass(comp, metadataComponentList.get(compKey), compSessionDetails);
							Future<JSONObject> future = executor.submit(callableTask);
							resultList.add(future);
							//JSONObject result = comp.getComponents(metadataComponentList.get(compKey),
							//		compSessionDetails);
							//json.accumulateAll(result);
						}
					}
				}
				for(Future<JSONObject> fut : resultList){
		              	json.accumulateAll(fut.get());		           
		        }
		        //shut down the executor service now
		        executor.shutdown();
				// return json;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		else {
			DFDesignerResponse dfdr = new DFDesignerResponse();
			dfdr.setSuccess(false);
			dfdr.setMsg("Please select some components to get data");
			String responseJSON = new Gson().toJson(dfdr);
			return responseJSON;
		}

		// Store component session details to the session
		usrSessionDetails.setComponentSessionDetails(compSessionDetails);
		req.getSession(false).setAttribute(DDConstants.SESSION_KEY, usrSessionDetails);

		return json.toString();
	}

	public static List<ApexClass> generateApexClassList(List<SObject> sobjList) {
		List<ApexClass> tempApexClassList = new ArrayList<ApexClass>();
		for (int i = 0; i < sobjList.size(); ++i) {
			ApexClass classRecord = (ApexClass) sobjList.get(i);
			tempApexClassList.add(classRecord);
		}
		return tempApexClassList;
	}

	public static List<ApexTrigger> generateApexTriggerList(List<SObject> sobjList) {
		List<ApexTrigger> tempApexTriggerList = new ArrayList<ApexTrigger>();
		for (int i = 0; i < sobjList.size(); ++i) {
			ApexTrigger classRecord = (ApexTrigger) sobjList.get(i);
			tempApexTriggerList.add(classRecord);
		}
		return tempApexTriggerList;
	}



	public static List<ValidationRule> generateValidationRuleList(List<SObject> sobjList) {
		List<ValidationRule> validationRuleList = new ArrayList<ValidationRule>();
		for (int i = 0; i < sobjList.size(); ++i) {
			ValidationRule classRecord = (ValidationRule) sobjList.get(i);
			validationRuleList.add(classRecord);
		}
		return validationRuleList;
	}

	public static List<CustomField> generateCustomFieldList(List<SObject> sobjList) {
		List<CustomField> tempCustomFieldList = new ArrayList<CustomField>();
		for (int i = 0; i < sobjList.size(); ++i) {
			CustomField fieldRecord = (CustomField) sobjList.get(i);
			tempCustomFieldList.add(fieldRecord);
		}
		return tempCustomFieldList;
	}

	public static List<ApexPage> generateApexPagesList(List<SObject> sobjList) {
		List<ApexPage> tempCustomFieldList = new ArrayList<ApexPage>();
		for (int i = 0; i < sobjList.size(); ++i) {
			ApexPage fieldRecord = (ApexPage) sobjList.get(i);
			tempCustomFieldList.add(fieldRecord);
		}
		return tempCustomFieldList;
	}

	

	public static List<StaticResource> generateStaticResourceList(List<SObject> sobjList) {
		List<StaticResource> staticResourceList = new ArrayList<StaticResource>();
		for (int i = 0; i < sobjList.size(); ++i) {
			StaticResource res = (StaticResource) sobjList.get(i);
			staticResourceList.add(res);
		}
		return staticResourceList;
	}

	public static List<WorkflowRule> generateWorkflowRuleList(List<SObject> sobjList) {
		List<WorkflowRule> workflowRuleList = new ArrayList<WorkflowRule>();
		for (int i = 0; i < sobjList.size(); ++i) {
			WorkflowRule classRecord = (WorkflowRule) sobjList.get(i);
			workflowRuleList.add(classRecord);
		}
		return workflowRuleList;
	}

	public static List<WorkflowFieldUpdate> generateWorkflowFieldUpdateList(List<SObject> sobjList) {
		List<WorkflowFieldUpdate> workflowFieldUpdateList = new ArrayList<WorkflowFieldUpdate>();
		for (int i = 0; i < sobjList.size(); ++i) {
			WorkflowFieldUpdate classRecord = (WorkflowFieldUpdate) sobjList.get(i);
			workflowFieldUpdateList.add(classRecord);
		}
		return workflowFieldUpdateList;
	}

	public static List<WorkflowOutboundMessage> generateWorkflowOutboundMessageList(List<SObject> sobjList) {
		List<WorkflowOutboundMessage> workflowOutboundMessageList = new ArrayList<WorkflowOutboundMessage>();
		for (int i = 0; i < sobjList.size(); ++i) {
			WorkflowOutboundMessage classRecord = (WorkflowOutboundMessage) sobjList.get(i);
			workflowOutboundMessageList.add(classRecord);
		}
		return workflowOutboundMessageList;
	}

	public static List<WorkflowAlert> generateWorkflowAlertList(List<SObject> sobjList) {
		List<WorkflowAlert> workflowAlertList = new ArrayList<WorkflowAlert>();
		for (int i = 0; i < sobjList.size(); ++i) {
			WorkflowAlert classRecord = (WorkflowAlert) sobjList.get(i);
			workflowAlertList.add(classRecord);
		}
		return workflowAlertList;
	}

	public static List<CustomTab> generateCustomTabList(List<SObject> sobjList) {
		List<CustomTab> CustomTabList = new ArrayList<CustomTab>();
		for (int i = 0; i < sobjList.size(); ++i) {
			CustomTab res = (CustomTab) sobjList.get(i);
			CustomTabList.add(res);
		}
		return CustomTabList;
	}


	public static List<Profile> generateProfileList(List<SObject> sobjList) {
		List<Profile> profileList = new ArrayList<Profile>();
		for (int i = 0; i < sobjList.size(); ++i) {
			Profile prof = (Profile) sobjList.get(i);
			profileList.add(prof);
		}
		return profileList;
	}	
	public static List<RecordType> generateRecordTypeList(List<SObject> sobjList) {
		List<RecordType> recordTypeList = new ArrayList<RecordType>();
		for (int i = 0; i < sobjList.size(); ++i) {
			RecordType classRecord = (RecordType) sobjList.get(i);
			recordTypeList.add(classRecord);
		}
		return recordTypeList;
	}

	public static List<ApexComponent> generateApexComponentList(List<SObject> sobjList) {
		List<ApexComponent> tempApexComponentList = new ArrayList<ApexComponent>();
		for (int i = 0; i < sobjList.size(); ++i) {
			ApexComponent apexComponentRecord = (ApexComponent) sobjList.get(i);
			tempApexComponentList.add(apexComponentRecord);
		}
		return tempApexComponentList;
	}

	public static List<FileProperties> generateApprovalProcessList(List<FileProperties> metadataList,
			MetadataConnection connection, Map<String, String> soql, String flag) {
		List<FileProperties> tempApprovalProcessList = new ArrayList<FileProperties>();
		List<String> approvalProcessFullNames = new ArrayList<String>();
		Map<String, FileProperties> mapFP = new HashMap<String, FileProperties>();
		for (FileProperties fp : metadataList) {
			if (flag.equals("fetch")) {
				if (fp != null
						// && (soql.get("componentPrefix")!= null &&
						// soql.get("componentPrefix")!="")
						&& (soql.get("componentPrefix") != null && soql.get("componentPrefix") != "")
								? ((fp.getFullName().split("\\.")[1]).toLowerCase())
										.startsWith(soql.get("componentPrefix").toLowerCase())
								: true && approvalProcessFullNames.size() < (Integer.valueOf(soql.get("Limit")))) {
					approvalProcessFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
			if (flag.equals("generate")) {
				if (fp != null) {
					String nameDesc[] = (fp.getFullName()).split("\\|");
					approvalProcessFullNames.add(nameDesc[0]);
					mapFP.put(nameDesc[0], fp);
				}
			}
		}
		List<Metadata> lstMetadata = getDataFromReadMetadata(connection, DDConstants.TYPE_APPROVAL_PROCESS,
				approvalProcessFullNames);
		Metadata[] mdInfo = null;
		mdInfo = lstMetadata.toArray(new Metadata[lstMetadata.size()]);

		for (Metadata md : mdInfo) {
			if (md != null) {
				ApprovalProcess obj = (ApprovalProcess) md;
				logger.fine("full name: " + obj.getFullName());
				logger.fine("Description: " + obj.getDescription());
				FileProperties fileProp = mapFP.get(obj.getFullName());
				if (obj.getDescription() != null)
					fileProp.setFullName(obj.getFullName() + "|" + obj.getDescription());
				else
					fileProp.setFullName(obj.getFullName() + "|" + "  ");
				mapFP.put(obj.getFullName(), fileProp);
			} else {
				logger.info("Empty metadata.");
			}
		}

		for (String fp : mapFP.keySet()) {
			tempApprovalProcessList.add(mapFP.get(fp));
		}
		return tempApprovalProcessList;
	}


	public static List<FileProperties> generateCustomObjectList(List<FileProperties> metadataList,
							MetadataConnection connection, Map<String, String> soql, String flag) {
		
		List<FileProperties> tempCustomObjectList = new ArrayList<FileProperties>();
		List<String> customObjectFullNames = new ArrayList<String>();
		List<CustomObject> customObjectList = new ArrayList<CustomObject>();
		Map<String, FileProperties> mapFP = new HashMap<String, FileProperties>();


		for (FileProperties fp : metadataList) {
			
			String objFullName = fp.getFullName();
			if(  fp.getNamespacePrefix() != null && !objFullName.startsWith( fp.getNamespacePrefix() + "__" ) ) {
				objFullName = fp.getNamespacePrefix() + "__" + objFullName ;
			}

			if (flag.equals("fetch")) {
				if (fp != null
						// && (soql.get("componentPrefix")!= null &&
						// soql.get("componentPrefix")!="")
						&& (soql.get("componentPrefix") != null && soql.get("componentPrefix") != "")
								? ((fp.getFullName().split("\\.")[1]).toLowerCase())
										.startsWith(soql.get("componentPrefix").toLowerCase())
								: true && customObjectFullNames.size() < (Integer.valueOf(soql.get("Limit")))) {
					if( !mapFP.containsKey( objFullName ) ) {
						customObjectFullNames.add( objFullName );
						mapFP.put(objFullName, fp);
					}
				}
			}
			if (flag.equals("generate")) {
				if (fp != null && !mapFP.containsKey( objFullName ) ) {
					customObjectFullNames.add(objFullName);
					mapFP.put(objFullName, fp);
				}
			}
		}
		List<Metadata> lstMetadata = getDataFromReadMetadata(connection, DDConstants.TYPE_CUSTOM_OBJECT,
				customObjectFullNames);
		Metadata[] mdInfo = null;
		mdInfo = lstMetadata.toArray(new Metadata[lstMetadata.size()]);
		for (Metadata md : mdInfo) {
			if (md != null) {
				CustomObject obj = (CustomObject) md;
				
				if(obj.getCustomSettingsType() == null ){
					
					FileProperties fileProp = mapFP.get(obj.getFullName());
					if (obj.getDescription() != null) {
						fileProp.setFullName(obj.getFullName() + "|" + obj.getDescription() );
					}
					else {
						fileProp.setFullName(obj.getFullName() + "|" + "  ");
					}
					if(mapFP.containsKey(obj.getFullName()) && mapFP.get(obj.getFullName()).getFileName().contains("/")) {
						fileProp.setFileName(mapFP.get(obj.getFullName()).getFileName().split("/")[1] + "|" + obj.getLabel());
					}
					else if( mapFP.containsKey(obj.getFullName()) ){
						fileProp.setFileName(mapFP.get(obj.getFullName()).getFileName() + "|" + obj.getLabel());	
					}
					else {
						fileProp.setFileName( "|" + obj.getLabel());	
					}
					mapFP.put(obj.getFullName(), fileProp);
					tempCustomObjectList.add(fileProp);
				}

			} else {
				logger.info("Empty metadata.");
			}
		}

		
		return tempCustomObjectList;
	}



	public static List<FileProperties> generateCustomSettingList(List<FileProperties> metadataList,
								MetadataConnection connection, Map<String, String> soql, String flag){

		List<FileProperties> tempCustomSettingList = new ArrayList<FileProperties>();
		List<String> customObjectFullNames = new ArrayList<String>();
		List<CustomObject> customSettingsList = new ArrayList<CustomObject>();
		Map<String, FileProperties> mapFP = new HashMap<String, FileProperties>();

		for (FileProperties fp : metadataList) {
			String objFullName = fp.getFullName();
			if( fp.getNamespacePrefix() != null && !objFullName.startsWith( fp.getNamespacePrefix() + "__" ) ) {
				objFullName = fp.getNamespacePrefix() + "__" + objFullName ;
			}

			if (flag.equals("fetch")) {
				if (fp != null
						// && (soql.get("componentPrefix")!= null &&
						// soql.get("componentPrefix")!="")
						&& (soql.get("componentPrefix") != null && soql.get("componentPrefix") != "")
								? ((fp.getFullName().split("\\.")[1]).toLowerCase())
										.startsWith(soql.get("componentPrefix").toLowerCase())
								: true && customObjectFullNames.size() < (Integer.valueOf(soql.get("Limit")))) {
					if( !mapFP.containsKey( objFullName ) ) {
						customObjectFullNames.add(objFullName);
						mapFP.put(objFullName, fp);
					}
				}
			}
			if (flag.equals("generate")) {
				if (fp != null && !mapFP.containsKey( objFullName ) ) {
					customObjectFullNames.add(objFullName);
					mapFP.put(objFullName, fp);
				}
			}
		}
		List<Metadata> lstMetadata = getDataFromReadMetadata(connection, DDConstants.TYPE_CUSTOM_OBJECT,
				customObjectFullNames);
		Metadata[] mdInfo = null;
		mdInfo = lstMetadata.toArray(new Metadata[lstMetadata.size()]);
		//mapFP = new HashMap<String, FileProperties>();
		for (Metadata md : mdInfo) {
			if (md != null) {
				CustomObject obj = (CustomObject) md;
				//logger.info("====ReadMetata==="+obj.getCustomSettingsType());
				if(obj.getCustomSettingsType() != null && obj.getCustomSettingsType().values()[0].toString() != "" ){
					FileProperties fileProp = mapFP.get(obj.getFullName());
					if (obj.getDescription() != null)
						fileProp.setFullName(obj.getFullName() + "|" + obj.getDescription());
					else
						fileProp.setFullName(obj.getFullName() + "|" + "  ");
					if( mapFP.containsKey(obj.getFullName()) && mapFP.get(obj.getFullName()).getFileName().contains("/") ) {
						fileProp.setFileName(mapFP.get(obj.getFullName()).getFileName().split("/")[1] + "|" + obj.getCustomSettingsType() + "|" + obj.getLabel() + "|" + obj.getVisibility());
					}
					else if( mapFP.containsKey( obj.getFullName() ) ) {
						fileProp.setFileName(mapFP.get(obj.getFullName()).getFileName() + "|" + obj.getCustomSettingsType() + "|" + obj.getLabel() + "|" + obj.getVisibility());	
					}
					else {
						fileProp.setFileName( "|" + obj.getCustomSettingsType() + "|" + obj.getLabel() + "|" + obj.getVisibility());		
					}
					mapFP.put(obj.getFullName(), fileProp);
					tempCustomSettingList.add(fileProp);
				}

			} else {
				logger.info("Empty metadata.");
			}
		}

		
		return tempCustomSettingList;

		
	}


	//	For User Role
	public static List<FileProperties> generateRoleList(List<FileProperties> metadataList,
			MetadataConnection connection, Map<String, String> soql, String flag) {
		List<FileProperties> tempRoleList = new ArrayList<FileProperties>();
		List<String> roleFullNames = new ArrayList<String>();
		Map<String, FileProperties> mapFP = new HashMap<String, FileProperties>();
		for (FileProperties fp : metadataList) {
			if (flag.equals("fetch")) {
				if (fp != null
						// && (soql.get("componentPrefix")!= null &&
						// soql.get("componentPrefix")!="")
						&& (soql.get("componentPrefix") != null && soql.get("componentPrefix") != "")
								? ((fp.getFullName().split("\\.")[1]).toLowerCase())
										.startsWith(soql.get("componentPrefix").toLowerCase())
								: true && roleFullNames.size() < (Integer.valueOf(soql.get("Limit")))) {
					roleFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
			if (flag.equals("generate")) {
				if (fp != null) {
					roleFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
		}
		List<Metadata> lstMetadata = getDataFromReadMetadata(connection, DDConstants.TYPE_USER_ROLE,
				roleFullNames);
		Metadata[] mdInfo = null;
		mdInfo = lstMetadata.toArray(new Metadata[lstMetadata.size()]);

		for (Metadata md : mdInfo) {
			if (md != null) {
				Role obj = (Role) md;
				logger.info("full name: " + obj.getFullName());
				logger.info("Name : " + obj.getName());
				logger.info("Parent Role : " + obj.getParentRole());
				logger.info("Description: " + obj.getDescription());
				FileProperties fileProp = mapFP.get(obj.getFullName());
				
				String str = obj.getFullName() + "|" + obj.getName() + "|" + obj.getParentRole();				
				fileProp.setFullName(str);
				
//				if (obj.getDescription() != null)
//					fileProp.setFullName(obj.getFullName() + "|" + obj.getDescription());
//				else
//					fileProp.setFullName(obj.getFullName() + "|" + "  ");
				mapFP.put(obj.getFullName(), fileProp);
			} else {
				logger.info("Empty metadata.");
			}
		}
		for (String fp : mapFP.keySet()) {
			tempRoleList.add(mapFP.get(fp));
		}
		return tempRoleList;
	}
	
	

	public static List<FileProperties> generatePermissionSetList(List<FileProperties> metadataList,
			MetadataConnection connection, Map<String, String> soql, String flag) {
		List<FileProperties> tempPermissionSetList = new ArrayList<FileProperties>();
		List<String> permissionSetFullNames = new ArrayList<String>();
		Map<String, FileProperties> mapFP = new HashMap<String, FileProperties>();
		for (FileProperties fp : metadataList) {
			if (flag.equals("fetch")) {
				if (fp != null
						&& (soql.get("componentPrefix") != null && soql.get("componentPrefix") != "")
								? ((fp.getFullName().split("\\.")[1]).toLowerCase())
										.startsWith(soql.get("componentPrefix").toLowerCase())
								: true && permissionSetFullNames.size() < (Integer.valueOf(soql.get("Limit")))) {
					permissionSetFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
			if (flag.equals("generate")) {
				if (fp != null) {
					permissionSetFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
		}
		List<Metadata> lstMetadata = getDataFromReadMetadata(connection, DDConstants.TYPE_PERMISSION_SET,
				permissionSetFullNames);
		Metadata[] mdInfo = null;
		mdInfo = lstMetadata.toArray(new Metadata[lstMetadata.size()]);

		for (Metadata md : mdInfo) {
			if (md != null) {
				PermissionSet obj = (PermissionSet) md;
				logger.fine("full name: " + obj.getFullName());
				logger.fine("Description: " + obj.getDescription());
				FileProperties fileProp = mapFP.get(obj.getFullName());
				if (obj.getDescription() != null)
					fileProp.setFullName(obj.getFullName() + "|" + obj.getDescription());
				else
					fileProp.setFullName(obj.getFullName() + "|" + "  ");
				mapFP.put(obj.getFullName(), fileProp);
			} else {
				logger.info("Empty metadata.");
			}
		}

		for (String fp : mapFP.keySet()) {
			tempPermissionSetList.add(mapFP.get(fp));
		}
		return tempPermissionSetList;
	}

	//Added By Nitin
	public static List<FileProperties> generateBusinessProcessList(List<FileProperties> metadataList,
			MetadataConnection connection, Map<String, String> soql, String flag) {

		List<FileProperties> tempBusinessProcessList = new ArrayList<FileProperties>();
		List<String> businessProcessFullNames = new ArrayList<String>();
		Map<String, FileProperties> mapFP = new HashMap<String, FileProperties>();
		for (FileProperties fp : metadataList) {
			if (flag.equals("fetch")) {
				if (fp != null
						// && (soql.get("componentPrefix")!= null &&
						// soql.get("componentPrefix")!="")
						&& (soql.get("componentPrefix") != null && soql.get("componentPrefix") != "")
								? ((fp.getFullName().split("\\.")[1]).toLowerCase())
										.startsWith(soql.get("componentPrefix").toLowerCase())
								: true && businessProcessFullNames.size() < (Integer.valueOf(soql.get("Limit")))) {
					businessProcessFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
			if (flag.equals("generate")) {
				if (fp != null) {
					businessProcessFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
		}
		List<Metadata> lstMetadata = getDataFromReadMetadata(connection, DDConstants.TYPE_BUSINESS_PROCESS,
				businessProcessFullNames);
		Metadata[] mdInfo = null;
		mdInfo = lstMetadata.toArray(new Metadata[lstMetadata.size()]);

		for (Metadata md : mdInfo) {
			if (md != null) {
				BusinessProcess obj = (BusinessProcess) md;
				FileProperties fileProp = mapFP.get(obj.getFullName());
				if (obj.getDescription() != null)
					fileProp.setFullName(obj.getFullName() + "|" + obj.getDescription());
				else
					fileProp.setFullName(obj.getFullName() + "|" + "  ");
				
				if( mapFP.containsKey(obj.getFullName()) && mapFP.get(obj.getFullName()).getFileName().contains("/") ) {
					fileProp.setFileName(mapFP.get(obj.getFullName()).getFileName().split("/")[1]+'/'+obj.getIsActive());
				}
				else if( mapFP.containsKey( obj.getFullName() ) ) {
					fileProp.setFileName(mapFP.get(obj.getFullName()).getFileName()+"/"+obj.getIsActive());
				}
				else {
					fileProp.setFileName("/"+obj.getIsActive());
				}
				
				mapFP.put(obj.getFullName(), fileProp);
			} else {
				logger.info("Empty metadata.");
			}
		}

		for (String fp : mapFP.keySet()) {
			tempBusinessProcessList.add(mapFP.get(fp));
		}
		return tempBusinessProcessList;
	}



	public static List<EmailTemplate> generateEmailTemplateList(List<SObject> sobjList) {
		List<EmailTemplate> tempEmailTemplateList = new ArrayList<EmailTemplate>();
		for (int i = 0; i < sobjList.size(); ++i) {
			EmailTemplate emailTemplateRecord = (EmailTemplate) sobjList.get(i);
			tempEmailTemplateList.add(emailTemplateRecord);
		}
		return tempEmailTemplateList;
	}

	public static List<AssignmentRule> generateAssignmentRuleList(List<SObject> sobjList) {
		List<AssignmentRule> assignmentRuleList = new ArrayList<AssignmentRule>();
		for (int i = 0; i < sobjList.size(); ++i) {
			AssignmentRule assignmentRuleRecord = (AssignmentRule) sobjList.get(i);
			assignmentRuleList.add(assignmentRuleRecord);
		}
		return assignmentRuleList;
	}


	public static List<FileProperties> generateCustomLabelList(List<FileProperties> metadataList,
			MetadataConnection connection, Map<String, String> soql, String flag) {
		List<FileProperties> tempCustomLabelList = new ArrayList<FileProperties>();
		List<String> customLabelsFullNames = new ArrayList<String>();
		Map<String, FileProperties> mapFP = new HashMap<String, FileProperties>();
		for (FileProperties fp : metadataList) {
			if (flag.equals("fetch")) {
				if (fp != null && (soql.get("componentPrefix") != null && soql.get("componentPrefix") != "")
						? ((fp.getFullName().split("\\.")[1]).toLowerCase())
								.startsWith(soql.get("componentPrefix").toLowerCase())
						: true && customLabelsFullNames.size() < (Integer.valueOf(soql.get("Limit")))) {
					customLabelsFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
			if (flag.equals("generate")) {
				if (fp != null) {
					customLabelsFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
		}
		List<Metadata> lstMetadata = getDataFromReadMetadata(connection, DDConstants.TYPE_CUSTOM_LABEL,
				customLabelsFullNames);

		Metadata[] mdInfo = null;
		mdInfo = lstMetadata.toArray(new Metadata[lstMetadata.size()]);
		for (Metadata md : mdInfo) {
			if (md != null) {
				CustomLabel obj = (CustomLabel) md;
				FileProperties fileProp = mapFP.get(obj.getFullName());
				fileProp.setFullName(obj.getFullName() + "|" + (obj.getCategories() != null ? obj.getCategories() : "")
						+ "|" + (obj.getShortDescription() != null ? obj.getShortDescription() : "") + "|"
						+ (obj.getValue() != null ? obj.getValue() : ""));
				mapFP.put(obj.getFullName(), fileProp);
			} else {
				logger.info("Empty metadata.");
			}
		}

		for (String fp : mapFP.keySet()) {
			tempCustomLabelList.add(mapFP.get(fp));
		}
		return tempCustomLabelList;
	}
	
	public static List<FileProperties> generateSharingRulesList(List<FileProperties> metadataList,
			MetadataConnection connection, Map<String, String> soql, String flag) {
		List<FileProperties> tempSharingRuleList = new ArrayList<FileProperties>();
		List<String> sharingRulesFullNames = new ArrayList<String>();
		Map<String, FileProperties> mapFP = new HashMap<String, FileProperties>();
		for (FileProperties fp : metadataList) {
			if (flag.equals("fetch")) {
				if (fp != null
						// && (soql.get("componentPrefix")!= null &&
						// soql.get("componentPrefix")!="")
						&& (soql.get("componentPrefix") != null && soql.get("componentPrefix") != "")
								? ((fp.getFullName().split("\\.")[1]).toLowerCase())
										.startsWith(soql.get("componentPrefix").toLowerCase())
								: true && sharingRulesFullNames.size() < (Integer.valueOf(soql.get("Limit")))) {
					sharingRulesFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
			if (flag.equals("generate")) {
				if (fp != null) {
					sharingRulesFullNames.add(fp.getFullName());
					mapFP.put(fp.getFullName(), fp);
				}
			}
		}
		
		for (String fp : mapFP.keySet()) {
			tempSharingRuleList.add(mapFP.get(fp));
		}
		return tempSharingRuleList;
	}
	
	public static List<WorkflowTask> generateWorkflowTaskList(List<SObject> sobjList) {
		List<WorkflowTask> tempWorkflowTaskList = new ArrayList<WorkflowTask>();
		for (int i = 0; i < sobjList.size(); ++i) {
			WorkflowTask workflowTaskRecord = (WorkflowTask) sobjList.get(i);
			tempWorkflowTaskList.add(workflowTaskRecord);
		}
		return tempWorkflowTaskList;
	}
	/** Added By Nitin**/
	/*public static List<HomePageComponent> generateHomePageComponentList(List<SObject> sobjList) {
		List<HomePageComponent> tempHomePageComponentList = new ArrayList<HomePageComponent>();
		for (int i = 0; i < sobjList.size(); ++i) {
			HomePageComponent classRecord = (HomePageComponent) sobjList.get(i);
			tempHomePageComponentList.add(classRecord);
		}
		return tempHomePageComponentList;
	}
	*/
	/*public static List<BusinessProcess> generateBusinessProcessList(List<SObject> sobjList) {
		List<BusinessProcess> tempBusinessProcessList = new ArrayList<BusinessProcess>();
		for (int i = 0; i < sobjList.size(); ++i) {
			BusinessProcess classRecord = (BusinessProcess) sobjList.get(i);
			tempBusinessProcessList.add(classRecord);
		}
		return tempBusinessProcessList;
	}*/
	


	/*****/
	public static List<Metadata> getDataFromReadMetadata(MetadataConnection connection, String ComponentName,
			List<String> fullNames) {
		List<Metadata> lstMetadata = new ArrayList<Metadata>();
		int componentCount = fullNames.size();
		int j = componentCount / 1;
		int k = 0;
		if (componentCount % 1 == 0)
			k = j;
		else
			k = j + 1;

		ReadResult readRes = null;
		Metadata[] mdInfo = null;
		int startIndex = 0;
		int endIndex = 0;
		logger.fine("-- componentCount --" + componentCount);
		for (int i = 0; i < k; i++) {
			if (i == (k - 1))
				endIndex = componentCount;
			else
				endIndex = startIndex + 1;
			List<String> sublist = fullNames.subList(startIndex, endIndex);
			logger.info("sublist###### " + sublist);
			try {
				readRes = connection.readMetadata(ComponentName, sublist.toArray(new String[sublist.size()]));
			} catch (ConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (readRes != null) {
				mdInfo = readRes.getRecords();
				for (int a = 0; a < mdInfo.length; a++) {
					lstMetadata.add(mdInfo[a]);
				}
			}
			startIndex = startIndex + 1;
		}
		return lstMetadata;
	}

	// Executes the query and returns the resultant set of records
	public List<SObject> queryRecords(SoapConnection connection, String soqlQuery) throws DFDException {
		if (connection == null || soqlQuery == null) {
			throw new DFDException("Bad Connection");
		}
		logger.info("DDUtilClass>queryRecords> soqlQuery :" + soqlQuery);
		QueryResult qResult = null;
		List<SObject> sobjList = null;
		try {
			qResult = connection.query(soqlQuery);
			boolean done = false;
			if (qResult.getSize() > 0) {
				logger.info("DDUtilClass>queryRecords> Logged-in user can see a total of " + qResult.getSize()
						+ " records.");
				sobjList = new ArrayList<SObject>();
				while (!done) {
					SObject[] records = qResult.getRecords();
					sobjList.addAll(Arrays.asList(records));
					if (qResult.isDone()) {
						done = true;
					} else {
						qResult = connection.queryMore(qResult.getQueryLocator());
					}
				}
			} else {
				logger.info("DDUtilClass>queryRecords> No records found.");
			}
			logger.info("DDUtilClass>queryRecords> Query succesfully executed.");
		} catch (ConnectionException ce) {
			logger.info("DDUtilClass>queryRecords>Exception: " + ce.getMessage());
			ce.printStackTrace();
		}
		return sobjList;
	}

	// Executes the query and returns the resultant set of records (Metadata
	// Connection)
	public List<FileProperties> queryRecordsMetadataComponents(MetadataConnection connection, Map<String, String> soql,
			String ComponentName, String flag) throws DFDException {
		if (connection == null || ComponentName == null) {
			throw new DFDException("Bad Connection");
		}
		logger.info("DDUtilClass>queryRecords>MetadataConnection ComponentName :" + ComponentName);

		List<FileProperties> filePropList = new ArrayList<FileProperties>();
		double asOfVersion = 34.0;

		try {
			Set<String> userIdSet = new HashSet<String>();
			// Integer Limit = 0;
			Calendar lastModifiedStart = Calendar.getInstance();
			Calendar lastModifiedEnd = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);

			ListMetadataQuery lstMetadataQuery = new ListMetadataQuery();
			lstMetadataQuery.setType(ComponentName);

			String query[] = null;

			if (flag.equals("fetch")) {
				userIdSet = getUsers(soql.get("userId"));
				if (soql.get("startDate") != null && !(soql.get("startDate")).isEmpty()) {
					lastModifiedStart.setTime(sdf.parse(soql.get("startDate")));
				}
				if (soql.get("endDate") != null && !(soql.get("endDate")).isEmpty()) {
					lastModifiedEnd.setTime(sdf.parse(soql.get("endDate")));
				}
			}

			if (flag.equals("generate")) {
				query = soql.get("componentId").split(",");
			}
			FileProperties[] listMetadataInfo = connection.listMetadata(new ListMetadataQuery[] { lstMetadataQuery },
					asOfVersion);
			FileProperties[] lmr = getSortedMetadata(listMetadataInfo);

			if (lmr != null) {
				for (FileProperties n : lmr) {
					if (flag.equals("fetch")) {
						if (((userIdSet != null) ? userIdSet.contains(n.getLastModifiedById()) : true)
								&& (!soql.get("startDate").equals("")
										? (n.getLastModifiedDate().after(lastModifiedStart)) : true)
								&& (!soql.get("endDate").equals("") ? (n.getLastModifiedDate().before(lastModifiedEnd))
										: true)) {
							filePropList.add(n);
						}
						
					}
					if (flag.equals("generate")) {
						if (new HashSet<String>(Arrays.asList(query)).contains(n.getId())) {
							filePropList.add(n);
						}
					}
				}
			}

			if (filePropList.size() > 0) {
				logger.info("DDUtilClass>queryRecordsMetadata>" + ComponentName + " Logged-in user can see a total of "
						+ filePropList.size() + " records.");
			} else {
				logger.info("DDUtilClass>queryRecordsMetadata> No records found.");
			}
			logger.info("DDUtilClass>queryRecordsMetadata> succesfully executed.");
		} catch (ConnectionException ce) {
			ce.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return metadataList;
		return filePropList;
	}

	public String quoteKeySet(Set<String> mapKeySet) {
		String newSetStr = "";
		for (String str : mapKeySet)
			newSetStr += "'" + str + "',";

		newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0, newSetStr.lastIndexOf(',')) + ')'
				: newSetStr;
		logger.fine("DDUtilClass>quoteKeySet> newSetStr result:--- " + newSetStr);
		return newSetStr;

	}

	// generic method to write into XLS sheet.
	public static void writeToExcel(HttpServletRequest request, HttpServletResponse response,
			ServletContext servletContext, String projectName, SoapConnection toolingConnection,
			PartnerConnection partnerConnection) {
		// Blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		String checkedCustomFields = request.getParameter("hidden_customFieldCheck");
		
		logger.fine("checked fields are:" + checkedCustomFields);
		if (checkedCustomFields != null && !checkedCustomFields.isEmpty()) {
			Map<String, List<String>> fieldMap = createFieldMapForDD(checkedCustomFields, servletContext, toolingConnection, partnerConnection);

			Map<String, XSSFSheet> worksheetsMap = new HashMap<String, XSSFSheet>();
			// Create a blank sheet
			XSSFSheet sheet = null;

			Map<String, Object[]> data = new TreeMap<String, Object[]>();
			XSSFCellStyle bodyCellStyle = getBodyCellStyle(workbook.createCellStyle());
			int rownum = 1;
			
			if( fieldMap != null ) {
				// Iterate over data and write to sheet
				for (String key : fieldMap.keySet()) {
					List<String> rowList;
					rowList = fieldMap.get(key);
					if( !worksheetsMap.containsKey( rowList.get(3) ) ) {
						//Create a blank sheet for object
						worksheetsMap.put( rowList.get(3), createWorkbookSheet( rowList.get(3), workbook ) );
					}
					sheet = worksheetsMap.get( rowList.get(3) );
					rownum = sheet.getPhysicalNumberOfRows();
					XSSFRow row = sheet.createRow(rownum++);
					data.put(key,
							new Object[] { rowList.get(0), rowList.get(1), rowList.get(2), rowList.get(3), rowList.get(4),
									rowList.get(5), rowList.get(6), rowList.get(7), rowList.get(8), rowList.get(9),
									rowList.get(10) });
					Object[] objArr = data.get(key);
					int cellnum = 0;
					for (Object obj : objArr) {
						XSSFCell cell = row.createCell(cellnum++);
						cell.setCellStyle(bodyCellStyle);

						if (obj instanceof String)
							cell.setCellValue((String) obj);
						else if (obj instanceof Integer)
							cell.setCellValue((Integer) obj);
					}
				}
				
				for( XSSFSheet s :worksheetsMap.values() ) {
					setColumnWidths(s);
				}
			}

			try {
				sendExcelToDownload(request, response, workbook, projectName, servletContext);
				logger.info("DDUtilClass>writeToExcel> xlsx file sent for download");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					workbook.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private static Map<String, List<String>> createFieldMapForDD(String checkedCustomFields, ServletContext servletContext, 
					SoapConnection toolingConnection, PartnerConnection partnerConnection) {
		
		String[] checkedFields = checkedCustomFields.split(",");

		Set<String> fieldIdSet = new HashSet<String>();

		LoginLogic l = new LoginLogic();
		Map<String, List<String>> fieldMap = new HashMap<String, List<String>>();

		if (checkedFields != null && checkedFields.length != 0) {
			fieldIdSet.addAll(Arrays.asList(checkedFields));
			fieldMap = l.fetchCustomFieldsMethod(fieldIdSet, servletContext, toolingConnection, partnerConnection);
		}

		logger.fine("selected fields are :" + fieldIdSet);
		return fieldMap;
	}

	private static XSSFSheet createWorkbookSheet(String sheetName, XSSFWorkbook workbook) {
		XSSFSheet sheet = workbook.createSheet(sheetName);
		XSSFCellStyle headerCellStyle = getHeaderCellStyle(workbook.createCellStyle(), workbook.createFont());

		XSSFRow defRow = sheet.createRow(0);
		XSSFCell cell1 = defRow.createCell(0);
		cell1.setCellValue("Field Label");
		cell1.setCellStyle(headerCellStyle);

		XSSFCell cell2 = defRow.createCell(1);
		cell2.setCellValue("Field Name");
		cell2.setCellStyle(headerCellStyle);

		XSSFCell cell3 = defRow.createCell(2);
		cell3.setCellValue("Object Label");
		cell3.setCellStyle(headerCellStyle);

		XSSFCell cell4 = defRow.createCell(3);
		cell4.setCellValue("Object API Name");
		cell4.setCellStyle(headerCellStyle);

		XSSFCell cell5 = defRow.createCell(4);
		cell5.setCellValue("Data Type");
		cell5.setCellStyle(headerCellStyle);

		XSSFCell cell6 = defRow.createCell(5);
		cell6.setCellValue("Length");
		cell6.setCellStyle(headerCellStyle);

		XSSFCell cell7 = defRow.createCell(6);
		cell7.setCellValue("Mandatory");
		cell7.setCellStyle(headerCellStyle);

		XSSFCell cell8 = defRow.createCell(7);
		cell8.setCellValue("Description");
		cell8.setCellStyle(headerCellStyle);

		XSSFCell cell9 = defRow.createCell(8);
		cell9.setCellValue("Picklist Values");
		cell9.setCellStyle(headerCellStyle);

		XSSFCell cell10 = defRow.createCell(9);
		cell10.setCellValue("Help Text");
		cell10.setCellStyle(headerCellStyle);

		XSSFCell cell11 = defRow.createCell(10);
		cell11.setCellValue("External Id");
		cell11.setCellStyle(headerCellStyle);

		return sheet;
	}

	private static XSSFCellStyle getHeaderCellStyle(XSSFCellStyle headerCellStyle, XSSFFont headerFont) {
		//Create HEADER cell style
		headerCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		headerCellStyle.setFillForegroundColor(new HSSFColor.DARK_BLUE().getIndex());
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
		headerCellStyle.setWrapText(true);
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);

		//Set font style
		headerFont.setBold(true);
		headerFont.setColor(new XSSFColor(Color.WHITE));

		headerCellStyle.setFont(headerFont);

		return headerCellStyle;		
	}

	private static XSSFCellStyle getBodyCellStyle(XSSFCellStyle bodyCellStyle ) {

		bodyCellStyle.setWrapText(true);
		bodyCellStyle.setBorderBottom(BorderStyle.THIN);
		bodyCellStyle.setBorderTop(BorderStyle.THIN);
		bodyCellStyle.setBorderRight(BorderStyle.THIN);
		bodyCellStyle.setBorderLeft(BorderStyle.THIN);

		return bodyCellStyle;		
	}

	private static void setColumnWidths(XSSFSheet sheet) {
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);
		sheet.autoSizeColumn(5);
		sheet.autoSizeColumn(6);
		sheet.setColumnWidth(7, 10000);
		sheet.setColumnWidth(8, 10000);
		sheet.setColumnWidth(9, 10000);
		sheet.autoSizeColumn(10);
	}

	public static boolean sendWordDocumentToDownload(HttpServletRequest request, HttpServletResponse response,
			WordprocessingMLPackage docData, String docName, ServletContext servletContext)
					throws IOException, Docx4JException {
		logger.info("DDUtilClass>sendWordDocumentToDownload> docName :" + docName);
		// Set to binary type.
		String mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		response.setContentType(mimeType);
		String cd = "attachment;filename=\""
				+ URLEncoder.encode("Deloitte_" + docName + "_TechnicalDesignDocument V1.0.docx", "UTF-8") + '"';
		logger.info("DDUtilClass>sendWordDocumentToDownload> cd :" + cd);
		response.setHeader("Content-Disposition", cd);

		// Set response monitor cookie to notify of download for stopping
		// spinner
		String uniqueId = request.getParameter(DDConstants.COOKIE_NAME_RESPONSE_MONITOR);
		Cookie ck = DDUtilClass.createCookieForDownloadMonitor(uniqueId);
		if (ck != null) {
			response.addCookie(ck);
		}
		org.docx4j.openpackaging.io3.Save s = new org.docx4j.openpackaging.io3.Save(docData);
		s.save(response.getOutputStream());
		return false;
	}

	public static boolean sendExcelToDownload(HttpServletRequest request, HttpServletResponse response,
			XSSFWorkbook workbook, String docName, ServletContext servletContext) throws IOException {
		logger.info("DDUtilClass>sendWordDocumentToDownload> docName :" + docName);
		// Set to binary type.
		String mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		response.setContentType(mimeType);
		String cd = "attachment;filename=\"" + URLEncoder.encode(docName + "_Data-Dictionary.xlsx", "UTF-8") + '"';
		logger.info("DDUtilClass>sendWordDocumentToDownload> cd :" + cd);
		response.setHeader("Content-Disposition", cd);

		// Set response monitor cookie to notify of download for stopping
		// spinner
		String uniqueId = request.getParameter(DDConstants.COOKIE_NAME_RESPONSE_MONITOR);
		Cookie ck = DDUtilClass.createCookieForDownloadMonitor(uniqueId);
		if (ck != null) {
			response.addCookie(ck);
		}

		workbook.write(response.getOutputStream());
		return true;
	}

	public static Cookie createCookieForDownloadMonitor(String uniqueId) {
		if (uniqueId == null || uniqueId.isEmpty())
			return null;
		Cookie ck = new Cookie(DDConstants.COOKIE_NAME_RESPONSE_MONITOR + "_" + uniqueId, "success");
		// Ten mins of expiry
		ck.setMaxAge(600);
		ck.setPath("/");
		return ck;
	}

	// Set the logging level
	public static void setLogLevel(Logger logger) {
		Level logLevel;
		String envLogLevel = System.getenv(DDConstants.ENV_LOG_LEVEL);
		if (envLogLevel != null && !envLogLevel.isEmpty() && DDConstants.LOG_LEVEL_MAP.containsKey(envLogLevel)) {
			logLevel = DDConstants.LOG_LEVEL_MAP.get(envLogLevel);
		} else {
			logLevel = DDConstants.LOG_LEVEL_MAP.get(DDConstants.DEFAULT_LOG_LEVEL);
		}
		logger.setLevel(logLevel);
	}

	public FileProperties[] getSortedMetadata(FileProperties[] fpMetadata) {
		List<FileProperties> metadataToBeSortList = new ArrayList<FileProperties>(Arrays.asList(fpMetadata));
		Collections.sort(metadataToBeSortList, new Comparator<FileProperties>() {
			public int compare(FileProperties fpOne, FileProperties fpTwo) {
				Calendar xcal = (Calendar) fpOne.getLastModifiedDate();
				Calendar ycal = (Calendar) fpTwo.getLastModifiedDate();
				if (ycal.before(xcal))
					return -1;
				if (ycal.after(xcal))
					return 1;
				return 0;
			}
		});
		return metadataToBeSortList.toArray(new FileProperties[metadataToBeSortList.size()]);
	}

	public Set<String> getUsers(String users) {
		if (users == null || users.isEmpty()) {
			return null;
		}
		users = users.substring(1, users.length() - 1);
		users = users.replace("'", "");
		Set<String> userSet = new HashSet<String>(Arrays.asList(users.split(",")));
		return userSet;
	}

	public static SoapConnection buildToolingConnection(String sessionId, String serviceEndpoint) {
		if (sessionId == null || serviceEndpoint == null)
			return null;
		ConnectorConfig toolingConfig = new ConnectorConfig();
		toolingConfig.setSessionId(sessionId); // Configuring ToolingConfig
		toolingConfig.setServiceEndpoint(serviceEndpoint);
		try {
			SoapConnection toolingConnection = com.sforce.soap.tooling.Connector.newConnection(toolingConfig);
			return toolingConnection;
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static PartnerConnection buildPartnerConnection(String sessionId, String serviceEndpoint) {
		logger.info("buildPartnerConnection> sessionId :" + sessionId);
		logger.info("buildPartnerConnection> serviceEndpoint :" + serviceEndpoint);
		if (sessionId == null || serviceEndpoint == null)
			return null;
		ConnectorConfig partnerConfig = new ConnectorConfig();
		partnerConfig.setSessionId(sessionId); // Configuring PartnerConfig
		partnerConfig.setServiceEndpoint(serviceEndpoint);
		try {
			PartnerConnection partnerConnection = com.sforce.soap.partner.Connector.newConnection(partnerConfig);
			return partnerConnection;
		} catch (ConnectionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static com.sforce.soap.metadata.MetadataConnection buildMetadataConnection(String sessionId,
			String serviceEndpoint) {
		System.out.println("-- SessionId --" + sessionId);
		if (sessionId == null)
			return null;
		String endpoint = "https://test.salesforce.com/services/Soap/u/34.0";
		ConnectorConfig metadataConfig = new ConnectorConfig();
		// metadataConfig.setUsername(userName);
		// metadataConfig.setAuthEndpoint(endpoint.toString().replace("u","c"));
		metadataConfig.setSessionId(sessionId);
		logger.info("buildMetadataConnection> serviceEndpoint before replacing :" + serviceEndpoint);
		String mdServiceEndpoint = serviceEndpoint.replace("/u/", "/m/");
		metadataConfig.setServiceEndpoint(mdServiceEndpoint);
		logger.info("buildMetadataConnection> serviceEndpoint before replacing :" + mdServiceEndpoint);
		try {
			com.sforce.soap.metadata.MetadataConnection metadataConnection = com.sforce.soap.metadata.Connector
					.newConnection(metadataConfig);
			// com.sforce.soap.metadata.MetadataConnection metadataConnection =
			// new MetadataConnection(metadataConfig);
			logger.info("buildMetadataConnection> metadataConnection : " + metadataConnection.getClass());
			return metadataConnection;
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static SoapConnection getToolingConnectionFromSession(HttpServletRequest request) {
		HttpSession userSession = request.getSession(false);
		logger.info("getToolingConnectionFromSession> userSession :" + userSession);
		if (userSession == null) {
			return null;
		}
		UserSessionDetails usd = LoginLogic.getSessionDetails(request);
		logger.info("getToolingConnectionFromSession> userSessionDetail:" + usd);
		if (usd == null || usd.getLoginDetails() == null || usd.getLoginDetails().toolingServiceEndpoint == null) {
			return null;
		}
		// logger.info("getToolingConnectionFromSession> usd.getLoginDetails()
		// :" + usd.getLoginDetails());
		// logger.info("getToolingConnectionFromSession>
		// usd.getLoginDetails().toolingServiceEndpoint :" +
		// usd.getLoginDetails().toolingServiceEndpoint);
		SoapConnection toolingConnection = DDUtilClass.buildToolingConnection(usd.getToolingSessionId(),
				usd.getLoginDetails().getToolingServiceEndpoint());
		return toolingConnection;
	}

	public static PartnerConnection getPartnerConnectionFromSession(HttpServletRequest request) {
		HttpSession userSession = request.getSession(false);
		logger.info("getPartnerConnectionFromSession> userSession :" + userSession);
		if (userSession == null) {
			return null;
		}
		UserSessionDetails usd = LoginLogic.getSessionDetails(request);
		logger.info("getPartnerConnectionFromSession> userSessionDetail:" + usd);
		if (usd == null || usd.getLoginDetails() == null || usd.getLoginDetails().toolingServiceEndpoint == null) {
			return null;
		}
		PartnerConnection partnerConnection = DDUtilClass.buildPartnerConnection(usd.getPartnerSessionId(),
				usd.getLoginDetails().getPartnerServiceEndpoint());
		logger.info("getPartnerConnectionFromSession> partnerConnection :" + partnerConnection);
		return partnerConnection;
	}

	public static MetadataConnection getMetadataConnectionFromSession(HttpServletRequest request) {
		HttpSession userSession = request.getSession(false);
		logger.info("getMetadataConnectionFromSession> userSession :" + userSession);
		if (userSession == null) {
			return null;
		}
		UserSessionDetails usd = LoginLogic.getSessionDetails(request);
		logger.info("getMetadataConnectionFromSession> userSessionDetail:" + usd);
		if (usd == null || usd.getLoginDetails() == null || usd.getLoginDetails().metadataServiceEndpoint == null) {
			return null;
		}
		// logger.info("getMetadataConnectionFromSession> usd.getLoginDetails()
		// :" + usd.getLoginDetails());
		// logger.info("getMetadataConnectionFromSession>
		// usd.getLoginDetails().toolingServiceEndpoint :" +
		// usd.getLoginDetails().toolingServiceEndpoint);
		logger.info("getMetadataConnectionFromSession> usd.getMetadataSessionId() :" + usd.getMetadataSessionId());
		logger.info("getMetadataConnectionFromSession> usd.getPartnerSessionId() :" + usd.getPartnerSessionId());

		MetadataConnection mdConnection = DDUtilClass.buildMetadataConnection(usd.getMetadataSessionId(),
				usd.getLoginDetails().getMetadataServiceEndpoint());
		// MetadataConnection toolingConnection = usd.getMetadataConnection();
		logger.info("getMetadataConnectionFromSession> metadata connection:" + mdConnection);
		return mdConnection;
	}

	// Fetch the component session details that has data fetched from "Fetch
	// Component" button press.
	public static ComponentSessionDetails getComponentSessionDetails(HttpServletRequest request) {
		HttpSession userSession = request.getSession(false);
		logger.info("getToolingConnectionFromSession> userSession :" + userSession);
		if (userSession == null) {
			return null;
		}
		UserSessionDetails usd = LoginLogic.getSessionDetails(request);
		if (usd != null) {
			return usd.getComponentSessionDetails();
		}
		return null;
	}

	public static UserSessionDetails getUserSessionDetails(HttpServletRequest request) {
		HttpSession userSession = request.getSession(false);
		logger.info("getUserSessionDetails> userSession :" + userSession);
		if (userSession == null) {
			return null;
		}
		UserSessionDetails usd = LoginLogic.getSessionDetails(request);
		return usd;
	}

	public static List<Layout> generatePageLayoutList(List<SObject> sobjList) {
		List<Layout> tempPageLayoutList = new ArrayList<Layout>();
		for (int i = 0; i < sobjList.size(); ++i) {
			Layout pageLayoutRecord = (Layout) sobjList.get(i);
			tempPageLayoutList.add(pageLayoutRecord);
		}
		return tempPageLayoutList;
	}
}
