package com.tooling.custom;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.deloitte.force.components.ComponentSessionDetails;
import com.sforce.soap.metadata.MetadataConnection;

public class DocumentGeneratorUtil {
	// Java util logging
	private final static Logger logger = Logger.getLogger(DocumentGeneratorUtil.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	public void generateTDD(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ComponentSessionDetails csd, MetadataConnection metadataConnection) throws Exception {
		logger.info("DocumentGeneratorUtil>generateTDD> IN TDD GENERATOR CLASS");
		String clientName = request.getParameter("clientName");
		String docName = request.getParameter("projectName");
		
		//logger.info("DocumentGeneratorUtil>generateTDD> request.getParameterNames() :" + request.getParameterNames());
		String[] checkedClasses = null;
		String[] checkedValidationRule = null;
		String[] checkedVFpages = null;
		String[] checkedCustomObject = null;
		String[] checkedCustomTab = null;
		String[] checkedStaticResource = null;
		String[] checkedApexComponent = null;
		String[] checkedEmailTemplate = null;
		String[] checkedPageLayout = null;
		String[] checkedAssignmentRule = null;
		String[] checkedCustomLabel = null;
		String[] checkedSharingRules = null;
		String[] checkedWorkflowTask = null;
		//Added By Nitin
		String[] checkedBusinessProcess = null;
		String[] checkedCustomSetting = null;

		String checkedValidationRuleHidden = request.getParameter("hidden_validationCheck");
		String checkedApexClassHidden = request.getParameter("hidden_apexCheck");
		String checkedApexPagesHidden = request.getParameter("hidden_apexvfpageCheck");
		String checkedCustomObjectHidden = request.getParameter("hidden_customObjCheck");
		String checkedCustomTabHidden = request.getParameter("hidden_customTabCheck");
		String checkedStaticResourceHidden = request.getParameter("hidden_staticResourceCheck");
		//Added By Nitin
		String checkedBusinessProcessHidden = request.getParameter("hidden_businessProcessCheck");
		String checkedCustomSettingHidden = request.getParameter("hidden_customSettingCheck");
		// Apex Components
		String checkedApexComponentHidden = request.getParameter("hidden_apexComponentCheck");
		logger.info("-- Apex Component hidden --" + checkedApexComponentHidden);

		// Approval Process
		String checkedApprovalProcessHidden = request.getParameter("hidden_approvalProcessCheck");
		logger.info("-- Approval Process hidden --" + checkedApprovalProcessHidden);

		// Permission Set
		String checkedPermissionSetHidden = request.getParameter("hidden_permissionSetCheck");
		logger.info("-- Permission Set hidden --" + checkedPermissionSetHidden);
		// Email Templates
		String checkedEmailTemplateHidden = request.getParameter("hidden_emailTemplateCheck");
		logger.info("-- Email Template hidden --" + checkedEmailTemplateHidden);
		// Page Layouts
		String checkedPageLayoutHidden = request.getParameter("hidden_pageLayoutCheck");
		logger.info("-- Page Layout hidden --" + checkedPageLayoutHidden);
		// User Roles
		String checkedRoleHidden = request.getParameter("hidden_roleCheck");
		logger.info("-- Role hidden --" + checkedRoleHidden);

		// Assignment Rules
		String checkedAssignmentRuleHidden = request.getParameter("hidden_assignmentRuleCheck");
		logger.info("-- AssignmentRule hidden --" + checkedAssignmentRuleHidden);

		// Custom Labels
		String checkedCustomLabelHidden = request.getParameter("hidden_customLabelCheck");
		logger.info("-- Custom Label hidden --" + checkedCustomLabelHidden);
		
		// Sharing Rules
		String checkedSharingRulesHidden = request.getParameter("hidden_sharingRulesCheck");
		logger.info("-- Sharing Rules hidden --" + checkedSharingRulesHidden);
		
		// Email Templates
		String checkedWorkflowTaskHidden = request.getParameter("hidden_workflowTaskCheck");
		logger.info("-- Workflow Task hidden --" + checkedWorkflowTaskHidden);
				
		if (checkedValidationRuleHidden != null && !checkedValidationRuleHidden.isEmpty()) {
			checkedValidationRule = checkedValidationRuleHidden.split(",");
		}
		if (checkedApexClassHidden != null && !checkedApexClassHidden.isEmpty()) {
			checkedClasses = checkedApexClassHidden.split(",");
		}
		if (checkedApexPagesHidden != null && !checkedApexPagesHidden.isEmpty()) {
			checkedVFpages = checkedApexPagesHidden.split(",");
		}

		if (checkedCustomObjectHidden != null && !checkedCustomObjectHidden.isEmpty()) {
			checkedCustomObject = checkedCustomObjectHidden.split(",");
		}
		if (checkedCustomTabHidden != null && !checkedCustomTabHidden.isEmpty()) {
			checkedCustomTab = checkedCustomTabHidden.split(",");
		}
		if (checkedStaticResourceHidden != null && !checkedStaticResourceHidden.isEmpty()) {
			checkedStaticResource = checkedStaticResourceHidden.split(",");
		}
		//Added By Nitin
		if (checkedBusinessProcessHidden != null && !checkedBusinessProcessHidden.isEmpty()) {
			checkedBusinessProcess = checkedBusinessProcessHidden.split(",");
		}
		if (checkedCustomSettingHidden != null && !checkedCustomSettingHidden.isEmpty()) {
			checkedCustomSetting = checkedCustomSettingHidden.split(",");
		}

		String[] checkedTrigger = null;
		String checkedTriggerHidden = request.getParameter("hidden_triggerCheck");
		if (checkedTriggerHidden != null && !checkedTriggerHidden.isEmpty()) {
			checkedTrigger = checkedTriggerHidden.split(",");
		}

		String[] checkedWorkflowRule = null;
		String checkedWorkflowRuleHidden = request.getParameter("hidden_workflowRuleCheck");
		if (checkedWorkflowRuleHidden != null && !checkedWorkflowRuleHidden.isEmpty()) {
			checkedWorkflowRule = checkedWorkflowRuleHidden.split(",");
		}

		String[] checkedWorkflowFieldUpdate = null;
		String checkedWorkflowFieldUpdateHidden = request.getParameter("hidden_workflowFieldUpdateCheck");
		if (checkedWorkflowFieldUpdateHidden != null && !checkedWorkflowFieldUpdateHidden.isEmpty()) {
			checkedWorkflowFieldUpdate = checkedWorkflowFieldUpdateHidden.split(",");
		}

		String[] checkedWorkflowOutboundMessage = null;
		String checkedWorkflowOutboundMessageHidden = request.getParameter("hidden_workflowOutboundMessageCheck");
		if (checkedWorkflowOutboundMessageHidden != null && !checkedWorkflowOutboundMessageHidden.isEmpty()) {
			checkedWorkflowOutboundMessage = checkedWorkflowOutboundMessageHidden.split(",");
		}

		String[] checkedWorkflowAlert = null;
		String checkedWorkflowAlertHidden = request.getParameter("hidden_workflowAlertCheck");
		if (checkedWorkflowAlertHidden != null && !checkedWorkflowAlertHidden.isEmpty()) {
			checkedWorkflowAlert = checkedWorkflowAlertHidden.split(",");
		}
		
		String[] checkedRecordType = null;
		String checkedRecordTypeHidden = request.getParameter("hidden_recordTypeCheck");
		if (checkedRecordTypeHidden != null && !checkedRecordTypeHidden.isEmpty()) {
			checkedRecordType = checkedRecordTypeHidden.split(",");
		}

		String[] checkedProfiles = null;
		String checkedProfileHidden = request.getParameter("hidden_profileCheck");
		if (checkedProfileHidden != null && !checkedProfileHidden.isEmpty()) {
			checkedProfiles = checkedProfileHidden.split(",");
		}

		if (checkedApexComponentHidden != null && !checkedApexComponentHidden.isEmpty()) {
			checkedApexComponent = checkedApexComponentHidden.split(",");
		}
		if (checkedEmailTemplateHidden != null && !checkedEmailTemplateHidden.isEmpty()) {
			checkedEmailTemplate = checkedEmailTemplateHidden.split(",");
		}
		if (checkedPageLayoutHidden != null && !checkedPageLayoutHidden.isEmpty()) {
			checkedPageLayout = checkedPageLayoutHidden.split(",");
		}
		if (checkedAssignmentRuleHidden != null && !checkedAssignmentRuleHidden.isEmpty()) {
			checkedAssignmentRule = checkedAssignmentRuleHidden.split(",");
		}
		if (checkedWorkflowTaskHidden != null && !checkedWorkflowTaskHidden.isEmpty()) {
			checkedWorkflowTask = checkedWorkflowTaskHidden.split(",");
		}
		/*
		 * if(checkedApprovalProcessHidden != null &&
		 * !checkedApprovalProcessHidden.isEmpty()){ checkedApprovalProcess =
		 * checkedApprovalProcessHidden.split(","); }
		 */
		try {
			String s1 = "/WEB-INF/Quality_Initiative_TDD.docx";
			logger.info("DocumentGeneratorUtil>generateTDD> About to load resource from :" + s1);
			WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage
					.load(servletContext.getResourceAsStream(s1));
			logger.info("DocumentGeneratorUtil>generateTDD> Loaded resource");
			DDUtilClass dutil = new DDUtilClass();
			LoginLogic l = new LoginLogic();
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			
			l.replacePlaceholder(wordMLPackage, docName, "Project Name");
			l.replacePlaceholder(wordMLPackage, clientName, "<Client Name>");
			l.replacePlaceholder(wordMLPackage, dutil.getUserSessionDetails(request).userName, "AuthorName");
			
			l.replacePlaceholder(wordMLPackage, dateFormat.format(date), "CreationDateTemp");
			l.replacePlaceholder(wordMLPackage, dateFormat.format(date), "RevisionDateTemp");
			logger.info("DocumentGeneratorUtil>generateTDD> -- Before call of methods --");

			if (checkedClasses != null && checkedClasses.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedClasses));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchApexClassMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_APEX_CLASS, servletContext, wordMLPackage);
			}

			

			if (checkedValidationRule != null && checkedValidationRule.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedValidationRule));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchValidationRuleMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_VALIDATION_RULE, servletContext, wordMLPackage);
			}

			if (checkedProfiles != null && checkedProfiles.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedProfiles));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchProfilesMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_PROFILE, servletContext, wordMLPackage);
			}


			if (checkedVFpages != null && checkedVFpages.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedVFpages));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchApexPagesMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_APEX_PAGE, servletContext, wordMLPackage);
			}

			if (checkedTrigger != null && checkedTrigger.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedTrigger));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchApexTriggerMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_APEX_TRIGGER, servletContext, wordMLPackage);
			}

			

			if (checkedCustomTab != null && checkedCustomTab.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedCustomTab));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchcustomtabsMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_CUSTOM_TAB, servletContext, wordMLPackage);
			}

			if (checkedStaticResource != null && checkedStaticResource.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedStaticResource));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchstaticResourcesMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_STATIC_RESOURCE, servletContext, wordMLPackage);
			}

			if (checkedWorkflowRule != null && checkedWorkflowRule.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedWorkflowRule));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchWorkflowRuleMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_WORKFLOW_RULE, servletContext, wordMLPackage);
			}

			if (checkedWorkflowFieldUpdate != null && checkedWorkflowFieldUpdate.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedWorkflowFieldUpdate));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchWorkflowFieldUpdateMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_WORKFLOW_FIELDUPDATE, servletContext, wordMLPackage);
			}

			if (checkedWorkflowOutboundMessage != null && checkedWorkflowOutboundMessage.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedWorkflowOutboundMessage));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchWorkflowOutboundMessageMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_WORKFLOW_OUTBOUNDMESSAGE, servletContext, wordMLPackage);
			}

			if (checkedWorkflowAlert != null && checkedWorkflowAlert.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedWorkflowAlert));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchWorkflowAlertMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_WORKFLOW_ALERT, servletContext, wordMLPackage);
			}
			
			if (checkedRecordType != null && checkedRecordType.length != 0) {
				Set<String> classIdSet = new HashSet<String>();
				classIdSet.addAll(Arrays.asList(checkedRecordType));
				String classIdString = dutil.quoteKeySet(classIdSet);
				l.fetchRecordTypeMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_RECORDTYPE, servletContext, wordMLPackage);
			}

			if (checkedApexComponent != null && checkedApexComponent.length != 0) {
				Set<String> componentIdSet = new HashSet<String>();
				componentIdSet.addAll(Arrays.asList(checkedApexComponent));
				logger.info("-- componentIdSet --" + componentIdSet);
				String classIdString = dutil.quoteKeySet(componentIdSet);
				l.fetchApexComponentMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_APEX_COMPONENT, servletContext, wordMLPackage);
			}

			if (checkedApprovalProcessHidden != null && checkedApprovalProcessHidden.length() != 0) {
				// Set<String> approvalProcessIdSet = new HashSet<String>();
				// approvalProcessIdSet.addAll(Arrays.asList(checkedApprovalProcessHidden));
				// logger.info("-- approvalProcessIdSet
				// --"+approvalProcessIdSet);
				// String classIdString =
				// dutil.quoteKeySet(approvalProcessIdSet);
				Map<String, String> mapApprovalProcessId = new HashMap<String, String>();
				mapApprovalProcessId.put("componentId", checkedApprovalProcessHidden);
				l.fetchApprovalProcessMethod(mapApprovalProcessId, servletContext, wordMLPackage, csd, metadataConnection);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_APPROVAL_PROCESS, servletContext, wordMLPackage);
			}
			
			if (checkedPermissionSetHidden != null && checkedPermissionSetHidden.length() != 0) {
				Map<String, String> mapPermissionSetId = new HashMap<String, String>();
				mapPermissionSetId.put("componentId", checkedPermissionSetHidden);
				l.fetchPermissionSetMethod(mapPermissionSetId, servletContext, wordMLPackage, csd, metadataConnection);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_PERMISSION_SET, servletContext, wordMLPackage);
			}

			if (checkedRoleHidden != null && checkedRoleHidden.length() != 0) {
				Map<String, String> mapRoleId = new HashMap<String, String>();
				mapRoleId.put("componentId", checkedRoleHidden);
				l.fetchRoleMethod(mapRoleId, servletContext, wordMLPackage, csd, metadataConnection);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_USER_ROLE, servletContext, wordMLPackage);
			}

			//Added  by Nitin
			if (checkedBusinessProcessHidden != null && checkedBusinessProcessHidden.length() != 0) {
				Map<String, String> mapBusinessProcessId = new HashMap<String, String>();
				mapBusinessProcessId.put("componentId", checkedBusinessProcessHidden);
				logger.info("==methodCallInDocumentGenerator===="+wordMLPackage);
				l.fetchBusinessProcessMethod(mapBusinessProcessId, servletContext, wordMLPackage, csd, metadataConnection);
			} else {
				logger.info("DocumentGeneratorUtil>TYPE_BUSINESS_PROCESS> -- Before call of methods --"+checkedBusinessProcess);
				l.resetVariablesMethod(DDConstants.TYPE_BUSINESS_PROCESS, servletContext, wordMLPackage);
			}

			if (checkedCustomSettingHidden != null && checkedCustomSettingHidden.length() != 0) {
				Map<String, String> mapCustomSettingId = new HashMap<String, String>();
				mapCustomSettingId.put("componentId", checkedCustomSettingHidden);
				logger.info("==methodCallInDocumentGenerator===="+wordMLPackage);
				l.fetchCustomSettingsMethod(mapCustomSettingId, servletContext, wordMLPackage, csd, metadataConnection);
			} else {
				logger.info("DocumentGeneratorUtil>TYPE_CUSTOM_SETTING> -- Before call of methods --"+checkedCustomSettingHidden);
				l.resetVariablesMethod(DDConstants.TYPE_CUSTOM_SETTING, servletContext, wordMLPackage);
			}
			if (checkedCustomObjectHidden != null && checkedCustomObjectHidden.length() != 0) {
				Map<String, String> mapCustomObjectId = new HashMap<String, String>();
				mapCustomObjectId.put("componentId", checkedCustomObjectHidden);
				
				l.fetchcustomobjsMethod(mapCustomObjectId, servletContext, wordMLPackage, csd, metadataConnection);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_CUSTOM_OBJECT, servletContext, wordMLPackage);
			}

			if (checkedEmailTemplate != null && checkedEmailTemplate.length != 0) {
				Set<String> componentIdSet = new HashSet<String>();
				componentIdSet.addAll(Arrays.asList(checkedEmailTemplate));
				logger.info("-- componentIdSet --" + componentIdSet);
				String classIdString = dutil.quoteKeySet(componentIdSet);
				l.fetchEmailTemplateMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_EMAIL_TEMPLATE, servletContext, wordMLPackage);
			}
			
			if (checkedSharingRulesHidden != null && checkedSharingRulesHidden.length() != 0) {
				Map<String, String> mapSharingRulesId = new HashMap<String, String>();
				mapSharingRulesId.put("componentId", checkedSharingRulesHidden);
				l.fetchSharingRulesMethod(mapSharingRulesId, servletContext, wordMLPackage, csd, metadataConnection);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_SHARING_RULE,servletContext, wordMLPackage);
			}
			
			if (checkedWorkflowTask != null && checkedWorkflowTask.length != 0) {
				Set<String> componentIdSet = new HashSet<String>();
				componentIdSet.addAll(Arrays.asList(checkedWorkflowTask));
				String classIdString = dutil.quoteKeySet(componentIdSet);
				l.fetchWorkflowTaskMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_WORKFLOW_TASK, servletContext, wordMLPackage);
			}

			if (checkedPageLayout != null && checkedPageLayout.length != 0) {
				Set<String> componentIdSet = new HashSet<String>();
				componentIdSet.addAll(Arrays.asList(checkedPageLayout));
				logger.info("-- componentIdSet --" + componentIdSet);
				String classIdString = dutil.quoteKeySet(componentIdSet);
				l.fetchPageLayoutMethod(classIdString, servletContext, wordMLPackage, csd);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_PAGE_LAYOUT, servletContext, wordMLPackage);
			}
			// if (checkedAssignmentRule != null && checkedAssignmentRule.length
			// != 0) {
			// Set<String> componentIdSet = new HashSet<String>();
			// componentIdSet.addAll(Arrays.asList(checkedAssignmentRule));
			// logger.info("-- componentIdSet --"+componentIdSet);
			// String classIdString = dutil.quoteKeySet(componentIdSet);
			// l.fetchAssignmentRuleMethod(classIdString, servletContext,
			// wordMLPackage);
			// }
			// else {
			// //l.resetVariablesMethod(DDConstants.TYPE_ASSIGNMENT_RULE,servletContext,
			// wordMLPackage);
			// //Uncomment the above method call once Assignment rules are
			// handled in TDD
			// }
			if (checkedCustomLabelHidden != null && checkedCustomLabelHidden.length() != 0) {
				// Set<String> approvalProcessIdSet = new HashSet<String>();
				// approvalProcessIdSet.addAll(Arrays.asList(checkedApprovalProcessHidden));
				// logger.info("-- approvalProcessIdSet
				// --"+approvalProcessIdSet);
				// String classIdString =
				// dutil.quoteKeySet(approvalProcessIdSet);
				Map<String, String> mapCustomLabelId = new HashMap<String, String>();
				mapCustomLabelId.put("componentId", checkedCustomLabelHidden);
				l.fetchCustomLabelMethod(mapCustomLabelId, servletContext, wordMLPackage, csd, metadataConnection);
			} else {
				l.resetVariablesMethod(DDConstants.TYPE_CUSTOM_LABEL,servletContext,
				 	wordMLPackage);

				// Uncomment the above method call once Custom Labels are
				// handled in TDD
			}

			DDUtilClass.sendWordDocumentToDownload(request, response, wordMLPackage, docName, servletContext);
		} catch (Docx4JException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
