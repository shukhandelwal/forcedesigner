package com.tooling.custom;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class DDConstants {

	public static final String SUCCESS = "Success";
	public static final String ENV_DEV = "Sandbox";
	public static final String ENV_PROD = "Production";
	public static final String ENV_VAR_NAME_OAUTH_REDIRECT_URL = "OAUTH_REDIRECT_URL";
	public static final String ENV_VAR_NAME_RECORD_LIMIT = "RECORD_LIMIT";
	public static final String ENV_VAR_NAME_DATA_DICTIONARY_FIELD_LIMIT = "DATA_DICTIONARY_FIELD_LIMIT";
	public static final int SOQL_LIMIT = System.getenv().containsKey(ENV_VAR_NAME_RECORD_LIMIT)
			? Integer.valueOf(System.getenv().get(ENV_VAR_NAME_RECORD_LIMIT)) : 500;
	public static final int DATA_DICTIONARY_SOQL_LIMIT = System.getenv()
			.containsKey(ENV_VAR_NAME_DATA_DICTIONARY_FIELD_LIMIT)
					? Integer.valueOf(System.getenv().get(ENV_VAR_NAME_DATA_DICTIONARY_FIELD_LIMIT)) : 5000;

	public static final String TYPE_APEX_CLASS = "ApexClass";
	public static final String TYPE_APEX_TRIGGER = "ApexTrigger";
	public static final String TYPE_APEX_PAGE = "ApexPage";
	public static final String TYPE_CUSTOM_FIELD = "CustomField";
	public static final String TYPE_VALIDATION_RULE = "ValidationRule";
	public static final String TYPE_WORKFLOW_RULE = "WorkflowRule";
	public static final String TYPE_WORKFLOW_FIELDUPDATE = "WorkflowFieldUpdate";
	public static final String TYPE_WORKFLOW_OUTBOUNDMESSAGE = "WorkflowOutboundMessage";
	public static final String TYPE_WORKFLOW_ALERT = "WorkflowAlert";
	public static final String TYPE_APEX_COMPONENT = "ApexComponent";
	public static final String TYPE_STATIC_RESOURCE = "StaticResource";
	public static final String TYPE_CUSTOM_OBJECT = "CustomObject";
	public static final String TYPE_CUSTOM_SETTING = "CustomSetting";
	public static final String TYPE_APPROVAL_PROCESS = "ApprovalProcess";
	public static final String TYPE_USER_ROLE = "Role";
	public static final String TYPE_PERMISSION_SET = "PermissionSet";
	public static final String TYPE_ASSIGNMENT_RULE = "AssignmentRule";
	public static final String TYPE_CUSTOM_TAB = "CustomTab";
	public static final String TYPE_EMAIL_TEMPLATE = "EmailTemplate";
	public static final String TYPE_PAGE_LAYOUT= "PageLayout";
	public static final String TYPE_QUEUE = "Queue";
	public static final String TYPE_RECORDTYPE = "RecordType";
	public static final String TYPE_REPORTTYPE = "ReportType";
	public static final String TYPE_WORKFLOW = "Workflow";
	public static final String TYPE_FLOW = "Flow";
	public static final String TYPE_CUSTOM_LABEL = "CustomLabel";
	public static final String TYPE_PROFILE = "Profile";
	public static final String TYPE_METADATA_FIELDS = "Metadata";
	public static final String TYPE_SHARING_RULE = "SharingRules";
	public static final String TYPE_SHARING_CRITERIA_RULE = "SharingCriteriaRule";
	public static final String TYPE_SHARING_OWNER_RULE = "SharingOwnerRule";
	public static final String TYPE_WORKFLOW_TASK = "WorkflowTask";
	//Added By Nitin
	public static final String TYPE_BUSINESS_PROCESS = "BusinessProcess";

	public static final String JSON_KEY_TYPE_APEX_CLASS = "apexclasslist";
	public static final String JSON_KEY_TYPE_APEX_TRIGGER = "apextriggerlist";
	public static final String JSON_KEY_TYPE_APEX_PAGE = "apexpagelist";
	public static final String JSON_KEY_TYPE_CUSTOM_FIELD = "customfieldlist";
	public static final String JSON_KEY_TYPE_VALIDATION_RULE = "validationrulelist";
	public static final String JSON_KEY_TYPE_WORKFLOW_RULE = "workflowrulelist";
	public static final String JSON_KEY_TYPE_WORKFLOW_FIELDUPDATE = "workflowfieldupdatelist";
	public static final String JSON_KEY_TYPE_WORKFLOW_OUTBOUNDMESSAGE = "workflowoutboundmessagelist";
	public static final String JSON_KEY_TYPE_WORKFLOW_ALERT = "workflowalertlist";
	public static final String JSON_KEY_TYPE_APEX_COMPONENT = "apexcomponentlist";
	public static final String JSON_KEY_TYPE_STATIC_RESOURCE = "staticresourcelist";
	public static final String JSON_KEY_TYPE_CUSTOM_OBJECT = "customobjectlist";
	public static final String JSON_KEY_TYPE_CUSTOM_SETTING = "customsettingslist";
	public static final String JSON_KEY_TYPE_APPROVAL_PROCESS = "approvalprocesslist";
	public static final String JSON_KEY_TYPE_USER_ROLE = "rolelist";
	public static final String JSON_KEY_TYPE_PERMISSION_SET = "permissionsetlist";
	public static final String JSON_KEY_TYPE_ASSIGNMENT_RULE = "assignmentrulelist";
	public static final String JSON_KEY_TYPE_CUSTOM_TAB = "customtablist";
	public static final String JSON_KEY_TYPE_EMAIL_TEMPLATE = "emailtemplatelist";
	public static final String JSON_KEY_TYPE_PAGE_LAYOUT = "pagelayoutlist";
	public static final String JSON_KEY_TYPE_QUEUE = "queuelist";
	public static final String JSON_KEY_TYPE_RECORDTYPE = "recordtypelist";
	public static final String JSON_KEY_TYPE_REPORTTYPE = "reporttypelist";
	public static final String JSON_KEY_TYPE_WORKFLOW = "workflowlist";
	public static final String JSON_KEY_TYPE_FLOW = "flowlist";
	public static final String JSON_KEY_TYPE_CUSTOM_LABEL = "customlabellist";
	public static final String JSON_KEY_TYPE_PROFILE = "profilelist";
	public static final String JSON_KEY_TYPE_SHARING_RULE = "sharingruleslist";
	public static final String JSON_KEY_TYPE_WORKFLOW_TASK = "workflowtasklist";
	//Added By Nitin
	public static final String JSON_KEY_TYPE_BUSINESS_PROCESS = "businessprocesslist";


	public static final String PARAM_GENERATE_TDD_BUTTON = "GenerateTDD";
	public static final String PARAM_GENERATE_DD_BUTTON = "GenerateDD";

	public static final String COOKIE_NAME_RESPONSE_MONITOR = "response-monitor";

	public static final String DEFAULT_LOG_LEVEL = Level.INFO.getName();
	public static final String ENV_LOG_LEVEL = "LOG_LEVEL";
	public static final String ENV_REDIS_URL = "REDIS_URL";

	public static final Map<String, Level> LOG_LEVEL_MAP = new HashMap<String, Level>();
	public static final String SESSION_KEY = "UserSession";

	static {
		// Build log level map
		LOG_LEVEL_MAP.put(Level.SEVERE.getName(), Level.SEVERE);
		LOG_LEVEL_MAP.put(Level.WARNING.getName(), Level.WARNING);
		LOG_LEVEL_MAP.put(Level.INFO.getName(), Level.INFO);
		LOG_LEVEL_MAP.put(Level.FINE.getName(), Level.FINE);
		LOG_LEVEL_MAP.put(Level.FINER.getName(), Level.FINER);
		LOG_LEVEL_MAP.put(Level.FINEST.getName(), Level.FINEST);
	}
}
