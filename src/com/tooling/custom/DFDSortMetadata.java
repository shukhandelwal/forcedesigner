package com.tooling.custom;

import java.util.Calendar;
import java.util.Comparator;

import com.sforce.soap.metadata.FileProperties;

public class DFDSortMetadata implements Comparator<FileProperties> {
	public int compare(FileProperties fpOne, FileProperties fpTwo) {
		Calendar xcal = (Calendar) fpOne.getLastModifiedDate();
		Calendar ycal = (Calendar) fpTwo.getLastModifiedDate();
		if (xcal.before(ycal))
			return -1;
		if (xcal.after(ycal))
			return 1;
		return 0;
	}
}
