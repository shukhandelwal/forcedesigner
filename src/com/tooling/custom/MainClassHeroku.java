package com.tooling.custom;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.deloitte.force.components.LoginDetails;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class MainClassHeroku extends HttpServlet {
	// Java util logging
	private final static Logger logger = Logger.getLogger(MainClassHeroku.class.getName());

	static {
		DDUtilClass.setLogLevel(logger);
	}

	/**
	 * 
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("IN SERVLET ");

		String userName = request.getParameter("userName").trim();
		String password = request.getParameter("password").trim();
		String environment = request.getParameter("environment").trim();
		String domainURL = request.getParameter("domainURL").trim();

		// logger.info("input values"+ userName + " " + password + " "
		// +environment );
		if ((userName == null || userName.isEmpty()) || (password == null || password.isEmpty())
				|| (environment == null || environment.isEmpty())) {
			sendLoginResponse(false, "Please enter valid information", response);
		} else {
			logger.info("MainClassHeroku>doPost> input data okay");
			try {
				Boolean isProd = false;
				if (DDConstants.ENV_PROD.equals(environment)) {
					isProd = true;
				}
				LoginLogic l = new LoginLogic();
				LoginDetails result = l.initMethod(userName, password, domainURL, isProd);
				logger.info("MainClassHeroku>doPost> result :" + result);
				// If it's success, redirect.
				if (DDConstants.SUCCESS.equals(result.loginResult)) {
					LoginLogic.doSessionStuff(request, result);

					logger.info("MainClassHeroku>doPost> Going to redirect");
					sendLoginResponse(true, "Success", response);
				} else {
					// Else, send the error response.
					logger.info("MainClassHeroku> Sending back error message :" + result);
					sendLoginResponse(false, result.loginResult, response);
				}
			} catch (Exception e) {
				logger.severe("MainClassHeroku> Exception :" + e);
				e.printStackTrace();
			}
		}

		logger.info("IN SERVLET END ");
	}

	public void sendLoginResponse(Boolean success, String msg, HttpServletResponse response) {
		try {
			if (msg == null || response == null) {
				throw new Exception("Bad input");
			}
			DFDesignerResponse lr = new DFDesignerResponse();
			lr.setSuccess(success);
			lr.setMsg(msg);

			Gson gson = new Gson();
			String jsonResponse = gson.toJson(lr);
			logger.info("MainClassHeroku>sendLoginResponse>jsonResponse :" + jsonResponse);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonResponse);
		} catch (IOException e) {
			logger.severe("MainClassHeroku>sendLoginResponse> IOException :" + e);
			e.printStackTrace();
		} catch (Exception ex) {
			logger.severe("MainClassHeroku>sendLoginResponse> Exception :" + ex);
			ex.printStackTrace();
		}
	}
}
